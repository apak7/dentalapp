import React from "react";
import { Link } from "react-router-dom";
import { Table } from "@mantine/core";
import '../App.css';

export const Fees = ({ fee }) => {
  // Check if fee and fee.children are defined before using map
  const rows = fee.children && Array.isArray(fee.children)
    ? fee.children.map((item) => (
        <Table.Tr key={item.full_code}>
          <Table.Td width={100}>
            {item.children && item.children.length > 0 ? (
              <Link to={`/fees/${item.full_code}`}>
                {item.full_code}
              </Link>
            ) : (
              // Render plain text when there are no children
              <span>{item.full_code}</span>
            )}
          </Table.Td>
          <Table.Td width={1000}>{item.description}</Table.Td>
          <Table.Td width={200}>{item.fee}</Table.Td>

        </Table.Tr>
      ))
    : null;

  // Render the table only if there are children
  return (
    <>
      {fee.children && Array.isArray(fee.children) && (
        <Table className="table-contents">
          <Table.Thead>
            <Table.Tr>
              <Table.Th>Code</Table.Th>
              <Table.Th>Description</Table.Th>
              <Table.Th>Fee</Table.Th>
            </Table.Tr>
          </Table.Thead>
          <Table.Tbody>{rows}</Table.Tbody>
        </Table>
      )}
    </>
  );
};
