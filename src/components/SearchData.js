export function SearchData(data, targetId) {
    for (const item of data) {
      if (item.full_code === targetId) {
        return item; // Return after finding the target
      }
  
      if (item.children && item.children.length > 0) {
        const result = SearchData(item.children, targetId);
  
        // If a match is found in the recursive call, return it
        if (result.full_code === targetId) {
          return result;
        }
      }
    }
  
    // Return an empty object if no match is found
    return {};
  }
  