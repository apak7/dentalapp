import { FeePage } from './pages/FeesPage';
import { HomePage } from './pages/HomePage'
import { Route, Routes } from'react-router-dom'
import { MantineProvider } from '@mantine/core';

function App() {
  return (
    <MantineProvider>
        <h1 className='title'>Alberta Dental Fee Guide</h1>

      <div className="App">
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/fees/:feeId" element={<FeePage />} />

        </Routes>
      </div>
    </MantineProvider>
  );
}

export default App;
