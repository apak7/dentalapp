import React, { useState, useEffect } from 'react';
import data from '../data';
import { Fees } from '../components/Fees';

export const HomePage = () => {
    const [root, setRoot] = useState({})

    useEffect(() => {
        const handleFee = () => {
            setRoot({
                full_code:'root',
                children: data
            })
        };
      handleFee();
    }, []);

    return (
        <div>
          {root ? (
            <div>
              {/* {root.full_code && <p>{root.full_code}</p>} */}
                <Fees fee={root}/>
              {/* Render other properties or components if needed */}
            </div>
          ) : (
            <p>Loading...</p>
          )}
        </div>
      );
}