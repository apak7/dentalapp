import React, { useState, useEffect } from 'react';
import data from '../data';
import { Fees } from '../components/Fees';
import { useParams } from 'react-router-dom';
import { SearchData } from '../components/SearchData';

export const FeePage = () => {
    const [feeList, setFeeList] = useState({});
    const {feeId} = useParams();

    useEffect(() => {
        const handleFee = () => {

            const res = SearchData(data, feeId);
            setFeeList(res);
    };
        handleFee();
    }, [feeId]);

    return (
        <>
        <h2>{feeList.full_code} {feeList.description}</h2>
        <Fees fee={feeList}/>
        </>
    );
};

