const data = [
  {
    "full_code": "00000",
    "children": [
      {
        "full_code": "01000",
        "children": [
          {
            "full_code": "01010",
            "children": [
              {
                "description": "Oral assessment for patients up to the age of 3 years inclusive. Assessment to include: Medical history, familial dental history; dietary/feeding practices; oral habits; oral hygiene; fluoride exposure.\nAnticipatory guidance with parent/guardian",
                "descriptor": "",
                "fee": "81.82",
                "full_code": "01011"
              }
            ],
            "descriptor": "",
            "description": "FIRST DENTAL VISIT/ORIENTATION",
            "fee": ""
          },
          {
            "full_code": "01100",
            "children": [
              {
                "description": "Examination and Diagnosis, Complete, Primary Dentition, to include: (a) Extended examination and diagnosis on primary dentition, recording  history, charting, treatment planning and case presentation, including above description as per 01100.",
                "descriptor": "",
                "fee": "81.82",
                "full_code": "01101"
              },
              {
                "description": "Examination and Diagnosis, Complete, Mixed Dentition, to include: (a) Extended examination and diagnosis on mixed dentition, recording history, charting, treatment planning and case presentation, including above description as per 01100. (b) Eruption sequence, tooth size - jaw size assessment.",
                "descriptor": "",
                "fee": "111.51",
                "full_code": "01102"
              },
              {
                "description": "Examination and Diagnosis, Complete, Permanent Dentition, to include: (a) Extended examination on permanent dentition, recording history, charting, treatment planning and case presentation, including above description as per 01100.",
                "descriptor": "",
                "fee": "116.66",
                "full_code": "01103"
              }
            ],
            "descriptor": "",
            "description": "EXAMINATIONS, AND DIAGNOSIS COMPLETE ORAL, to include:\n(a) History, Medical and Dental.\n(b) Clinical Examination and Diagnosis of Hard and Soft tissues, including the following as necessary: Carious lesions, missing teeth, determination of sulcular depth, gingival contours, mobility of teeth, interproximal tooth contact relationships, occlusion of teeth, TMJ, pulp vitality test/analysis, where necessary and any other pertinent factors;\n(c) Radiographs  extra, as required.",
            "fee": ""
          },
          {
            "full_code": "01200",
            "children": [
              {
                "description": "Examination and Diagnosis, Limited, Oral, New Patient. Examination and diagnosis of hard and soft tissues, including checking of occlusion and appliances, but not including specific test/ analysis as for\n01100.  (May include PSR)",
                "descriptor": "",
                "fee": "86.63",
                "full_code": "01201"
              },
              {
                "description": "Examination and diagnosis, Limited oral, Previous Patient (recall). Examination of hard and soft tissues, including checking of occlusion and appliances, but not including specific tests, as for 01100.",
                "descriptor": "",
                "fee": "73.85",
                "full_code": "01202"
              },
              {
                "description": "Examination and Diagnosis, Specific  Examination and evaluation of a specific situation  in a localized area. Not to be used as a substitute for limited exam codes (01201, 01202)",
                "descriptor": "",
                "fee": "73.85",
                "full_code": "01204"
              },
              {
                "description": "Examination and Diagnosis, Emergency.  Examination and Diagnosis for the investigation of discomfort and/or infection in a localized area. Not to be used as a substitute for limited exam codes (01201,\n01202).",
                "descriptor": "",
                "fee": "73.85",
                "full_code": "01205"
              },
              {
                "description": "Analysis, Mixed Dentition",
                "descriptor": "",
                "fee": "92.69",
                "full_code": "01206"
              }
            ],
            "descriptor": "",
            "description": "EXAMINATIONS AND DIAGNOSIS, LIMITED ORAL",
            "fee": ""
          },
          {
            "full_code": "01300",
            "children": [
              {
                "description": "Examination and Diagnosis, Stomatognathic Dysfunctional, Comprehensive, to include: (a) History, Medical , Dental, Pain/Dysfunction (b) Clinical examination to include, general appraisal, examination of head and neck, musculoskeletal system (static and functional); intraoral examination of hard and soft tissues, including occlusal analysis; consultation with other health care professionals, review of previous records, including radiographs, ordering of appropriate test/analysis and consultations.",
                "descriptor": "",
                "fee": "310.6",
                "full_code": "01301"
              },
              {
                "description": "Examination and Diagnosis, Stomatognathic Dysfunctional, Limited",
                "descriptor": "",
                "fee": "94.36",
                "full_code": "01302"
              }
            ],
            "descriptor": "",
            "description": "EXAMINATIONS AND DIAGNOSIS, STOMATOGNATHIC, DYSFUNCTIONAL",
            "fee": ""
          },
          {
            "full_code": "01400",
            "children": [
              {
                "description": "Examination and Diagnosis, Oral Pathology, General, to include: (a) Initial consultation with referring dentist or physician, (b) History, Medical and Dental, (c) Clinical examination including in-depth analysis of medical status, (d) Diagnosis, prognosis and formulation of a treatment plan.",
                "descriptor": "",
                "fee": "188.71",
                "full_code": "01401"
              },
              {
                "description": "Examination and Diagnosis, Oral Pathology, Specific (or repeat examination within 90 days for the\nsame illness).",
                "descriptor": "",
                "fee": "94.36",
                "full_code": "01402"
              }
            ],
            "descriptor": "",
            "description": "EXAMINATIONS AND DIAGNOSIS, ORAL PATHOLOGY",
            "fee": ""
          },
          {
            "full_code": "01500",
            "children": [
              {
                "description": "Examination and Diagnosis, Periodontal, General Recording History, Charting, Treatment Planning and Case Presentation: (a) History, Medical and Dental (b) Clinical Examination includes evaluation of topography of the gingiva and related structures; degree of gingival inflammation; location, extent, sulcular depth; furcation involvement, mobility of teeth; tooth contact relationships; evaluation of occlusion; TMJ; examination of oral soft tissue pathosis; evaluation of the existing restorative and/or prosthetic appliances; caries and pulpal vitality.",
                "descriptor": "",
                "fee": "236.91",
                "full_code": "01501"
              },
              {
                "description": "Examination and Diagnosis, Periodontal,  Limited (previous patient)",
                "descriptor": "",
                "fee": "171.59",
                "full_code": "01502"
              },
              {
                "description": "Examination and Diagnosis, Periodontal, Specific",
                "descriptor": "",
                "fee": "171.59",
                "full_code": "01503"
              }
            ],
            "descriptor": "",
            "description": "EXAMINATION AND DIAGNOSIS, PERIODONTAL",
            "fee": ""
          },
          {
            "full_code": "01600",
            "children": [
              {
                "description": "Examination and Diagnosis, Surgical, General (a) History, Medical and Dental (b) Clinical Examination as above, may include in-depth analysis of medical status, medication, anaesthetic and surgical risk, initial consultation with referring dentist or physician, parent or guardian, evaluation of source of chief complaint, evaluation of pulpal vitality, mobility of teeth, occlusal factors, TMJ, or where the patient is to be admitted to hospital for dental procedures.",
                "descriptor": "",
                "fee": "188.72",
                "full_code": "01601"
              },
              {
                "description": "Examination and Diagnosis, Surgical, Specific",
                "descriptor": "",
                "fee": "113.12",
                "full_code": "01602"
              }
            ],
            "descriptor": "",
            "description": "EXAMINATIONS AND DIAGNOSIS, SURGICAL",
            "fee": ""
          },
          {
            "full_code": "01700",
            "children": [
              {
                "description": "Examination and Diagnosis, Prosthodontic, Edentulous (a) Extended Examination of the Edentulous Mouth, including detailed Medical and Dental History (incl. prosthetic history), visual and digital examination of the oral structures, head and neck (incl. TMJ), lips, oral mucosa, tongue, oral pharynx, salivary glands and lymph nodes, and including evaluation for implant-supported or retained prosthesis.",
                "descriptor": "",
                "fee": "128.69",
                "full_code": "01701"
              },
              {
                "description": "Examination and Diagnosis, Prosthodontic, Specific",
                "descriptor": "",
                "fee": "86.94",
                "full_code": "01702"
              },
              {
                "description": "Examination and Diagnosis, Prosthodontic, Fixed Oral Rehabilitation, to include: (a) History, Medical and Dental (b) Clinical  Examination of Hard and Soft Tissues, including carious lesions, missing teeth, determination of sulcular depth, gingival contours, mobility of teeth, interproximal tooth contact relationships, occlusion of teeth, TMJ, pulp vitality test/analysis, where necessary and any other pertinent factors. (c) Evaluation of specific sites for implant-supported or retained prosthesis; (d) Radiographs extra, as required",
                "descriptor": "",
                "fee": "353.56",
                "full_code": "01703"
              }
            ],
            "descriptor": "",
            "description": "EXAMINATIONS AND DIAGNOSIS, PROSTHODONTIC",
            "fee": ""
          },
          {
            "full_code": "01800",
            "children": [
              {
                "description": "Examination and Diagnosis, Endodontic, Complete Endodontic examination and/or complicated diagnosis. Recording history, charting treatment planning and case history. Includes the following: (a) History, Medical and Dental (b) Clinical Examination and Diagnosis may include vitality test/analysis, thermal test/analysis, cracked tooth test/analysis, occlusal exams, percussion, palpation, transillumination, anaesthetic test/analysis and mobility test/analysis.",
                "descriptor": "",
                "fee": "189.86",
                "full_code": "01801"
              },
              {
                "description": "Examination and Diagnosis, Endodontic, Specific Endodontic examination and evaluation of a specific\nsituation in a localized area and vitality tests/analysis.",
                "descriptor": "",
                "fee": "118.5",
                "full_code": "01802"
              }
            ],
            "descriptor": "",
            "description": "EXAMINATION AND DIAGNOSIS, ENDODONTIC",
            "fee": ""
          },
          {
            "full_code": "01900",
            "children": [
              {
                "description": "Examination and Diagnosis, Orthodontic, General. To include:\n(a) Diagnostic models, complete intraoral radiograph series, or panoramic film, cephalograms, facial and intraoral photographs, consultation and case presentation.",
                "descriptor": "",
                "fee": "487.54",
                "full_code": "01901"
              },
              {
                "description": "Examination and Diagnosis, Orthodontic, Specific",
                "descriptor": "",
                "fee": "97.81",
                "full_code": "01902"
              }
            ],
            "descriptor": "",
            "description": "EXAMINATION AND DIAGNOSIS, ORTHODONTIC",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "EXAMINATION AND DIAGNOSIS, CLINICAL ORAL",
        "fee": ""
      },
      {
        "full_code": "02000",
        "children": [
          {
            "full_code": "02100",
            "children": [
              {
                "description": "Radiographs, Complete Series (minimum of 12 images incl. bitewings)",
                "descriptor": "",
                "fee": "228.54",
                "full_code": "02101"
              },
              {
                "description": "Radiographs, Complete Series (minimum of 16 images incl. bitewings)",
                "descriptor": "",
                "fee": "228.54",
                "full_code": "02102"
              },
              {
                "description": "Radiographs, Periapical",
                "descriptor": "",
                "fee": "",
                "full_code": "02110"
              },
              {
                "description": "Single image",
                "descriptor": "",
                "fee": "34.26",
                "full_code": "02111"
              },
              {
                "description": "Two images",
                "descriptor": "",
                "fee": "54.6",
                "full_code": "02112"
              },
              {
                "description": "Three images",
                "descriptor": "",
                "fee": "77.48",
                "full_code": "02113"
              },
              {
                "description": "Four images",
                "descriptor": "",
                "fee": "100.36",
                "full_code": "02114"
              },
              {
                "description": "Five images",
                "descriptor": "",
                "fee": "115.38",
                "full_code": "02115"
              },
              {
                "description": "Six images",
                "descriptor": "",
                "fee": "138.16",
                "full_code": "02116"
              },
              {
                "description": "Seven images",
                "descriptor": "",
                "fee": "162.64",
                "full_code": "02117"
              },
              {
                "description": "Eight images",
                "descriptor": "",
                "fee": "184.66",
                "full_code": "02118"
              },
              {
                "description": "Nine images",
                "descriptor": "",
                "fee": "206.68",
                "full_code": "02119"
              },
              {
                "description": "Ten images - The assignment to this service of a code ending in 0 is not aligned to the coding rules for the USC&LS. 02120 is nonetheless the appropriate code for the representation of this service",
                "descriptor": "",
                "fee": "217.77",
                "full_code": "02120"
              },
              {
                "description": "Radiographs, Occlusal",
                "descriptor": "",
                "fee": "",
                "full_code": "02130"
              },
              {
                "description": "Single image",
                "descriptor": "",
                "fee": "57.04",
                "full_code": "02131"
              },
              {
                "description": "Two images",
                "descriptor": "",
                "fee": "79.92",
                "full_code": "02132"
              },
              {
                "description": "Three images",
                "descriptor": "",
                "fee": "102.8",
                "full_code": "02133"
              },
              {
                "description": "Four images",
                "descriptor": "",
                "fee": "125.67",
                "full_code": "02134"
              },
              {
                "description": "Radiographs, Bitewing",
                "descriptor": "",
                "fee": "",
                "full_code": "02140"
              },
              {
                "description": "Single image",
                "descriptor": "",
                "fee": "34.26",
                "full_code": "02141"
              },
              {
                "description": "Two images",
                "descriptor": "",
                "fee": "54.6",
                "full_code": "02142"
              },
              {
                "description": "Three images",
                "descriptor": "",
                "fee": "77.48",
                "full_code": "02143"
              },
              {
                "description": "Four images",
                "descriptor": "",
                "fee": "100.36",
                "full_code": "02144"
              },
              {
                "description": "Five images",
                "descriptor": "",
                "fee": "115.38",
                "full_code": "02145"
              },
              {
                "description": "Six images",
                "descriptor": "",
                "fee": "138.16",
                "full_code": "02146"
              }
            ],
            "descriptor": "",
            "description": "RADIOGRAPHS, REGIONAL/LOCALIZED",
            "fee": ""
          },
          {
            "full_code": "02300",
            "children": [
              {
                "description": "Single image",
                "descriptor": "",
                "fee": "85.64",
                "full_code": "02301"
              },
              {
                "description": "Two images",
                "descriptor": "",
                "fee": "142.79",
                "full_code": "02302"
              },
              {
                "description": "Three images",
                "descriptor": "",
                "fee": "199.97",
                "full_code": "02303"
              },
              {
                "description": "Sinus Examination and Diagnosis - Minimum four images identified as:  1) Waters 2) Caldwell 3) Lateral Skull 4) Basal",
                "descriptor": "",
                "fee": "257.11",
                "full_code": "02304"
              },
              {
                "description": "Each additional image over four",
                "descriptor": "",
                "fee": "56.59",
                "full_code": "02309"
              }
            ],
            "descriptor": "",
            "description": "RADIOGRAPHS, POSTERO-ANTERIOR AND LATERAL SKULL AND FACIAL BONE",
            "fee": ""
          },
          {
            "full_code": "02400",
            "children": [
              {
                "description": "Single image",
                "descriptor": "",
                "fee": "85.66",
                "full_code": "02401"
              },
              {
                "description": "Two images",
                "descriptor": "",
                "fee": "142.79",
                "full_code": "02402"
              },
              {
                "description": "Each additional image over two",
                "descriptor": "",
                "fee": "56.59",
                "full_code": "02409"
              },
              {
                "description": "Radiopaque Dyes, Use of, To Demonstrate Lesions",
                "descriptor": "",
                "fee": "",
                "full_code": "02410"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "02411"
              },
              {
                "description": "Two units of time",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "02412"
              },
              {
                "description": "Each additional unit over two",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "02419"
              }
            ],
            "descriptor": "",
            "description": "RADIOGRAPHS, SIALOGRAPHY",
            "fee": ""
          },
          {
            "full_code": "02500",
            "children": [
              {
                "description": "Single image",
                "descriptor": "",
                "fee": "85.64",
                "full_code": "02501"
              },
              {
                "description": "Two images",
                "descriptor": "",
                "fee": "142.79",
                "full_code": "02502"
              },
              {
                "description": "Three images",
                "descriptor": "",
                "fee": "199.97",
                "full_code": "02503"
              },
              {
                "description": "Four images (minimum examination and diagnosis closed and open each side)",
                "descriptor": "",
                "fee": "257.11",
                "full_code": "02504"
              },
              {
                "description": "Each additional image over four",
                "descriptor": "",
                "fee": "56.59",
                "full_code": "02509"
              },
              {
                "description": "Arthrography of Temporo-mandibular joint",
                "descriptor": "",
                "fee": "",
                "full_code": "02510"
              },
              {
                "description": "Performing the Arthrographic Procedure",
                "descriptor": "",
                "fee": "283.08",
                "full_code": "02511"
              },
              {
                "description": "Interpretation of the Arthrogram",
                "descriptor": "",
                "fee": "",
                "full_code": "02520"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "85.79",
                "full_code": "02521"
              },
              {
                "description": "Each additional unit of time",
                "descriptor": "",
                "fee": "85.79",
                "full_code": "02529"
              }
            ],
            "descriptor": "",
            "description": "RADIOGRAPHS, TEMPOROMANDIBULAR JOINT",
            "fee": ""
          },
          {
            "full_code": "02600",
            "children": [
              {
                "description": "Single image",
                "descriptor": "",
                "fee": "101.53",
                "full_code": "02601"
              }
            ],
            "descriptor": "",
            "description": "RADIOGRAPHS, PANORAMIC",
            "fee": ""
          },
          {
            "full_code": "02700",
            "children": [
              {
                "description": "Single image",
                "descriptor": "",
                "fee": "136.65",
                "full_code": "02701"
              },
              {
                "description": "Two images",
                "descriptor": "",
                "fee": "214.26",
                "full_code": "02702"
              },
              {
                "description": "Radiographs, Cephalometric, Tracing and Interpretation",
                "descriptor": "",
                "fee": "",
                "full_code": "02750"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "94.36",
                "full_code": "02751"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "188.72",
                "full_code": "02752"
              },
              {
                "description": "Each additional unit over  two",
                "descriptor": "",
                "fee": "94.36",
                "full_code": "02759"
              }
            ],
            "descriptor": "",
            "description": "RADIOGRAPHS, CEPHALOMETRIC",
            "fee": ""
          },
          {
            "full_code": "02800",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "+PS",
                "fee": "105.01",
                "full_code": "02801"
              },
              {
                "description": "Two units",
                "descriptor": "+PS",
                "fee": "210.02",
                "full_code": "02802"
              },
              {
                "description": "Each additional unit over two",
                "descriptor": "+PS",
                "fee": "105.01",
                "full_code": "02809"
              }
            ],
            "descriptor": "",
            "description": "RADIOGRAPHS, COMPUTERIZED AXIAL TOMOGRAMS (CT), POSITRON EMISSION TOMOGRAPHY\n(P.E.T), MAGNETIC RESONANCE IMAGES (M.R.I) INTERPRETATION (either the radiographs, CT scans, PET scans, MRI scans, or the interpretation must be received from another source)",
            "fee": ""
          },
          {
            "full_code": "02900",
            "children": [
              {
                "description": "Radiographs, Duplications",
                "descriptor": "",
                "fee": "",
                "full_code": "02910"
              },
              {
                "description": "Single image",
                "descriptor": "",
                "fee": "6.51",
                "full_code": "02911"
              },
              {
                "description": "Two images",
                "descriptor": "",
                "fee": "12.93",
                "full_code": "02912"
              },
              {
                "description": "Three images",
                "descriptor": "",
                "fee": "19.39",
                "full_code": "02913"
              },
              {
                "description": "Four  images",
                "descriptor": "",
                "fee": "25.86",
                "full_code": "02914"
              },
              {
                "description": "Five  images",
                "descriptor": "",
                "fee": "32.32",
                "full_code": "02915"
              },
              {
                "description": "Six  images",
                "descriptor": "",
                "fee": "38.78",
                "full_code": "02916"
              },
              {
                "description": "Seven images",
                "descriptor": "",
                "fee": "45.27",
                "full_code": "02917"
              },
              {
                "description": "Eight images",
                "descriptor": "",
                "fee": "50.11",
                "full_code": "02918"
              },
              {
                "description": "Each additional image over eight",
                "descriptor": "",
                "fee": "6.51",
                "full_code": "02919"
              },
              {
                "description": "Radiographs, Tomography",
                "descriptor": "",
                "fee": "",
                "full_code": "02930"
              },
              {
                "description": "Single view",
                "descriptor": "",
                "fee": "136.65",
                "full_code": "02931"
              },
              {
                "description": "Two views",
                "descriptor": "",
                "fee": "214.33",
                "full_code": "02932"
              },
              {
                "description": "Three views",
                "descriptor": "",
                "fee": "288.12",
                "full_code": "02933"
              },
              {
                "description": "Four views",
                "descriptor": "",
                "fee": "357.07",
                "full_code": "02934"
              },
              {
                "description": "Each additional view over four",
                "descriptor": "",
                "fee": "56.59",
                "full_code": "02939"
              },
              {
                "description": "Radiographs, Hand and Wrist",
                "descriptor": "",
                "fee": "",
                "full_code": "02940"
              },
              {
                "description": "Radiographs, Hand and Wrist (as a diagnostic aid for dental treatment) per case",
                "descriptor": "",
                "fee": "136.65",
                "full_code": "02941"
              },
              {
                "description": "Radiographic Guide, (includes diagnostic wax-up, with radio-opaque markers for pre-surgical assessment of alveolar bone and vital structures as potential osseo-integrated implant site(s))",
                "descriptor": "",
                "fee": "",
                "full_code": "02950"
              },
              {
                "description": "Maxillary Guide",
                "descriptor": "+L +E",
                "fee": "I.C.",
                "full_code": "02951"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L +E",
                "fee": "I.C.",
                "full_code": "02952"
              }
            ],
            "descriptor": "",
            "description": "RADIOGRAPHS, OTHER"
          }
        ],
        "descriptor": "",
        "description": "RADIOGRAPHS (including radiographic examination and diagnosis and interpretation)"
      },
      {
        "full_code": "03000",
        "children": [
          {
            "full_code": "03001",
            "children": [
              {
                "description": "Mandibular Template",
                "descriptor": "+L +E",
                "fee": "85.79",
                "full_code": "03002"
              }
            ],
            "descriptor": "+L +E",
            "description": "Maxillary Template",
            "fee": "85.79"
          }
        ],
        "descriptor": "",
        "description": "TEMPLATE, SURGICAL (includes diagnostic wax-up. Also used to locate and orient osseo-integrated\nimplants)",
        "fee": ""
      },
      {
        "full_code": "04000",
        "children": [
          {
            "full_code": "04100",
            "children": [
              {
                "description": "Microbiological Test/Analysis for the Determination of Pathological Agents",
                "descriptor": "+L",
                "fee": "81.5",
                "full_code": "04101"
              }
            ],
            "descriptor": "",
            "description": "Test/Analysis, Microbiological (technical procedure only)",
            "fee": ""
          },
          {
            "full_code": "04200",
            "children": [
              {
                "description": "Bacteriological Test/Analysis for the Determination of Dental Caries Susceptibility (technical procedure\nonly)",
                "descriptor": "+L",
                "fee": "81.5",
                "full_code": "04201"
              },
              {
                "description": "Non-ionizing scanning procedure to detect caries and capable of quantifying, monitoring and recording changes in enamel, dentin, and cementum, which includes diagnosis and interpretation of findings.",
                "descriptor": "",
                "fee": "",
                "full_code": "04220"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "34.26",
                "full_code": "04221"
              },
              {
                "description": "One half unit of time",
                "descriptor": "",
                "fee": "17.13",
                "full_code": "04227"
              }
            ],
            "descriptor": "",
            "description": "Test/Analysis, Caries Susceptibility/Diagnosis",
            "fee": ""
          },
          {
            "full_code": "04300",
            "children": [
              {
                "description": "Test/Analysis, Histopathological, Soft Tissue",
                "descriptor": "",
                "fee": "",
                "full_code": "04310"
              },
              {
                "description": "Biopsy, Soft Oral Tissue - by Puncture",
                "descriptor": "+L",
                "fee": "94.36",
                "full_code": "04311"
              },
              {
                "description": "Biopsy, Soft Oral Tissue - by Incision",
                "descriptor": "+L",
                "fee": "94.36",
                "full_code": "04312"
              },
              {
                "description": "Biopsy, Soft Oral Tissue - by Aspiration",
                "descriptor": "+L",
                "fee": "94.36",
                "full_code": "04313"
              },
              {
                "description": "Test/Analysis, Histopathological, Hard Tissue",
                "descriptor": "",
                "fee": "",
                "full_code": "04320"
              },
              {
                "description": "Biopsy, Hard Oral Tissue - by Puncture",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "04321"
              },
              {
                "description": "Biopsy, Hard Oral Tissue - by Incision",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "04322"
              },
              {
                "description": "Biopsy, Hard Oral Tissue - by Aspiration",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "04323"
              }
            ],
            "descriptor": "",
            "description": "TEST/ANALYSIS, HISTOPATHOLOGICAL (technical procedure only)"
          },
          {
            "full_code": "04400",
            "children": [
              {
                "description": "Cytological Smear from the Oral Cavity",
                "descriptor": "+L+E",
                "fee": "81.5",
                "full_code": "04401"
              },
              {
                "description": "Vital Staining of Oral Mucosal Tissues",
                "descriptor": "+E",
                "fee": "81.5",
                "full_code": "04402"
              }
            ],
            "descriptor": "",
            "description": "TEST/ANALYSIS, CYTOLOGICAL (technical procedure only)",
            "fee": ""
          },
          {
            "full_code": "04500",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "81.5",
                "full_code": "04501"
              },
              {
                "description": "Each additional unit",
                "descriptor": "",
                "fee": "81.5",
                "full_code": "04509"
              }
            ],
            "descriptor": "",
            "description": "TESTS/ANALYSIS, PULP VITALITY AND INTERPRETATION",
            "fee": ""
          },
          {
            "full_code": "04600",
            "children": [
              {
                "description": "Interpretation and/or Report, Microbiological by Oral Microbiologist\n",
                "descriptor": "+L",
                "fee": "81.49 to 244.54",
                "full_code": "04601"
              },
              {
                "description": "Interpretation and/or Report, Histopathological by Oral Pathologist or Microbiologist\n",
                "descriptor": "+L",
                "fee": "94.36 to 283.08",
                "full_code": "04602"
              },
              {
                "description": "Interpretation and/or Report, Cytological by Oral Pathologist",
                "descriptor": "+L",
                "fee": "81.5",
                "full_code": "04603"
              },
              {
                "description": "Reports, Other",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "04604"
              }
            ],
            "descriptor": "",
            "description": "INTERPRETATION AND/OR REPORTS, LABORATORY",
            "fee": ""
          },
          {
            "full_code": "04700",
            "children": [
              {
                "description": "Equilibration, Casts Diagnostic (Pilot Equilibration) For Extensive Or Complicated Restorative\nDentistry",
                "descriptor": "",
                "fee": "",
                "full_code": "04710"
              },
              {
                "description": "One unit of time",
                "descriptor": "+L",
                "fee": "85.8",
                "full_code": "04711"
              },
              {
                "description": "Two units",
                "descriptor": "+L",
                "fee": "171.6",
                "full_code": "04712"
              },
              {
                "description": "Three units",
                "descriptor": "+L",
                "fee": "257.4",
                "full_code": "04713"
              },
              {
                "description": "Four units",
                "descriptor": "+L",
                "fee": "343.2",
                "full_code": "04714"
              },
              {
                "description": "Each additional unit over four",
                "descriptor": "+L",
                "fee": "85.8",
                "full_code": "04719"
              },
              {
                "description": "Wax-up, Diagnostic (To Evaluate Cosmetic And/Or Preparation Design And/Or Occlusal\nConsiderations) (Gnathological Wax-up)",
                "descriptor": "",
                "fee": "",
                "full_code": "04720"
              },
              {
                "description": "One unit of time",
                "descriptor": "+L",
                "fee": "85.8",
                "full_code": "04721"
              },
              {
                "description": "Two units",
                "descriptor": "+L",
                "fee": "171.6",
                "full_code": "04722"
              },
              {
                "description": "Three units",
                "descriptor": "+L",
                "fee": "257.4",
                "full_code": "04723"
              },
              {
                "description": "Four units",
                "descriptor": "+L",
                "fee": "343.2",
                "full_code": "04724"
              },
              {
                "description": "Each additional unit over four",
                "descriptor": "+L",
                "fee": "85.8",
                "full_code": "04729"
              },
              {
                "description": "Split Cast Mounting, Diagnostic",
                "descriptor": "",
                "fee": "",
                "full_code": "04730"
              },
              {
                "description": "One unit of time",
                "descriptor": "+L",
                "fee": "85.8",
                "full_code": "04731"
              },
              {
                "description": "Two units",
                "descriptor": "+L",
                "fee": "171.6",
                "full_code": "04732"
              },
              {
                "description": "Three units",
                "descriptor": "+L",
                "fee": "257.4",
                "full_code": "04733"
              },
              {
                "description": "Four units",
                "descriptor": "+L",
                "fee": "343.2",
                "full_code": "04734"
              },
              {
                "description": "Each additional unit over four",
                "descriptor": "+L",
                "fee": "85.8",
                "full_code": "04739"
              },
              {
                "description": "Interpretation of Diagnostic Casts",
                "descriptor": "",
                "fee": "",
                "full_code": "04740"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "82.64",
                "full_code": "04741"
              },
              {
                "description": "Each additional unit",
                "descriptor": "",
                "fee": "82.64",
                "full_code": "04749"
              }
            ],
            "descriptor": "",
            "description": "SUPPLEMENTARY DIAGNOSTIC PROCEDURES (INTERPRETATION ONLY)"
          },
          {
            "full_code": "04800",
            "children": [
              {
                "description": "Photographs, diagnotic (technical procedure only)",
                "descriptor": "",
                "fee": "",
                "full_code": "04810"
              },
              {
                "description": "Single photograph",
                "descriptor": "",
                "fee": "19.55",
                "full_code": "04811"
              },
              {
                "description": "Two photos",
                "descriptor": "",
                "fee": "37.05",
                "full_code": "04812"
              },
              {
                "description": "Three photos",
                "descriptor": "",
                "fee": "55.58",
                "full_code": "04813"
              },
              {
                "description": "Each additional photo over three",
                "descriptor": "",
                "fee": "19.55",
                "full_code": "04819"
              }
            ],
            "descriptor": "",
            "description": "VISUAL IMAGING, DIAGNOSTIC"
          },
          {
            "full_code": "04900",
            "children": [
              {
                "description": "Cast, Diagnostic, Unmounted",
                "descriptor": "",
                "fee": "",
                "full_code": "04910"
              },
              {
                "description": "Cast, Diagnostic, Unmounted",
                "descriptor": "+L",
                "fee": "91.84",
                "full_code": "04911"
              },
              {
                "description": "Cast, Diagnostic, Unmounted, Duplicate",
                "descriptor": "+L",
                "fee": "40.75",
                "full_code": "04912"
              },
              {
                "description": "Casts, Diagnostic, Unmounted, Upper and Lower Combined",
                "descriptor": "+L",
                "fee": "192.91",
                "full_code": "04913"
              },
              {
                "description": "Casts, Diagnostic, Mounted",
                "descriptor": "",
                "fee": "",
                "full_code": "04920"
              },
              {
                "description": "Casts, Diagnostic, Mounted",
                "descriptor": "+L",
                "fee": "144.11",
                "full_code": "04921"
              },
              {
                "description": "Casts, Diagnostic, Mounted, using face bow transfer",
                "descriptor": "+L",
                "fee": "191.76",
                "full_code": "04922"
              },
              {
                "description": "Casts, Diagnostic, Mounted, using face bow and occlusal records",
                "descriptor": "+L",
                "fee": "378.69",
                "full_code": "04923"
              },
              {
                "description": "Casts, Diagnostic, Mounted using fully adjustable articulator (used with 04941 and 04942)",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "04924"
              },
              {
                "description": "Casts, Diagnostic, Orthodontic",
                "descriptor": "",
                "fee": "",
                "full_code": "04930"
              },
              {
                "description": "Casts, Diagnostic, Orthodontic (unmounted, angle trimmed and soaped)",
                "descriptor": "+L",
                "fee": "163.02",
                "full_code": "04931"
              },
              {
                "description": "Casts, Diagnostic, Miscellaneous Procedures",
                "descriptor": "",
                "fee": "",
                "full_code": "04940"
              },
              {
                "description": "Transverse Axis Location and Transfer, used in conjunction with 04922, 04923, and 04924",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "04941"
              },
              {
                "description": "Three Dimensional Recordings of Patient's Dynamic Movements for Programming of Fully Adjustable\nArticulators",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "04942"
              },
              {
                "description": "Custom Incisal Guide Table",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "04943"
              }
            ],
            "descriptor": "",
            "description": "CASTS, DIAGNOSTIC (technical procedure only)"
          }
        ],
        "descriptor": "",
        "description": "TEST/ANALYSIS/LABORATORY PROCEDURES/INTERPRETATION AND/OR REPORTS"
      },
      {
        "full_code": "05000",
        "children": [
          {
            "full_code": "05100",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "85.8",
                "full_code": "05101"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "171.6",
                "full_code": "05102"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "257.4",
                "full_code": "05103"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "343.2",
                "full_code": "05104"
              },
              {
                "description": "Each additional unit over four",
                "descriptor": "",
                "fee": "85.8",
                "full_code": "05109"
              }
            ],
            "descriptor": "",
            "description": "TREATMENT PLANNING (This service is only for extra time spent on unusually complicated cases or where the patient demands unusual time in explanation or where diagnostic material is received from another source.  Usual case presentation time and usual treatment planning time are implicit in the examination fee and diagnosis fee in the radiographic interpretation fee.)",
            "fee": ""
          },
          {
            "full_code": "05200",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "89.24",
                "full_code": "05201"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "178.48",
                "full_code": "05202"
              },
              {
                "description": "Each additional unit over two",
                "descriptor": "",
                "fee": "89.24",
                "full_code": "05209"
              }
            ],
            "descriptor": "",
            "description": "CONSULTATION, with patient",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "CASE PRESENTATION/TREATMENT PLANNING"
      },
      {
        "full_code": "07000",
        "children": [
          {
            "full_code": "07010",
            "children": [
              {
                "description": "Small field of view (e.g. sextant or part of; isolated temporomandibular joint)",
                "descriptor": "",
                "fee": "114.2",
                "full_code": "07011"
              },
              {
                "description": "Large field of view (1 arch)",
                "descriptor": "",
                "fee": "136.65",
                "full_code": "07012"
              },
              {
                "description": "Large field of view (2 arches)",
                "descriptor": "",
                "fee": "214.33",
                "full_code": "07013"
              },
              {
                "description": "Radiographs, CBCT, Image Processing",
                "descriptor": "",
                "fee": "",
                "full_code": "07020"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "07021"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "07022"
              },
              {
                "description": "One half unit of time",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "07027"
              },
              {
                "description": "Each additional unit over two",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "07029"
              },
              {
                "description": "Radiographs, CBCT, Interpretation",
                "descriptor": "",
                "fee": "",
                "full_code": "07030"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "94.36",
                "full_code": "07031"
              },
              {
                "description": "Two units of time",
                "descriptor": "",
                "fee": "188.72",
                "full_code": "07032"
              },
              {
                "description": "One half unit of time",
                "descriptor": "",
                "fee": "47.18",
                "full_code": "07037"
              },
              {
                "description": "Each additional unit over two",
                "descriptor": "",
                "fee": "94.36",
                "full_code": "07039"
              },
              {
                "description": "Radiographs, CBCT, Acquisition, Processing and Interpretation",
                "descriptor": "",
                "fee": "",
                "full_code": "07040"
              },
              {
                "description": "Small field of view (sextant or part of; isolated temporomandibular joint)",
                "descriptor": "",
                "fee": "208.56",
                "full_code": "07041"
              },
              {
                "description": "Large field of view (1 arch)",
                "descriptor": "",
                "fee": "231",
                "full_code": "07042"
              },
              {
                "description": "Large field of view (2 arches)",
                "descriptor": "",
                "fee": "308.68",
                "full_code": "07043"
              }
            ],
            "descriptor": "",
            "description": "Radiographs, CBCT, Acquisition",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "RADIOGRAPHS, CONE BEAM COMPUTERIZED TOMOGRAPHY (CBCT)",
        "fee": ""
      },
      {
        "full_code": "08000",
        "children": [
          {
            "full_code": "08010",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "89.24",
                "full_code": "08011"
              },
              {
                "description": "Two units of time",
                "descriptor": "",
                "fee": "178.48",
                "full_code": "08012"
              },
              {
                "description": "Each additional unit over two",
                "descriptor": "",
                "fee": "89.24",
                "full_code": "08019"
              }
            ],
            "descriptor": "",
            "description": "Of chief complaint",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "REMOTE ASSESSMENT \u2022 Codes in the 08010 series: May be used for consultations with patients exceeding 7.5 minutes, utilizing a remote dentistry platform. The code includes verifying patient identity, informed consent, review of medical and clinical history, assessment of the clinical situation, interim diagnosis, remote management (e.g.: calling in a prescription, appropriate referral etc.), appropriate documentation and subsequent follow up calls. \u2022 Use of this code series will only be authorized for the use of remote dentistry during the Covid-19 Pandemic and State of Public Health Emergency in Alberta, and its use will not be authorized in any other setting or circumstances",
        "fee": ""
      }
    ],
    "descriptor": "",
    "description": "DIAGNOSTIC"
  },
  {
    "full_code": "10000",
    "children": [
      {
        "full_code": "11100",
        "children": [
          {
            "full_code": "11101",
            "children": [
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "137.58",
                "full_code": "11102"
              },
              {
                "description": "One half unit",
                "descriptor": "",
                "fee": "34.4",
                "full_code": "11107"
              },
              {
                "description": "SCALING",
                "descriptor": "",
                "fee": "",
                "full_code": "11110"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "77.21",
                "full_code": "11111"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "154.42",
                "full_code": "11112"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "231.63",
                "full_code": "11113"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "308.84",
                "full_code": "11114"
              },
              {
                "description": "Five units",
                "descriptor": "",
                "fee": "386.05",
                "full_code": "11115"
              },
              {
                "description": "Six units",
                "descriptor": "",
                "fee": "463.26",
                "full_code": "11116"
              },
              {
                "description": "One half unit",
                "descriptor": "",
                "fee": "38.61",
                "full_code": "11117"
              },
              {
                "description": "Each Additional unit over six",
                "descriptor": "",
                "fee": "77.21",
                "full_code": "11119"
              }
            ],
            "descriptor": "",
            "description": "One unit of time",
            "fee": "68.79"
          }
        ],
        "descriptor": "",
        "description": "POLISHING",
        "fee": ""
      },
      {
        "full_code": "12100",
        "children": [
          {
            "full_code": "12110",
            "children": [
              {
                "description": "Rinse",
                "descriptor": "",
                "fee": "33.33",
                "full_code": "12111"
              },
              {
                "description": "Gel or Foam",
                "descriptor": "",
                "fee": "33.33",
                "full_code": "12112"
              },
              {
                "description": "Varnish",
                "descriptor": "",
                "fee": "33.33",
                "full_code": "12113"
              },
              {
                "description": "Self-Administered Brush-In, supervised",
                "descriptor": "",
                "fee": "33.33",
                "full_code": "12114"
              }
            ],
            "descriptor": "",
            "description": "Topical, Whole Mouth, in office",
            "fee": ""
          },
          {
            "full_code": "12600",
            "children": [
              {
                "description": "Fluoride, Custom Appliance - Maxillary Arch",
                "descriptor": "+L",
                "fee": "81.5",
                "full_code": "12601"
              },
              {
                "description": "Fluoride, Custom Appliance - Mandibular Arch",
                "descriptor": "+L",
                "fee": "81.5",
                "full_code": "12602"
              }
            ],
            "descriptor": "",
            "description": "FLUORIDE, CUSTOM APPLIANCES, (home application)",
            "fee": ""
          },
          {
            "full_code": "12700",
            "children": [
              {
                "description": "Medication, Custom Appliance - Maxillary Arch",
                "descriptor": "+L",
                "fee": "81.5",
                "full_code": "12701"
              },
              {
                "description": "Medication, Custom Appliance - Mandibular Arch",
                "descriptor": "+L",
                "fee": "81.5",
                "full_code": "12702"
              }
            ],
            "descriptor": "",
            "description": "MEDICATION, CUSTOM APPLIANCE",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "FLUORIDE TREATMENTS (whole mouth)",
        "fee": ""
      },
      {
        "full_code": "13000",
        "children": [
          {
            "full_code": "13100",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "81.5",
                "full_code": "13101"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "163",
                "full_code": "13102"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "244.5",
                "full_code": "13103"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "326",
                "full_code": "13104"
              },
              {
                "description": "Each additional unit over four",
                "descriptor": "",
                "fee": "81.5",
                "full_code": "13109"
              }
            ],
            "descriptor": "",
            "description": "NUTRITIONAL COUNSELLING Including: recording and analysis of up to seven-day dietary intake and consultation",
            "fee": ""
          },
          {
            "full_code": "13200",
            "children": [
              {
                "description": "Individual Instruction (One Instructor To One Patient) - Excluding Audio-Visual Time",
                "descriptor": "",
                "fee": "",
                "full_code": "13210"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "81.5",
                "full_code": "13211"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "163",
                "full_code": "13212"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "244.5",
                "full_code": "13213"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "326",
                "full_code": "13214"
              },
              {
                "description": "One half of unit",
                "descriptor": "",
                "fee": "40.75",
                "full_code": "13217"
              },
              {
                "description": "Each additional unit over four",
                "descriptor": "",
                "fee": "81.5",
                "full_code": "13219"
              },
              {
                "description": "Group Instruction - Excluding Audio-Visual Time",
                "descriptor": "",
                "fee": "",
                "full_code": "13220"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "81.5",
                "full_code": "13221"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "163",
                "full_code": "13222"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "244.5",
                "full_code": "13223"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "326",
                "full_code": "13224"
              },
              {
                "description": "Each additional unit over four",
                "descriptor": "",
                "fee": "81.5",
                "full_code": "13229"
              },
              {
                "description": "Re-Instruction (Within 6 Months) - Excluding Audio-Visual Time",
                "descriptor": "",
                "fee": "",
                "full_code": "13230"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "81.5",
                "full_code": "13231"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "163",
                "full_code": "13232"
              },
              {
                "description": "Each additional unit over two",
                "descriptor": "",
                "fee": "81.5",
                "full_code": "13239"
              },
              {
                "description": "Oral Hygiene Instruction - Audio-Visual",
                "descriptor": "",
                "fee": "",
                "full_code": "13240"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "81.5",
                "full_code": "13241"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "163",
                "full_code": "13242"
              },
              {
                "description": "Each additional unit over two",
                "descriptor": "",
                "fee": "81.5",
                "full_code": "13249"
              }
            ],
            "descriptor": "",
            "description": "ORAL HYGIENE INSTRUCTION/PLAQUE CONTROL To include:  brushing and/or flossing and/or embrasure cleaning."
          },
          {
            "full_code": "13400",
            "children": [
              {
                "description": "First tooth",
                "descriptor": "",
                "fee": "37.51",
                "full_code": "13401"
              },
              {
                "description": "Each additional tooth same quadrant",
                "descriptor": "",
                "fee": "18.76",
                "full_code": "13409"
              },
              {
                "description": "Preventive Restorative Resin (procedure that involves some preparation of the pits and/or fissures in\ntooth enamel and may extend into dentin in limited areas)",
                "descriptor": "",
                "fee": "",
                "full_code": "13410"
              },
              {
                "description": "First tooth",
                "descriptor": "",
                "fee": "83.41",
                "full_code": "13411"
              },
              {
                "description": "Each additional tooth same quadrant",
                "descriptor": "",
                "fee": "78.81",
                "full_code": "13419"
              }
            ],
            "descriptor": "",
            "description": "SEALANTS, PIT AND FISSURE (Mechanical and/or chemical preparation included)",
            "fee": ""
          },
          {
            "full_code": "13600",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "+E",
                "fee": "81.5",
                "full_code": "13601"
              },
              {
                "description": "Two units",
                "descriptor": "+E",
                "fee": "163",
                "full_code": "13602"
              },
              {
                "description": "Each additional unit over two",
                "descriptor": "",
                "fee": "81.5",
                "full_code": "13609"
              }
            ],
            "descriptor": "",
            "description": "TOPICAL APPLICATION TO HARD TISSUE LESION(S) OF AN ANTIMICROBIAL OR REMINERALIZATION\nAGENT",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "PREVENTIVE SERVICES, OTHER"
      },
      {
        "full_code": "14000",
        "children": [
          {
            "full_code": "14100",
            "children": [
              {
                "description": "Appliance, Maxillary",
                "descriptor": "+L",
                "fee": "601.79",
                "full_code": "14101"
              },
              {
                "description": "Appliance, Mandibular",
                "descriptor": "+L",
                "fee": "601.79",
                "full_code": "14102"
              }
            ],
            "descriptor": "",
            "description": "APPLIANCES, REMOVABLE, CONTROL OF ORAL HABITS",
            "fee": ""
          },
          {
            "full_code": "14200",
            "children": [
              {
                "description": "Appliance, Maxillary",
                "descriptor": "+L",
                "fee": "660.51",
                "full_code": "14201"
              },
              {
                "description": "Appliance, Mandibular",
                "descriptor": "+L",
                "fee": "660.51",
                "full_code": "14202"
              }
            ],
            "descriptor": "",
            "description": "APPLIANCES, FIXED/CEMENTED, CONTROL OF ORAL HABITS",
            "fee": ""
          },
          {
            "full_code": "14300",
            "children": [
              {
                "description": "Motivation of Patient - Psychological Approach (e.g. thumb sucking, lip biting, etc.) - per visit",
                "descriptor": "+L",
                "fee": "94.36",
                "full_code": "14301"
              },
              {
                "description": "Myofunctional Therapy (e.g. to correct mouth breathing, abnormal swallowing, tongue thrust, etc.)",
                "descriptor": "",
                "fee": "",
                "full_code": "14310"
              },
              {
                "description": "First unit of time per visit",
                "descriptor": "+L",
                "fee": "94.36",
                "full_code": "14311"
              },
              {
                "description": "Two units",
                "descriptor": "+L",
                "fee": "188.72",
                "full_code": "14312"
              },
              {
                "description": "Each additional unit over two",
                "descriptor": "+L",
                "fee": "94.36",
                "full_code": "14319"
              }
            ],
            "descriptor": "",
            "description": "CONTROL OF ORAL HABITS, MISCELLANEOUS",
            "fee": ""
          },
          {
            "full_code": "14400",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "+L",
                "fee": "94.36",
                "full_code": "14401"
              },
              {
                "description": "Two units of time",
                "descriptor": "+L",
                "fee": "188.72",
                "full_code": "14402"
              },
              {
                "description": "Three units of time",
                "descriptor": "+L",
                "fee": "283.08",
                "full_code": "14403"
              },
              {
                "description": "Each additional unit over three",
                "descriptor": "+L",
                "fee": "94.36",
                "full_code": "14409"
              }
            ],
            "descriptor": "",
            "description": "APPLIANCES, CONTROL OF ORAL HABITS ADJUSTMENTS, REPAIRS, MAINTENANCE",
            "fee": ""
          },
          {
            "full_code": "14500",
            "children": [
              {
                "description": "Appliance, Protected Mouth Guards, Preformed",
                "descriptor": "",
                "fee": "97.59",
                "full_code": "14501"
              },
              {
                "description": "Appliance, Protective Mouth Guards, Processed",
                "descriptor": "+L",
                "fee": "106.78",
                "full_code": "14502"
              }
            ],
            "descriptor": "",
            "description": "APPLIANCES, PROTECTIVE MOUTH GUARDS",
            "fee": ""
          },
          {
            "full_code": "14600",
            "children": [
              {
                "description": "Appliances, Periodontal (including bruxism appliance); Includes Impression, Insertion and Insertion\nAdjustment (no post-insertion adjustments)",
                "descriptor": "",
                "fee": "",
                "full_code": "14610"
              },
              {
                "description": "Maxillary Appliance",
                "descriptor": "+L",
                "fee": "481.22",
                "full_code": "14611"
              },
              {
                "description": "Mandibular Appliance",
                "descriptor": "+L",
                "fee": "481.23",
                "full_code": "14612"
              },
              {
                "description": "Appliances, Adjustment, Repair",
                "descriptor": "",
                "fee": "",
                "full_code": "14620"
              },
              {
                "description": "One unit of time",
                "descriptor": "+L",
                "fee": "87.5",
                "full_code": "14621"
              },
              {
                "description": "Two units",
                "descriptor": "+L",
                "fee": "175",
                "full_code": "14622"
              },
              {
                "description": "Three units",
                "descriptor": "+L",
                "fee": "262.5",
                "full_code": "14623"
              },
              {
                "description": "Each additional unit over three",
                "descriptor": "+L",
                "fee": "87.5",
                "full_code": "14629"
              },
              {
                "description": "Appliances, Reline",
                "descriptor": "",
                "fee": "",
                "full_code": "14630"
              },
              {
                "description": "Reline, Direct",
                "descriptor": "",
                "fee": "262.53",
                "full_code": "14631"
              },
              {
                "description": "Reline, Processed",
                "descriptor": "+L",
                "fee": "262.53",
                "full_code": "14632"
              }
            ],
            "descriptor": "",
            "description": "APPLIANCES, PERIODONTAL (see separate code for control of Oral Habits 14000, Protective Mouth Guards 14500, TMJ 14700 and TMJ appliances 78700)"
          },
          {
            "full_code": "14700",
            "children": [
              {
                "description": "Appliance, TMJ, Diagnostic and/or Therapeutic, includes impression, insertion and insertion\nadjustment (no post-insertion adjustments)",
                "descriptor": "",
                "fee": "",
                "full_code": "14710"
              },
              {
                "description": "Maxillary Appliance",
                "descriptor": "+L",
                "fee": "708.45",
                "full_code": "14711"
              },
              {
                "description": "Mandibular Appliance",
                "descriptor": "+L",
                "fee": "708.45",
                "full_code": "14712"
              },
              {
                "description": "Appliance, TMJ Intraoral Repositioning; includes impression, insertion and insertion adjustment (no\npost-insertion adjustments)",
                "descriptor": "",
                "fee": "",
                "full_code": "14720"
              },
              {
                "description": "Maxillary Appliance",
                "descriptor": "+L",
                "fee": "708.45",
                "full_code": "14721"
              },
              {
                "description": "Mandibular Appliance",
                "descriptor": "+L",
                "fee": "708.45",
                "full_code": "14722"
              },
              {
                "description": "Appliance, TMJ, Periodic Maintenance, Adjustments, Repairs",
                "descriptor": "",
                "fee": "",
                "full_code": "14730"
              },
              {
                "description": "One unit of time",
                "descriptor": "+L",
                "fee": "91.87",
                "full_code": "14731"
              },
              {
                "description": "Two units",
                "descriptor": "+L",
                "fee": "183.74",
                "full_code": "14732"
              },
              {
                "description": "Three units",
                "descriptor": "+L",
                "fee": "275.61",
                "full_code": "14733"
              },
              {
                "description": "Each additional unit over three",
                "descriptor": "+L",
                "fee": "91.87",
                "full_code": "14739"
              },
              {
                "description": "Appliance, TMJ, Reline",
                "descriptor": "",
                "fee": "",
                "full_code": "14740"
              },
              {
                "description": "Reline, Direct",
                "descriptor": "",
                "fee": "262.53",
                "full_code": "14741"
              },
              {
                "description": "Reline, Indirect",
                "descriptor": "+L",
                "fee": "262.53",
                "full_code": "14742"
              }
            ],
            "descriptor": "",
            "description": "APPLIANCES, TEMPOROMANDIBULAR JOINT"
          },
          {
            "full_code": "14800",
            "children": [
              {
                "description": "Appliance, Myofascial Pain Dysfunction Syndrome, (to include: models, gnathological determinants) Appliance Construction only, and insertion adjustment (no post-insertion adjustments)",
                "descriptor": "",
                "fee": "",
                "full_code": "14810"
              },
              {
                "description": "Maxillary Appliance",
                "descriptor": "+L",
                "fee": "799.53",
                "full_code": "14811"
              },
              {
                "description": "Mandibular Appliance",
                "descriptor": "+L",
                "fee": "799.53",
                "full_code": "14812"
              },
              {
                "description": "Appliance, Myofascial Pain Dysfunction Syndrome, Periodic Maintenance, Adjustment and Repairs",
                "descriptor": "",
                "fee": "",
                "full_code": "14820"
              },
              {
                "description": "One unit of time",
                "descriptor": "+L",
                "fee": "91.87",
                "full_code": "14821"
              },
              {
                "description": "Two units",
                "descriptor": "+L",
                "fee": "183.74",
                "full_code": "14822"
              },
              {
                "description": "Three units",
                "descriptor": "+L",
                "fee": "275.61",
                "full_code": "14823"
              },
              {
                "description": "Each additional unit over three",
                "descriptor": "+L",
                "fee": "91.87",
                "full_code": "14829"
              }
            ],
            "descriptor": "",
            "description": "APPLIANCES, MYOFASCIAL PAIN DYSFUNCTION SYNDROME (conditions that originate outside the temporomandibular joint)"
          },
          {
            "full_code": "14900",
            "children": [
              {
                "description": "Appliance, Intraoral, For the Treatment of Obstructive Airway Disorders, Ridge or Tooth Supported",
                "descriptor": "+L",
                "fee": "849.26",
                "full_code": "14901"
              },
              {
                "description": "Appliance, Tongue Retaining Device, for the Treatment of Obstructive Airway Disorders",
                "descriptor": "+E",
                "fee": "481.22",
                "full_code": "14902"
              },
              {
                "description": "Appliance, Intraoral, For the Treatment of Obstructive Airway Disorders, Periodic Maintenance,\nAdjustment and Repairs",
                "descriptor": "",
                "fee": "",
                "full_code": "14910"
              },
              {
                "description": "One unit of time",
                "descriptor": "+L",
                "fee": "94.36",
                "full_code": "14911"
              },
              {
                "description": "Two units",
                "descriptor": "+L",
                "fee": "188.72",
                "full_code": "14912"
              },
              {
                "description": "Each additional unit over two",
                "descriptor": "+L",
                "fee": "94.36",
                "full_code": "14919"
              },
              {
                "description": "Appliance, Intraoral, For the Treatment of Obstructive Airway Disorders, Monitoring To include patient to ensure proper use of appliances and evaluation for referrals to other health care\nprofessionals for appropriate medical management.",
                "descriptor": "",
                "fee": "",
                "full_code": "14920"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "85.8",
                "full_code": "14921"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "171.6",
                "full_code": "14922"
              },
              {
                "description": "Each additional unit over two",
                "descriptor": "",
                "fee": "85.8",
                "full_code": "14929"
              }
            ],
            "descriptor": "",
            "description": "APPLIANCES, INTRAORAL, TO TREAT MEDICALLY DIAGNOSED OBSTRUCTIVE SLEEP APNEA, SNORING, UPPER AIRWAY RESISTANCE SYNDROM (UARS) WITH OR WITHOUT APNEA  (Includes models,\ngnathological determinants, appliance construction and insertion adjustment [no post-insertion\nadjustments])",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "APPLIANCES"
      },
      {
        "full_code": "15000",
        "children": [
          {
            "full_code": "15100",
            "children": [
              {
                "description": "Space Maintainer, Band Type, Fixed, Unilateral",
                "descriptor": "+L",
                "fee": "283.08",
                "full_code": "15101"
              },
              {
                "description": "Space Maintainer, Band Type, Fixed, Unilateral with Intra-alveolar attachment",
                "descriptor": "+L",
                "fee": "283.08",
                "full_code": "15102"
              },
              {
                "description": "Space Maintainer, Band Type, Fixed, Bilateral (soldered lingual arch)",
                "descriptor": "+L",
                "fee": "377.44",
                "full_code": "15103"
              },
              {
                "description": "Space Maintainer, Band Type, Fixed, Bilateral (soldered lingual arch), with Teeth Attached",
                "descriptor": "+L",
                "fee": "377.44",
                "full_code": "15104"
              },
              {
                "description": "Space Maintainer, Band Type, Fixed, Bilateral Tubes and Locking Wire",
                "descriptor": "+L",
                "fee": "377.44",
                "full_code": "15105"
              }
            ],
            "descriptor": "",
            "description": "SPACE MAINTAINERS, BAND TYPE",
            "fee": ""
          },
          {
            "full_code": "15200",
            "children": [
              {
                "description": "Space Maintainer, Stainless Steel Crown Type, Fixed",
                "descriptor": "+L",
                "fee": "299.17",
                "full_code": "15201"
              },
              {
                "description": "Space Maintainer, Stainless Steel Crown Type, Fixed, with Intra Alveolar Attachment",
                "descriptor": "+L",
                "fee": "283.08",
                "full_code": "15202"
              }
            ],
            "descriptor": "",
            "description": "SPACE MAINTAINERS, STAINLESS STEEL CROWN TYPE",
            "fee": ""
          },
          {
            "full_code": "15300",
            "children": [
              {
                "description": "Space Maintainer, Cast Type, Fixed",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "15301"
              },
              {
                "description": "Space Maintainer, Cast Type, Fixed, with Intra Alveolar Attachment",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "15302"
              }
            ],
            "descriptor": "",
            "description": "SPACE MAINTAINERS, CAST TYPE",
            "fee": ""
          },
          {
            "full_code": "15400",
            "children": [
              {
                "description": "Space Maintainer, Acrylic, Removable, Bilateral Clasps, Retaining Wires",
                "descriptor": "+L",
                "fee": "283.07",
                "full_code": "15401"
              },
              {
                "description": "Space Maintainer, Acrylic, Removable, Bilateral Clasps, Retaining Wires with Teeth",
                "descriptor": "+L",
                "fee": "283.08",
                "full_code": "15402"
              },
              {
                "description": "Space Maintainer, Acrylic Removable, No Clasps",
                "descriptor": "+L",
                "fee": "283.08",
                "full_code": "15403"
              }
            ],
            "descriptor": "",
            "description": "SPACE MAINTAINERS, ACRYLIC, REMOVABLE",
            "fee": ""
          },
          {
            "full_code": "15500",
            "children": [
              {
                "description": "Space Maintainer, Bonded, Pontic Type",
                "descriptor": "+L",
                "fee": "283.08",
                "full_code": "15501"
              }
            ],
            "descriptor": "",
            "description": "SPACE MAINTAINERS, BONDED, PONTIC TYPE",
            "fee": ""
          },
          {
            "full_code": "15600",
            "children": [
              {
                "description": "Maintenance, Space Maintainer Appliances, to include: adjustment  and/or recementation after 30\ndays from insertion",
                "descriptor": "",
                "fee": "94.36",
                "full_code": "15601"
              },
              {
                "description": "Maintenance, Space Maintainer Appliances, addition of clasps and/or activating wires",
                "descriptor": "+L",
                "fee": "188.72",
                "full_code": "15602"
              },
              {
                "description": "Repairs, Space Maintainer Appliances (including recementation)",
                "descriptor": "+L",
                "fee": "188.72",
                "full_code": "15603"
              },
              {
                "description": "Removal of Fixed Space Maintainer Appliances by Second Dentist",
                "descriptor": "",
                "fee": "90.07",
                "full_code": "15604"
              }
            ],
            "descriptor": "",
            "description": "SPACE MAINTAINERS, MAINTENANCE OF",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "SPACE MAINTAINERS (Includes the design, separation, fabrication, insertion, and where applicable initial cementation and removal)"
      },
      {
        "full_code": "16100",
        "children": [
          {
            "full_code": "16101",
            "children": [
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "171.6",
                "full_code": "16102"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "257.4",
                "full_code": "16103"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "343.2",
                "full_code": "16104"
              },
              {
                "description": "Each additional unit over four",
                "descriptor": "",
                "fee": "85.8",
                "full_code": "16109"
              }
            ],
            "descriptor": "",
            "description": "One unit of time",
            "fee": "85.8"
          },
          {
            "full_code": "16200",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "81.49",
                "full_code": "16201"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "162.98",
                "full_code": "16202"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "244.47",
                "full_code": "16203"
              },
              {
                "description": "Each additional unit over three",
                "descriptor": "",
                "fee": "81.49",
                "full_code": "16209"
              }
            ],
            "descriptor": "",
            "description": "DISKING OF TEETH, Interproximal",
            "fee": ""
          },
          {
            "full_code": "16300",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "90.07",
                "full_code": "16301"
              },
              {
                "description": "Each additional unit of time",
                "descriptor": "",
                "fee": "90.07",
                "full_code": "16309"
              }
            ],
            "descriptor": "",
            "description": "RECONTOURING OF NATURAL TEETH FOR AESTHETIC REASONS",
            "fee": ""
          },
          {
            "full_code": "16400",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "90.07",
                "full_code": "16401"
              },
              {
                "description": "Each additional unit of time",
                "descriptor": "",
                "fee": "90.07",
                "full_code": "16409"
              }
            ],
            "descriptor": "",
            "description": "RECONTOURING OF TEETH FOR FUNCTIONAL REASONS (Not associated with delivery of a single or multiple prosthesis)",
            "fee": ""
          },
          {
            "full_code": "16500",
            "children": [
              {
                "description": "Occlusal Adjustment/Equilibration: (a) May require several sessions (b) May be used in conjunction with basic restorative treatment only when occlusal adjustment/equilibration is not required as a result of that restoration. (c)  Not to be used in conjunction with the delivery and post-insertion care of:  fixed or removable prosthesis (50000 & 60000 code series) by the same dentist for period of three months.",
                "descriptor": "",
                "fee": "",
                "full_code": "16510"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "100.59",
                "full_code": "16511"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "201.18",
                "full_code": "16512"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "301.77",
                "full_code": "16513"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "402.36",
                "full_code": "16514"
              },
              {
                "description": "Each additional unit over four",
                "descriptor": "",
                "fee": "100.59",
                "full_code": "16519"
              }
            ],
            "descriptor": "",
            "description": "OCCLUSION"
          }
        ],
        "descriptor": "",
        "description": "FINISHING RESTORATIONS To include: polishing, removal of overhangs, refining marginal ridges and occlusal surfaces, etc. (when restorations were performed by another dentist or restorations are over two years old)",
        "fee": ""
      }
    ],
    "descriptor": "",
    "description": "PREVENTION"
  },
  {
    "full_code": "20000",
    "children": [
      {
        "full_code": "20100",
        "children": [
          {
            "full_code": "20110",
            "children": [
              {
                "description": "First tooth\n",
                "descriptor": "",
                "fee": "91.87 to 183.75",
                "full_code": "20111"
              },
              {
                "description": "Each additional tooth same quadrant\n",
                "descriptor": "",
                "fee": "91.87 to 183.75",
                "full_code": "20119"
              },
              {
                "description": "Caries/Trauma/Pain Control (removal of carious lesions or existing restorations or gingivally attached tooth fragment and placement of sedative/protective dressings, includes pulp caps when necessary and the use of a band for retention and support, as a separate procedure)",
                "descriptor": "",
                "fee": "",
                "full_code": "20120"
              },
              {
                "description": "First tooth\n",
                "descriptor": "",
                "fee": "137.82 to 229.69",
                "full_code": "20121"
              },
              {
                "description": "Each additional tooth same quadrant\n",
                "descriptor": "",
                "fee": "137.82 to 229.69",
                "full_code": "20129"
              },
              {
                "description": "Trauma Control, Smoothing of Fractured Surfaces Per Tooth",
                "descriptor": "",
                "fee": "",
                "full_code": "20130"
              },
              {
                "description": "First tooth",
                "descriptor": "",
                "fee": "49.5",
                "full_code": "20131"
              },
              {
                "description": "Each additional tooth same quadrant",
                "descriptor": "",
                "fee": "44.9",
                "full_code": "20139"
              }
            ],
            "descriptor": "",
            "description": "Caries/Trauma/Pain Control (removal of carious lesions or existing restorations or gingivally attached tooth fragment and placement of sedative/protective dressings, includes pulp caps when necessary, as a separate procedure).",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "CARIES, TRAUMA AND PAIN CONTROL",
        "fee": ""
      },
      {
        "full_code": "21000",
        "children": [
          {
            "full_code": "21100",
            "children": [
              {
                "description": "Restorations, Amalgam, Non-Bonded, Primary Teeth",
                "descriptor": "",
                "fee": "",
                "full_code": "21110"
              },
              {
                "description": "One surface",
                "descriptor": "",
                "fee": "111.84",
                "full_code": "21111"
              },
              {
                "description": "Two surfaces",
                "descriptor": "",
                "fee": "148.07",
                "full_code": "21112"
              },
              {
                "description": "Three surfaces",
                "descriptor": "",
                "fee": "202.66",
                "full_code": "21113"
              },
              {
                "description": "Four surfaces",
                "descriptor": "",
                "fee": "247.71",
                "full_code": "21114"
              },
              {
                "description": "Five surfaces or maximum surfaces per tooth",
                "descriptor": "",
                "fee": "289.79",
                "full_code": "21115"
              },
              {
                "description": "Restorations, Amalgam, Bonded, Primary Teeth",
                "descriptor": "",
                "fee": "",
                "full_code": "21120"
              },
              {
                "description": "One surface",
                "descriptor": "",
                "fee": "147.09",
                "full_code": "21121"
              },
              {
                "description": "Two surfaces",
                "descriptor": "",
                "fee": "195.09",
                "full_code": "21122"
              },
              {
                "description": "Three surfaces",
                "descriptor": "",
                "fee": "234.39",
                "full_code": "21123"
              },
              {
                "description": "Four surfaces",
                "descriptor": "",
                "fee": "275.99",
                "full_code": "21124"
              },
              {
                "description": "Five surfaces or maximum surfaces per tooth",
                "descriptor": "",
                "fee": "320.34",
                "full_code": "21125"
              }
            ],
            "descriptor": "",
            "description": "RESTORATION, AMALGAM, PRIMARY TEETH"
          },
          {
            "full_code": "21200",
            "children": [
              {
                "description": "Restorations, Amalgam, Non-Bonded, Permanent Bicuspids and Anteriors",
                "descriptor": "",
                "fee": "",
                "full_code": "21210"
              },
              {
                "description": "One surface",
                "descriptor": "",
                "fee": "123.34",
                "full_code": "21211"
              },
              {
                "description": "Two surfaces",
                "descriptor": "",
                "fee": "154.17",
                "full_code": "21212"
              },
              {
                "description": "Three surfaces",
                "descriptor": "",
                "fee": "216.45",
                "full_code": "21213"
              },
              {
                "description": "Four surfaces",
                "descriptor": "",
                "fee": "266.1",
                "full_code": "21214"
              },
              {
                "description": "Five surfaces or maximum surfaces per tooth",
                "descriptor": "",
                "fee": "289.79",
                "full_code": "21215"
              },
              {
                "description": "Restorations, Amalgam, Non-Bonded, Permanent Molars",
                "descriptor": "",
                "fee": "",
                "full_code": "21220"
              },
              {
                "description": "One surface",
                "descriptor": "",
                "fee": "130.24",
                "full_code": "21221"
              },
              {
                "description": "Two surfaces",
                "descriptor": "",
                "fee": "161.81",
                "full_code": "21222"
              },
              {
                "description": "Three surfaces",
                "descriptor": "",
                "fee": "221.05",
                "full_code": "21223"
              },
              {
                "description": "Four surfaces",
                "descriptor": "",
                "fee": "278.75",
                "full_code": "21224"
              },
              {
                "description": "Five surfaces or maximum surfaces per tooth",
                "descriptor": "",
                "fee": "311.63",
                "full_code": "21225"
              },
              {
                "description": "Restorations, Amalgam, Bonded, Permanent Bicuspids and Anteriors",
                "descriptor": "",
                "fee": "",
                "full_code": "21230"
              },
              {
                "description": "One surface",
                "descriptor": "",
                "fee": "156.29",
                "full_code": "21231"
              },
              {
                "description": "Two surfaces",
                "descriptor": "",
                "fee": "193.95",
                "full_code": "21232"
              },
              {
                "description": "Three surfaces",
                "descriptor": "",
                "fee": "241.29",
                "full_code": "21233"
              },
              {
                "description": "Four surfaces",
                "descriptor": "",
                "fee": "288.64",
                "full_code": "21234"
              },
              {
                "description": "Five surfaces or maximum surfaces per tooth",
                "descriptor": "",
                "fee": "323.79",
                "full_code": "21235"
              },
              {
                "description": "Restorations, Amalgam, Bonded, Permanent Molars",
                "descriptor": "",
                "fee": "",
                "full_code": "21240"
              },
              {
                "description": "One surface",
                "descriptor": "",
                "fee": "168.93",
                "full_code": "21241"
              },
              {
                "description": "Two surfaces",
                "descriptor": "",
                "fee": "208.89",
                "full_code": "21242"
              },
              {
                "description": "Three surfaces",
                "descriptor": "",
                "fee": "249.33",
                "full_code": "21243"
              },
              {
                "description": "Four surfaces",
                "descriptor": "",
                "fee": "296.68",
                "full_code": "21244"
              },
              {
                "description": "Five surfaces or maximum surfaces per tooth",
                "descriptor": "",
                "fee": "358.28",
                "full_code": "21245"
              }
            ],
            "descriptor": "",
            "description": "RESTORATIONS, AMALGAM, PERMANENT TEETH"
          },
          {
            "full_code": "21300",
            "children": [
              {
                "description": "Restorations, Amalgam Core, Non-Bonded, in Conjunction with Crown or Fixed Bridge Retainer",
                "descriptor": "",
                "fee": "227.49",
                "full_code": "21301"
              },
              {
                "description": "Restorations, Amalgam Core, Bonded, in Conjunction with Crown or Fixed Bridge Retainer",
                "descriptor": "",
                "fee": "254.61",
                "full_code": "21302"
              }
            ],
            "descriptor": "",
            "description": "Restorations, Amalgam Cores",
            "fee": ""
          },
          {
            "full_code": "21400",
            "children": [
              {
                "description": "One pin",
                "descriptor": "",
                "fee": "37.84",
                "full_code": "21401"
              },
              {
                "description": "Two pins",
                "descriptor": "",
                "fee": "54.48",
                "full_code": "21402"
              },
              {
                "description": "Three pins",
                "descriptor": "",
                "fee": "71.11",
                "full_code": "21403"
              },
              {
                "description": "Four pins",
                "descriptor": "",
                "fee": "88.9",
                "full_code": "21404"
              },
              {
                "description": "Five pins or more",
                "descriptor": "",
                "fee": "99.76",
                "full_code": "21405"
              }
            ],
            "descriptor": "",
            "description": "PINS, RETENTIVE per restoration (for amalgams and tooth coloured restorations)",
            "fee": ""
          },
          {
            "full_code": "21500",
            "children": [
              {
                "description": "Per restoration",
                "descriptor": "",
                "fee": "85.45",
                "full_code": "21501"
              }
            ],
            "descriptor": "",
            "description": "RESTORATIONS MADE TO A TOOTH SUPPORTING AN EXISTING PARTIAL DENTURE CLASP\n(ADDITIONAL TO RESTORATION)",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "RESTORATIONS, AMALGAM"
      },
      {
        "full_code": "22000",
        "children": [
          {
            "full_code": "22200",
            "children": [
              {
                "description": "Primary Anterior",
                "descriptor": "",
                "fee": "238.3",
                "full_code": "22201"
              },
              {
                "description": "Primary Anterior - open face/acrylic veneer",
                "descriptor": "+L",
                "fee": "293.69",
                "full_code": "22202"
              },
              {
                "description": "Primary Posterior",
                "descriptor": "",
                "fee": "233.36",
                "full_code": "22211"
              },
              {
                "description": "Primary Posterior - open face",
                "descriptor": "",
                "fee": "315.26",
                "full_code": "22212"
              }
            ],
            "descriptor": "",
            "description": "RESTORATIONS, PREFABRICATED, METAL, PRIMARY TEETH",
            "fee": ""
          },
          {
            "full_code": "22300",
            "children": [
              {
                "description": "Permanent Anterior",
                "descriptor": "",
                "fee": "270.24",
                "full_code": "22301"
              },
              {
                "description": "Permanent Anterior - open face",
                "descriptor": "",
                "fee": "345.15",
                "full_code": "22302"
              },
              {
                "description": "Permanent Posterior",
                "descriptor": "",
                "fee": "270.24",
                "full_code": "22311"
              },
              {
                "description": "Permanent Posterior - open face",
                "descriptor": "",
                "fee": "315.26",
                "full_code": "22312"
              }
            ],
            "descriptor": "",
            "description": "RESTORATIONS PREFABRICATED, METAL, PERMANENT TEETH",
            "fee": ""
          },
          {
            "full_code": "22400",
            "children": [
              {
                "description": "Primary Anterior",
                "descriptor": "",
                "fee": "200.99",
                "full_code": "22401"
              },
              {
                "description": "Primary Posterior",
                "descriptor": "",
                "fee": "200.99",
                "full_code": "22411"
              }
            ],
            "descriptor": "",
            "description": "RESTORATIONS PREFABRICATED, PLASTIC, PRIMARY TEETH",
            "fee": ""
          },
          {
            "full_code": "22500",
            "children": [
              {
                "description": "Permanent Anterior",
                "descriptor": "",
                "fee": "267.94",
                "full_code": "22501"
              },
              {
                "description": "Permanent Posterior",
                "descriptor": "",
                "fee": "267.94",
                "full_code": "22511"
              }
            ],
            "descriptor": "",
            "description": "RESTORATIONS PREFABRICATED, PLASTIC, PERMANENT TEETH",
            "fee": ""
          },
          {
            "full_code": "22600",
            "children": [
              {
                "description": "Primary Anterior",
                "descriptor": "",
                "fee": "279.97",
                "full_code": "22601"
              },
              {
                "description": "Primary Posterior",
                "descriptor": "",
                "fee": "279.97",
                "full_code": "22611"
              }
            ],
            "descriptor": "",
            "description": "RESTORATIONS, PREFABRICATED, PORCELAIN/CERAMIC/POLYMER GLASS, PRIMARY TEETH",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "RESTORATIONS, PREFABRICATED, FULL COVERAGE"
      },
      {
        "full_code": "23000",
        "children": [
          {
            "full_code": "23100",
            "children": [
              {
                "description": "One surface",
                "descriptor": "",
                "fee": "129.78",
                "full_code": "23101"
              },
              {
                "description": "Two surfaces",
                "descriptor": "",
                "fee": "147.02",
                "full_code": "23102"
              },
              {
                "description": "Three surfaces",
                "descriptor": "",
                "fee": "206.76",
                "full_code": "23103"
              },
              {
                "description": "Four surfaces",
                "descriptor": "",
                "fee": "225.12",
                "full_code": "23104"
              },
              {
                "description": "Five surfaces (maximum surfaces per tooth)",
                "descriptor": "",
                "fee": "268.75",
                "full_code": "23105"
              },
              {
                "description": "Restorations, Permanent Anteriors, Bonded Technique (not to be used for Veneer Applications or Diastema Closures)",
                "descriptor": "",
                "fee": "",
                "full_code": "23110"
              },
              {
                "description": "One surface",
                "descriptor": "",
                "fee": "156.93",
                "full_code": "23111"
              },
              {
                "description": "Two surfaces",
                "descriptor": "",
                "fee": "187.84",
                "full_code": "23112"
              },
              {
                "description": "Three surfaces",
                "descriptor": "",
                "fee": "215.63",
                "full_code": "23113"
              },
              {
                "description": "Four surfaces",
                "descriptor": "",
                "fee": "282.2",
                "full_code": "23114"
              },
              {
                "description": "Five surfaces (maximum surfaces per tooth)",
                "descriptor": "",
                "fee": "346.78",
                "full_code": "23115"
              },
              {
                "description": "Restorations, Tooth Coloured, Veneer Applications",
                "descriptor": "",
                "fee": "",
                "full_code": "23120"
              },
              {
                "description": "Tooth Colored Veneer Application - Non Prefabricated Direct Buildup - Bonded",
                "descriptor": "",
                "fee": "382.84",
                "full_code": "23122"
              },
              {
                "description": "Tooth Colored Veneer Application - Diastema Closure, Interproximal only, Bonded",
                "descriptor": "",
                "fee": "306.55",
                "full_code": "23123"
              }
            ],
            "descriptor": "",
            "description": "RESTORATIONS, TOOTH COLOURED, PERMANENT ANTERIORS, NON BONDED TECHNIQUE",
            "fee": ""
          },
          {
            "full_code": "23200",
            "children": [
              {
                "description": "Permanent Bicuspids",
                "descriptor": "",
                "fee": "",
                "full_code": "23210"
              },
              {
                "description": "One surface",
                "descriptor": "",
                "fee": "126.33",
                "full_code": "23211"
              },
              {
                "description": "Two surfaces",
                "descriptor": "",
                "fee": "160.78",
                "full_code": "23212"
              },
              {
                "description": "Three surfaces",
                "descriptor": "",
                "fee": "202.14",
                "full_code": "23213"
              },
              {
                "description": "Four surfaces",
                "descriptor": "",
                "fee": "243.52",
                "full_code": "23214"
              },
              {
                "description": "Five surfaces or maximum surface per tooth",
                "descriptor": "",
                "fee": "256.14",
                "full_code": "23215"
              },
              {
                "description": "Permanent Molars",
                "descriptor": "",
                "fee": "",
                "full_code": "23220"
              },
              {
                "description": "One surface",
                "descriptor": "",
                "fee": "137.82",
                "full_code": "23221"
              },
              {
                "description": "Two surfaces",
                "descriptor": "",
                "fee": "180.32",
                "full_code": "23222"
              },
              {
                "description": "Three surfaces",
                "descriptor": "",
                "fee": "211.34",
                "full_code": "23223"
              },
              {
                "description": "Four surfaces",
                "descriptor": "",
                "fee": "245.81",
                "full_code": "23224"
              },
              {
                "description": "Five surfaces or maximum surface per tooth",
                "descriptor": "",
                "fee": "309.02",
                "full_code": "23225"
              }
            ],
            "descriptor": "",
            "description": "RESTORATIONS, TOOTH COLOURED/ PLASTIC WITH/WITHOUT SILVER FILINGS, PERMANENT\nPOSTERIORS NON BONDED"
          },
          {
            "full_code": "23300",
            "children": [
              {
                "description": "Permanent Bicuspids",
                "descriptor": "",
                "fee": "",
                "full_code": "23310"
              },
              {
                "description": "One surface",
                "descriptor": "",
                "fee": "164.53",
                "full_code": "23311"
              },
              {
                "description": "Two surfaces",
                "descriptor": "",
                "fee": "229.2",
                "full_code": "23312"
              },
              {
                "description": "Three surfaces",
                "descriptor": "",
                "fee": "268.43",
                "full_code": "23313"
              },
              {
                "description": "Four surfaces",
                "descriptor": "",
                "fee": "331.35",
                "full_code": "23314"
              },
              {
                "description": "Five surfaces or maximum surface per tooth",
                "descriptor": "",
                "fee": "376.4",
                "full_code": "23315"
              },
              {
                "description": "Permanent Molars",
                "descriptor": "",
                "fee": "",
                "full_code": "23320"
              },
              {
                "description": "One surface",
                "descriptor": "",
                "fee": "171.98",
                "full_code": "23321"
              },
              {
                "description": "Two surfaces",
                "descriptor": "",
                "fee": "242.45",
                "full_code": "23322"
              },
              {
                "description": "Three surfaces",
                "descriptor": "",
                "fee": "287.04",
                "full_code": "23323"
              },
              {
                "description": "Four surfaces",
                "descriptor": "",
                "fee": "352.05",
                "full_code": "23324"
              },
              {
                "description": "Five surfaces or maximum surface per tooth",
                "descriptor": "",
                "fee": "407.43",
                "full_code": "23325"
              }
            ],
            "descriptor": "",
            "description": "RESTORATIONS, TOOTH COLORED, PERMANENT POSTERIORS - BONDED"
          },
          {
            "full_code": "23400",
            "children": [
              {
                "description": "One surface",
                "descriptor": "",
                "fee": "124.03",
                "full_code": "23401"
              },
              {
                "description": "Two surfaces",
                "descriptor": "",
                "fee": "152.77",
                "full_code": "23402"
              },
              {
                "description": "Three surfaces",
                "descriptor": "",
                "fee": "179.17",
                "full_code": "23403"
              },
              {
                "description": "Four surfaces",
                "descriptor": "",
                "fee": "226.27",
                "full_code": "23404"
              },
              {
                "description": "Five surfaces (or maximum surfaces per tooth)",
                "descriptor": "",
                "fee": "275.65",
                "full_code": "23405"
              },
              {
                "description": "Restorations, Tooth Colored, Primary, Anterior, Bonded Technique",
                "descriptor": "",
                "fee": "",
                "full_code": "23410"
              },
              {
                "description": "One surface",
                "descriptor": "",
                "fee": "157.62",
                "full_code": "23411"
              },
              {
                "description": "Two surfaces",
                "descriptor": "",
                "fee": "184.75",
                "full_code": "23412"
              },
              {
                "description": "Three surfaces",
                "descriptor": "",
                "fee": "202.66",
                "full_code": "23413"
              },
              {
                "description": "Four surfaces",
                "descriptor": "",
                "fee": "247.71",
                "full_code": "23414"
              },
              {
                "description": "Five surfaces (or maximum surfaces per tooth)",
                "descriptor": "",
                "fee": "323.79",
                "full_code": "23415"
              }
            ],
            "descriptor": "",
            "description": "RESTORATIONS, TOOTH COLORED, PRIMARY, ANTERIOR, NON BONDED",
            "fee": ""
          },
          {
            "full_code": "23500",
            "children": [
              {
                "description": "One surface",
                "descriptor": "",
                "fee": "126.33",
                "full_code": "23501"
              },
              {
                "description": "Two surfaces",
                "descriptor": "",
                "fee": "165.38",
                "full_code": "23502"
              },
              {
                "description": "Three surfaces",
                "descriptor": "",
                "fee": "190.65",
                "full_code": "23503"
              },
              {
                "description": "Four surfaces",
                "descriptor": "",
                "fee": "205.58",
                "full_code": "23504"
              },
              {
                "description": "Five surfaces or maximum surface per tooth",
                "descriptor": "",
                "fee": "251.54",
                "full_code": "23505"
              },
              {
                "description": "Restorations, Tooth Colored, Primary, Posterior, Bonded Technique",
                "descriptor": "",
                "fee": "",
                "full_code": "23510"
              },
              {
                "description": "One surface",
                "descriptor": "",
                "fee": "165.67",
                "full_code": "23511"
              },
              {
                "description": "Two surfaces",
                "descriptor": "",
                "fee": "209.41",
                "full_code": "23512"
              },
              {
                "description": "Three surfaces",
                "descriptor": "",
                "fee": "270.24",
                "full_code": "23513"
              },
              {
                "description": "Four surfaces",
                "descriptor": "",
                "fee": "315.26",
                "full_code": "23514"
              },
              {
                "description": "Five surfaces or maximum surface per tooth",
                "descriptor": "",
                "fee": "360.3",
                "full_code": "23515"
              }
            ],
            "descriptor": "",
            "description": "RESTORATIONS, TOOTH COLOURED/ PLASTIC WITH/WITHOUT SILVER FILINGS, PRIMARY,\nPOSTERIOR, NON BONDED",
            "fee": ""
          },
          {
            "full_code": "23600",
            "children": [
              {
                "description": "Restoration, Tooth Colored, Non-Bonded Core, in Conjunction with Crown or Fixed Bridge Retainer",
                "descriptor": "",
                "fee": "241.29",
                "full_code": "23601"
              },
              {
                "description": "Restoration, Tooth Colored, Bonded Core, in Conjunction with Crown or Fixed Bridge Retainer",
                "descriptor": "",
                "fee": "278.29",
                "full_code": "23602"
              }
            ],
            "descriptor": "",
            "description": "RESTORATIONS, TOOTH COLOURED/ PLASTIC WITH/WITHOUT SILVER FILINGS, CORES",
            "fee": ""
          },
          {
            "full_code": "23700",
            "children": [
              {
                "description": "One surface",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "23701"
              },
              {
                "description": "Each additional surface over one",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "23709"
              }
            ],
            "descriptor": "",
            "description": "RESIN INFILTRATION (Placement of an infiltrating resin restoration for the purpose of filling the sub- surface porosity of an incipient, non-cavitated lesion for the purpose of strengthening, stabilizing\nand/or limiting the progression of the lesion.)",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "RESTORATIONS, TOOTH COLOURED/PLASTIC WITH/WITHOUT SILVER FILINGS"
      },
      {
        "full_code": "24000",
        "children": [
          {
            "full_code": "24100",
            "children": [
              {
                "description": "Class l",
                "descriptor": "",
                "fee": "601.86",
                "full_code": "24101"
              },
              {
                "description": "Class lll",
                "descriptor": "",
                "fee": "802.88",
                "full_code": "24102"
              },
              {
                "description": "Class V",
                "descriptor": "",
                "fee": "551.28",
                "full_code": "24103"
              },
              {
                "description": "Class lV",
                "descriptor": "",
                "fee": "946.7",
                "full_code": "24104"
              }
            ],
            "descriptor": "",
            "description": "RESTORATIONS, FOIL, GOLD, ANTERIORS",
            "fee": ""
          },
          {
            "full_code": "24200",
            "children": [
              {
                "description": "Class l",
                "descriptor": "",
                "fee": "601.86",
                "full_code": "24201"
              },
              {
                "description": "Class ll",
                "descriptor": "",
                "fee": "802.88",
                "full_code": "24202"
              },
              {
                "description": "Class V",
                "descriptor": "",
                "fee": "601.7",
                "full_code": "24203"
              }
            ],
            "descriptor": "",
            "description": "RESTORATIONS, FOIL, GOLD, POSTERIORS",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "RESTORATIONS, FOIL, GOLD"
      },
      {
        "full_code": "25000",
        "children": [
          {
            "full_code": "25100",
            "children": [
              {
                "description": "Inlays, Metal",
                "descriptor": "",
                "fee": "",
                "full_code": "25110"
              },
              {
                "description": "One surface",
                "descriptor": "+L",
                "fee": "523.78",
                "full_code": "25111"
              },
              {
                "description": "Two surfaces",
                "descriptor": "+L",
                "fee": "696",
                "full_code": "25112"
              },
              {
                "description": "Three surfaces",
                "descriptor": "+L",
                "fee": "748.86",
                "full_code": "25113"
              },
              {
                "description": "Three surfaces, modified",
                "descriptor": "+L",
                "fee": "904.64",
                "full_code": "25114"
              },
              {
                "description": "Inlays, Composite/Compomer, Indirect (Bonded)",
                "descriptor": "",
                "fee": "",
                "full_code": "25120"
              },
              {
                "description": "One surface",
                "descriptor": "+L",
                "fee": "541.42",
                "full_code": "25121"
              },
              {
                "description": "Two surfaces",
                "descriptor": "+L",
                "fee": "631.51",
                "full_code": "25122"
              },
              {
                "description": "Three surfaces",
                "descriptor": "+L",
                "fee": "737.66",
                "full_code": "25123"
              },
              {
                "description": "Three surfaces, modified",
                "descriptor": "+L",
                "fee": "948.91",
                "full_code": "25124"
              },
              {
                "description": "Inlays, Porcelain/Ceramic/Polymer Glass",
                "descriptor": "",
                "fee": "",
                "full_code": "25130"
              },
              {
                "description": "One surface",
                "descriptor": "+L",
                "fee": "501.94",
                "full_code": "25131"
              },
              {
                "description": "Two surfaces",
                "descriptor": "+L",
                "fee": "562.78",
                "full_code": "25132"
              },
              {
                "description": "Three surfaces",
                "descriptor": "+L",
                "fee": "760.41",
                "full_code": "25133"
              },
              {
                "description": "Three surfaces, modified",
                "descriptor": "+L",
                "fee": "795.43",
                "full_code": "25134"
              },
              {
                "description": "Inlays, Porcelain/Ceramic/Polymer Glass (Bonded)",
                "descriptor": "",
                "fee": "",
                "full_code": "25140"
              },
              {
                "description": "One surface",
                "descriptor": "+L",
                "fee": "536.82",
                "full_code": "25141"
              },
              {
                "description": "Two surfaces",
                "descriptor": "+L",
                "fee": "753.36",
                "full_code": "25142"
              },
              {
                "description": "Three surfaces",
                "descriptor": "+L",
                "fee": "879.06",
                "full_code": "25143"
              },
              {
                "description": "Three surfaces, modified",
                "descriptor": "+L",
                "fee": "948.91",
                "full_code": "25144"
              }
            ],
            "descriptor": "",
            "description": "RESTORATIONS INLAYS"
          },
          {
            "full_code": "25500",
            "children": [
              {
                "description": "Onlays, Cast Metal, Indirect",
                "descriptor": "",
                "fee": "",
                "full_code": "25510"
              },
              {
                "description": "Onlay, Cast Metal, Indirect",
                "descriptor": "+L",
                "fee": "748.86",
                "full_code": "25511"
              },
              {
                "description": "Onlays, Cast Metal, Indirect (Bonded external retention type)",
                "descriptor": "+L",
                "fee": "783.35",
                "full_code": "25512"
              },
              {
                "description": "Onlays, Composite/Compomer, Processed (Bonded)",
                "descriptor": "",
                "fee": "",
                "full_code": "25520"
              },
              {
                "description": "Onlays, Composite/Compomer, Indirect (Bonded)",
                "descriptor": "+L",
                "fee": "948.91",
                "full_code": "25521"
              },
              {
                "description": "Onlays, Porcelain/Ceramic/Polymer glass (Bonded)",
                "descriptor": "",
                "fee": "",
                "full_code": "25530"
              },
              {
                "description": "Onlays, Porcelain/Ceramic/Polymer Glass (Bonded)",
                "descriptor": "+L",
                "fee": "948.91",
                "full_code": "25531"
              }
            ],
            "descriptor": "",
            "description": "RESTORATIONS, ONLAYS (where one or more cusps are restored)"
          },
          {
            "full_code": "25600",
            "children": [
              {
                "description": "One pin/tooth",
                "descriptor": "+L",
                "fee": "51.27",
                "full_code": "25601"
              },
              {
                "description": "Two pins/tooth",
                "descriptor": "+L",
                "fee": "97.96",
                "full_code": "25602"
              },
              {
                "description": "Three pins/tooth",
                "descriptor": "+L",
                "fee": "155.18",
                "full_code": "25603"
              },
              {
                "description": "Four pins/tooth",
                "descriptor": "+L",
                "fee": "190.07",
                "full_code": "25604"
              },
              {
                "description": "Five or more pins/tooth",
                "descriptor": "+L",
                "fee": "223.82",
                "full_code": "25605"
              }
            ],
            "descriptor": "",
            "description": "PINS, RETENTIVE (for inlays, onlays and crowns per tooth)",
            "fee": ""
          },
          {
            "full_code": "25700",
            "children": [
              {
                "description": "Posts, Cast Metal, (including core) As a Separate Procedure",
                "descriptor": "",
                "fee": "",
                "full_code": "25710"
              },
              {
                "description": "Single section",
                "descriptor": "+L",
                "fee": "382.46",
                "full_code": "25711"
              },
              {
                "description": "Two sections",
                "descriptor": "+L",
                "fee": "459.41",
                "full_code": "25712"
              },
              {
                "description": "Three sections",
                "descriptor": "+L",
                "fee": "603.01",
                "full_code": "25713"
              },
              {
                "description": "Posts, Cast Metal (including core) Concurrent with Impression for Crown",
                "descriptor": "",
                "fee": "",
                "full_code": "25720"
              },
              {
                "description": "Single section",
                "descriptor": "+L",
                "fee": "218.24",
                "full_code": "25721"
              },
              {
                "description": "Two sections",
                "descriptor": "+L",
                "fee": "294.04",
                "full_code": "25722"
              },
              {
                "description": "Three sections",
                "descriptor": "+L",
                "fee": "367.51",
                "full_code": "25723"
              },
              {
                "description": "Post, Prefabricated Retentive",
                "descriptor": "",
                "fee": "",
                "full_code": "25730"
              },
              {
                "description": "One post",
                "descriptor": "+E",
                "fee": "182.66",
                "full_code": "25731"
              },
              {
                "description": "Two posts same tooth",
                "descriptor": "+E",
                "fee": "303.24",
                "full_code": "25732"
              },
              {
                "description": "Three posts same tooth",
                "descriptor": "+E",
                "fee": "413.46",
                "full_code": "25733"
              },
              {
                "description": "Posts, Prefabricated, Retentive and Cast Core",
                "descriptor": "",
                "fee": "",
                "full_code": "25740"
              },
              {
                "description": "One post and cast core",
                "descriptor": "+L +E",
                "fee": "318.18",
                "full_code": "25741"
              },
              {
                "description": "Two posts (same tooth) and cast core",
                "descriptor": "+L +E",
                "fee": "402.01",
                "full_code": "25742"
              },
              {
                "description": "Three posts (same tooth) and cast core",
                "descriptor": "+L +E",
                "fee": "501.94",
                "full_code": "25743"
              },
              {
                "description": "Posts, Provisional",
                "descriptor": "",
                "fee": "",
                "full_code": "25770"
              },
              {
                "description": "Per post",
                "descriptor": "+L and/or\n+E",
                "fee": "99.92",
                "full_code": "25771"
              },
              {
                "description": "Post Removal",
                "descriptor": "",
                "fee": "",
                "full_code": "25780"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "122.9",
                "full_code": "25781"
              },
              {
                "description": "Two units of time",
                "descriptor": "",
                "fee": "245.8",
                "full_code": "25782"
              },
              {
                "description": "Three units of time",
                "descriptor": "",
                "fee": "368.7",
                "full_code": "25783"
              },
              {
                "description": "Four units of time",
                "descriptor": "",
                "fee": "491.6",
                "full_code": "25784"
              },
              {
                "description": "Each additional unit over four",
                "descriptor": "",
                "fee": "122.9",
                "full_code": "25789"
              }
            ],
            "descriptor": "",
            "description": "POSTS"
          }
        ],
        "descriptor": "",
        "description": "RESTORATIONS, INLAYS, ONLAYS, PINS AND POSTS"
      },
      {
        "full_code": "26000",
        "children": [
          {
            "full_code": "26100",
            "children": [
              {
                "description": "Indirect, Angulated or transmucosal pre-fabricated abutment, per implant",
                "descriptor": "+L +E",
                "fee": "I.C.",
                "full_code": "26101"
              },
              {
                "description": "Indirect, Custom laboratory fabricated, per implant",
                "descriptor": "+L +E",
                "fee": "I.C.",
                "full_code": "26102"
              },
              {
                "description": "Direct, (with intra-oral preparation), per implant site",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "26103"
              }
            ],
            "descriptor": "",
            "description": "Mesostructures, Osseo-integrated Implant - Supported",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "MESOSTRUCTURES (a separate component positioned between the head of an implant and the final restoration, retained by either a cemented post or screw)"
      },
      {
        "full_code": "27000",
        "children": [
          {
            "full_code": "27100",
            "children": [
              {
                "description": "Crowns, Acrylic/Composite/Compomer, Indirect",
                "descriptor": "",
                "fee": "",
                "full_code": "27110"
              },
              {
                "description": "Crown, Acrylic/Composite/Compomer, Indirect",
                "descriptor": "+L",
                "fee": "752.3",
                "full_code": "27111"
              },
              {
                "description": "Crown, Acrylic/Composite/Compomer, Indirect, Complicated (restorative, positional and/or esthetic)",
                "descriptor": "+L",
                "fee": "1005.04",
                "full_code": "27112"
              },
              {
                "description": "Crown, Acrylic/Composite/Compomer, Provisional [Long Term], Indirect (lab fabricated/relined intra-\norally)",
                "descriptor": "+L",
                "fee": "294.04",
                "full_code": "27113"
              },
              {
                "description": "Crowns, Acrylic/Composite/Compomer, Direct",
                "descriptor": "",
                "fee": "",
                "full_code": "27120"
              },
              {
                "description": "Crowns, Acrylic/Composite/Compomer, Direct, Provisional (chairside)",
                "descriptor": "+E",
                "fee": "227.49",
                "full_code": "27121"
              },
              {
                "description": "Crowns, Acrylic/Composite/Compomer, Direct, Provisional Implant-supported",
                "descriptor": "+E",
                "fee": "227.49",
                "full_code": "27125"
              },
              {
                "description": "Crown, Acrylic/Composite/Compomer/Cast Metal Base, Indirect",
                "descriptor": "",
                "fee": "",
                "full_code": "27130"
              },
              {
                "description": "Crown, Acrylic/Composite/Compomer/Cast Metal Base, Indirect",
                "descriptor": "+L",
                "fee": "801.73",
                "full_code": "27131"
              },
              {
                "description": "Crown, Acrylic/Composite/Compomer Cast Metal Base, Implant-supported",
                "descriptor": "+L +E",
                "fee": "801.73",
                "full_code": "27135"
              },
              {
                "description": "Crown, Acrylic/Composite/Compomer/Cast Metal Base with Cast Post Retention",
                "descriptor": "+L",
                "fee": "1005.04",
                "full_code": "27136"
              },
              {
                "description": "Crown, Acrylic/Composite/Compomer/ Prefabricated Metal Base, Provisional, Direct",
                "descriptor": "",
                "fee": "",
                "full_code": "27140"
              },
              {
                "description": "Crown, Acrylic/Composite/Compomer/ Pre-fabricated Metal Base, Provisional, Implant-supported,\nDirect",
                "descriptor": "+E",
                "fee": "227.49",
                "full_code": "27145"
              },
              {
                "description": "Crown, Acrylic/Composite/Compomer/ Pre-Fabricated Metal Base, Provisional, Indirect",
                "descriptor": "",
                "fee": "",
                "full_code": "27150"
              },
              {
                "description": "Crown, Acrylic/ Composite/Compomer/Pre-fabricated Metal Base, Provisional, Implant-supported,\nIndirect",
                "descriptor": "+L +E",
                "fee": "227.49",
                "full_code": "27155"
              }
            ],
            "descriptor": "",
            "description": "CROWNS, ACRYLIC/COMPOSITE/COMPOMER, (with or without Cast or Prefabricated Metal Bases)"
          },
          {
            "full_code": "27200",
            "children": [
              {
                "description": "Crown, Porcelain/Ceramic/Polymer Glass",
                "descriptor": "+L",
                "fee": "948.91",
                "full_code": "27201"
              },
              {
                "description": "Crown, Porcelain/Ceramic/Polymer Glass, Complicated",
                "descriptor": "+L",
                "fee": "1259.57",
                "full_code": "27202"
              },
              {
                "description": "Crown, Porcelain/Ceramic/Polymer Glass, Implant-supported",
                "descriptor": "+L +E",
                "fee": "948.91",
                "full_code": "27205"
              },
              {
                "description": "Crown, Porcelain/Ceramic/Polymer Glass, with Cast Ceramic Post Retention",
                "descriptor": "+L",
                "fee": "1259.57",
                "full_code": "27206"
              },
              {
                "description": "Crown, Porcelain/Ceramic/Polymer Glass, Fused to Metal Base",
                "descriptor": "",
                "fee": "",
                "full_code": "27210"
              },
              {
                "description": "Crown, Porcelain/Ceramic/Polymer Glass, Fused to Metal Base",
                "descriptor": "+L",
                "fee": "948.91",
                "full_code": "27211"
              },
              {
                "description": "Crown, Porcelain/Ceramic/Polymer Glass, Fused to Metal Base, Complicated (restorative, positional\nand/or aesthetic)",
                "descriptor": "+L",
                "fee": "1259.57",
                "full_code": "27212"
              },
              {
                "description": "Crown, Porcelain/Ceramic Fused to Metal Base, Implant-supported",
                "descriptor": "+L +E",
                "fee": "948.91",
                "full_code": "27215"
              },
              {
                "description": "Crown, Porcelain/Ceramic Fused to Metal Base with Cast Metal Post Retention",
                "descriptor": "+L",
                "fee": "1259.57",
                "full_code": "27216"
              },
              {
                "description": "Crown,  \u00be, Porcelain/Ceramic/Polymer Glass",
                "descriptor": "",
                "fee": "",
                "full_code": "27220"
              },
              {
                "description": "Crown, \u00be, Porcelain/Ceramic/Polymer Glass",
                "descriptor": "+L",
                "fee": "948.91",
                "full_code": "27221"
              },
              {
                "description": "Crown, \u00be, Porcelain/Ceramic/Polymer Glass, Complicated",
                "descriptor": "+L",
                "fee": "1259.57",
                "full_code": "27222"
              }
            ],
            "descriptor": "",
            "description": "CROWNS, PORCELAIN/CERAMIC/POLYMER GLASS",
            "fee": ""
          },
          {
            "full_code": "27300",
            "children": [
              {
                "description": "Crown, Cast Metal",
                "descriptor": "+L",
                "fee": "948.91",
                "full_code": "27301"
              },
              {
                "description": "Crown, Cast Metal, Complicated (restorative, positional)",
                "descriptor": "+L",
                "fee": "1259.57",
                "full_code": "27302"
              },
              {
                "description": "Crown, Cast Metal, Implant-supported",
                "descriptor": "+L +E",
                "fee": "948.91",
                "full_code": "27305"
              },
              {
                "description": "Crown, Cast Metal, with Cast Metal Post Retention",
                "descriptor": "+L",
                "fee": "1259.57",
                "full_code": "27306"
              },
              {
                "description": "Semi-precision Rest (Interlock) (in addition to Cast Metal Crown)",
                "descriptor": "+L +E",
                "fee": "212.29",
                "full_code": "27307"
              },
              {
                "description": "Semi-precision or Precision Attachment RPD Retainer (in addition to Cast Metal Crown)",
                "descriptor": "+L +E",
                "fee": "524.89",
                "full_code": "27308"
              },
              {
                "description": "Crowns, \u00be, Cast Metal",
                "descriptor": "",
                "fee": "",
                "full_code": "27310"
              },
              {
                "description": "Crowns, \u00be, Cast Metal",
                "descriptor": "+L",
                "fee": "948.91",
                "full_code": "27311"
              },
              {
                "description": "Crowns, Metal \u00be Cast Metal, Complicated",
                "descriptor": "+L",
                "fee": "1259.57",
                "full_code": "27312"
              },
              {
                "description": "Crowns, \u00be, Cast Metal, with Direct Tooth Colored Corner",
                "descriptor": "+L",
                "fee": "948.91",
                "full_code": "27313"
              }
            ],
            "descriptor": "",
            "description": "CROWNS, CAST METAL",
            "fee": ""
          },
          {
            "full_code": "27400",
            "children": [
              {
                "description": "One crown",
                "descriptor": "+L",
                "fee": "137.18",
                "full_code": "27401"
              },
              {
                "description": "Each additional crown",
                "descriptor": "+L",
                "fee": "90.05",
                "full_code": "27409"
              }
            ],
            "descriptor": "",
            "description": "CROWNS MADE TO AN EXISTING PARTIAL DENTURE CLASP (additional to crown)",
            "fee": ""
          },
          {
            "full_code": "27500",
            "children": [
              {
                "description": "Coping, Metal/Acrylic, Transfer (thimble), as a Separate Procedure",
                "descriptor": "",
                "fee": "",
                "full_code": "27510"
              },
              {
                "description": "Coping, Metal/Acrylic, Transfer (thimble) as a Separate  Procedure",
                "descriptor": "+L",
                "fee": "400.86",
                "full_code": "27511"
              },
              {
                "description": "Coping, Metal/Acrylic, Transfer (thimble) Concurrent with Impression for Crown",
                "descriptor": "",
                "fee": "",
                "full_code": "27520"
              },
              {
                "description": "Coping, Metal/Acrylic, Transfer (thimble) Concurrent with Impression for Crown",
                "descriptor": "+L",
                "fee": "99.92",
                "full_code": "27521"
              }
            ],
            "descriptor": "",
            "description": "COPINGS, METAL/PLASTIC, TRANSFER (thimble type)"
          },
          {
            "full_code": "27600",
            "children": [
              {
                "description": "Veneers, Acrylic/Composite/Compomer, Bonded",
                "descriptor": "+L",
                "fee": "829.32",
                "full_code": "27601"
              },
              {
                "description": "Veneers, Porcelain/Ceramic/Polymer Glass, Bonded",
                "descriptor": "+L",
                "fee": "948.91",
                "full_code": "27602"
              }
            ],
            "descriptor": "",
            "description": "VENEERS, LABORATORY PROCESSED",
            "fee": ""
          },
          {
            "full_code": "27700",
            "children": [
              {
                "description": "Repairs, Inlays, Onlays or Crowns, Acrylic/Composite/Compomer (single units)",
                "descriptor": "",
                "fee": "",
                "full_code": "27710"
              },
              {
                "description": "Repairs, Acrylic/Composite/Compomer, Direct\n",
                "descriptor": "",
                "fee": "91.87 to 275.64",
                "full_code": "27711"
              },
              {
                "description": "Repairs, Inlays, Onlays or Crowns, Porcelain/Ceramic/Polymer Glass, Porcelain/Ceramic/Polymer\nGlass/Fused to Metal base (single units)",
                "descriptor": "",
                "fee": "",
                "full_code": "27720"
              },
              {
                "description": "Repairs, Inlays, Onlays or Crowns, Porcelain/Ceramic/Polymer Glass, Porcelain/Ceramic/Polymer Glass/Fused to Metal base, Direct\n",
                "descriptor": "",
                "fee": "91.87 to 275.64",
                "full_code": "27721"
              },
              {
                "description": "Repairs, Inlays, Onlays or Crowns, Porcelain/Ceramic/Polymer Glass, Porcelain/Ceramic/Polymer\nGlass/Fused to Metal base, Indirect",
                "descriptor": "+L",
                "fee": "180.39",
                "full_code": "27722"
              }
            ],
            "descriptor": "",
            "description": "REPAIRS, (SINGLE UNIT ONLY, DOES NOT INCLUDE AND RECEMENTATION)"
          },
          {
            "full_code": "27800",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "97.62",
                "full_code": "27801"
              },
              {
                "description": "Each additional unit of time",
                "descriptor": "",
                "fee": "97.62",
                "full_code": "27809"
              }
            ],
            "descriptor": "",
            "description": "RECONTOURING OF EXISTING CROWNS per tooth",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "CROWNS, SINGLE UNITS ONLY (includes temporary protection and local anaesthetic, caries removal, and uncomplicated restoration prior to crown preparation). Extensive restoration requiring pins or dowels extra."
      },
      {
        "full_code": "28000",
        "children": [
          {
            "full_code": "28100",
            "children": [
              {
                "description": "Natural Tooth Preparation, Placement of Pulp Chamber Restoration (amalgam or composite) and Fluoride Application Endodontically Treated Tooth",
                "descriptor": "",
                "fee": "251.54",
                "full_code": "28101"
              },
              {
                "description": "Natural Tooth Preparation and Fluoride Application, Vital Tooth",
                "descriptor": "",
                "fee": "300.94",
                "full_code": "28102"
              },
              {
                "description": "Pre-fabricated Attachment, as an Internal/External Overdenture Retentive Device, Direct to a Natural\nTooth (used with the appropriate denture code) per tooth",
                "descriptor": "+L +E",
                "fee": "300.94",
                "full_code": "28103"
              },
              {
                "description": "Implant-supported Prefabricated Attachment as an Overdenture Retentive Device, Direct",
                "descriptor": "+E",
                "fee": "150.47",
                "full_code": "28105"
              }
            ],
            "descriptor": "",
            "description": "RESTORATIVE PROCEDURES, OVERDENTURES, DIRECT",
            "fee": ""
          },
          {
            "full_code": "28200",
            "children": [
              {
                "description": "Coping Crowns, Cast Metal, No Attachments, Indirect",
                "descriptor": "",
                "fee": "",
                "full_code": "28210"
              },
              {
                "description": "Coping Crown, Cast Metal, No Attachments, Indirect",
                "descriptor": "+L",
                "fee": "402.01",
                "full_code": "28211"
              },
              {
                "description": "Coping Crown, Cast Metal, No Attachments, Implant-supported, Indirect",
                "descriptor": "+L +E",
                "fee": "402.01",
                "full_code": "28215"
              },
              {
                "description": "Coping Crown, Cast Metal with Cast Metal Retentive Post, No Attachments",
                "descriptor": "+L +E",
                "fee": "603.01",
                "full_code": "28216"
              },
              {
                "description": "Coping Crown, Cast Metal, with Attachments, Indirect",
                "descriptor": "",
                "fee": "",
                "full_code": "28220"
              },
              {
                "description": "Coping Crown, Metal Cast, with Attachment, Indirect",
                "descriptor": "+L +E",
                "fee": "501.94",
                "full_code": "28221"
              },
              {
                "description": "Coping Crown, Cast Metal, Implant-supported with Attachment",
                "descriptor": "+L +E",
                "fee": "501.94",
                "full_code": "28225"
              },
              {
                "description": "Coping Crown, Cast Metal with Cast Metal Retentive Post, with Attachment",
                "descriptor": "+L +E",
                "fee": "738.81",
                "full_code": "28226"
              }
            ],
            "descriptor": "",
            "description": "RESTORATIVE PROCEDURES, OVERDENTURES, INDIRECT"
          }
        ],
        "descriptor": "",
        "description": "RESTORATIVE PROCEDURES, OVERDENTURES"
      },
      {
        "full_code": "29000",
        "children": [
          {
            "full_code": "29100",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "+L +E",
                "fee": "98.77",
                "full_code": "29101"
              },
              {
                "description": "Two units",
                "descriptor": "+L +E",
                "fee": "197.54",
                "full_code": "29102"
              },
              {
                "description": "Three units",
                "descriptor": "+L +E",
                "fee": "296.31",
                "full_code": "29103"
              },
              {
                "description": "Four units",
                "descriptor": "+L +E",
                "fee": "395.08",
                "full_code": "29104"
              }
            ],
            "descriptor": "",
            "description": "RECEMENTATION/REBONDING, INLAYS/ONLAYS/CROWNS/VENEERS/POSTS/ NATURAL TOOTH\nFRAGMENTS (single units only) (+ L and/or +E where laboratory charges or expenses are incurred during repair of the unit)",
            "fee": ""
          },
          {
            "full_code": "29300",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "97.63",
                "full_code": "29301"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "195.26",
                "full_code": "29302"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "292.89",
                "full_code": "29303"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "390.52",
                "full_code": "29304"
              }
            ],
            "descriptor": "",
            "description": "REMOVAL, INLAYS/ONLAYS/ CROWNS/ VENEERS (single units only)",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "RESTORATIVE SERVICES, OTHER"
      }
    ],
    "descriptor": "",
    "description": "RESTORATION\nNote 1: Treatment of dental caries includes pulp protection and local anaesthesia.\nNote 2: Where, at the same appointment, in order to conserve tooth structure,  two separate restorations are performed on the same tooth involving a common surface, when one restoration might have been done; this should be considered as one restoration in assessing the fee.\nNote 3: Finishing restorations is a separate procedure done at a separate appointment (See 16100)",
    "fee": ""
  },
  {
    "full_code": "30000",
    "children": [
      {
        "full_code": "31100",
        "children": [],
        "descriptor": "",
        "description": "PULP CAPPING (refer to code 20100)"
      },
      {
        "full_code": "32000",
        "children": [
          {
            "full_code": "32200",
            "children": [
              {
                "description": "Pulpotomy, Permanent Teeth (as a separate Emergency Procedure)",
                "descriptor": "",
                "fee": "",
                "full_code": "32220"
              },
              {
                "description": "Anterior and Bicuspid Teeth",
                "descriptor": "",
                "fee": "183.75",
                "full_code": "32221"
              },
              {
                "description": "Molar Teeth",
                "descriptor": "",
                "fee": "183.75",
                "full_code": "32222"
              },
              {
                "description": "Pulpotomy, Primary Teeth",
                "descriptor": "",
                "fee": "",
                "full_code": "32230"
              },
              {
                "description": "Primary Tooth, as a Separate Procedure",
                "descriptor": "",
                "fee": "175.02",
                "full_code": "32231"
              },
              {
                "description": "Primary Tooth, Concurrent with Restorations (but excluding final restoration)",
                "descriptor": "",
                "fee": "90.64",
                "full_code": "32232"
              },
              {
                "description": "Pulpotomy, Permanent Teeth, concurrent with restoration (but excluding final restoration)",
                "descriptor": "",
                "fee": "",
                "full_code": "32240"
              },
              {
                "description": "Anterior and bicuspid teeth",
                "descriptor": "",
                "fee": "95.55",
                "full_code": "32241"
              },
              {
                "description": "Molar Teeth",
                "descriptor": "",
                "fee": "95.55",
                "full_code": "32242"
              }
            ],
            "descriptor": "",
            "description": "PULPOTOMY"
          },
          {
            "full_code": "32300",
            "children": [
              {
                "description": "Pulpectomy, Permanent Teeth/Retained Primary Teeth",
                "descriptor": "",
                "fee": "",
                "full_code": "32310"
              },
              {
                "description": "One Canal",
                "descriptor": "",
                "fee": "166.56",
                "full_code": "32311"
              },
              {
                "description": "Two Canals",
                "descriptor": "",
                "fee": "213.64",
                "full_code": "32312"
              },
              {
                "description": "Three Canals",
                "descriptor": "",
                "fee": "288.33",
                "full_code": "32313"
              },
              {
                "description": "Four Canals or more",
                "descriptor": "",
                "fee": "315.88",
                "full_code": "32314"
              },
              {
                "description": "Pulpectomy, Primary Teeth",
                "descriptor": "",
                "fee": "",
                "full_code": "32320"
              },
              {
                "description": "Anterior Tooth",
                "descriptor": "",
                "fee": "141.27",
                "full_code": "32321"
              },
              {
                "description": "Posterior Tooth",
                "descriptor": "",
                "fee": "254.99",
                "full_code": "32322"
              }
            ],
            "descriptor": "",
            "description": "PULPECTOMY (An emergency procedure and/or as a pre-emptive phase to the preparation of the\nroot canal system for obturation)"
          }
        ],
        "descriptor": "",
        "description": "PULP CHAMBER, TREATMENT OF, (excluding final restoration)"
      },
      {
        "full_code": "33000",
        "children": [
          {
            "full_code": "33100",
            "children": [
              {
                "description": "Root Canals, Permanent Teeth/Retained Primary Teeth, One Canal",
                "descriptor": "",
                "fee": "",
                "full_code": "33110"
              },
              {
                "description": "One canal",
                "descriptor": "",
                "fee": "761.19",
                "full_code": "33111"
              },
              {
                "description": "Difficult Access",
                "descriptor": "",
                "fee": "1010.6",
                "full_code": "33112"
              },
              {
                "description": "Exceptional Anatomy",
                "descriptor": "",
                "fee": "1033.6",
                "full_code": "33113"
              },
              {
                "description": "Calcified Canal",
                "descriptor": "",
                "fee": "1062.34",
                "full_code": "33114"
              },
              {
                "description": "Re-treatment of Previously Completed Therapy",
                "descriptor": "",
                "fee": "1028.59",
                "full_code": "33115"
              },
              {
                "description": "Root Canals, Permanent Teeth/Retained Primary Teeth, Two Canals",
                "descriptor": "",
                "fee": "",
                "full_code": "33120"
              },
              {
                "description": "Two canals",
                "descriptor": "",
                "fee": "1108.48",
                "full_code": "33121"
              },
              {
                "description": "Difficult Access",
                "descriptor": "",
                "fee": "1419.66",
                "full_code": "33122"
              },
              {
                "description": "Exceptional Anatomy",
                "descriptor": "",
                "fee": "1419.66",
                "full_code": "33123"
              },
              {
                "description": "Calcified Canal",
                "descriptor": "",
                "fee": "1419.66",
                "full_code": "33124"
              },
              {
                "description": "Retreatment of Previously Completed Therapy",
                "descriptor": "",
                "fee": "1458.75",
                "full_code": "33125"
              },
              {
                "description": "Root Canals, Permanent Teeth/Retained Primary Teeth, Three Canals",
                "descriptor": "",
                "fee": "",
                "full_code": "33130"
              },
              {
                "description": "Three canals",
                "descriptor": "",
                "fee": "1296.32",
                "full_code": "33131"
              },
              {
                "description": "Difficult Access",
                "descriptor": "",
                "fee": "1608.71",
                "full_code": "33132"
              },
              {
                "description": "Exceptional Anatomy",
                "descriptor": "",
                "fee": "1684.65",
                "full_code": "33133"
              },
              {
                "description": "Calcified Canal",
                "descriptor": "",
                "fee": "1598.43",
                "full_code": "33134"
              },
              {
                "description": "Retreatment of Previously Completed Therapy",
                "descriptor": "",
                "fee": "1586.94",
                "full_code": "33135"
              },
              {
                "description": "Root Canals, Permanent Teeth/Retained Primary Teeth, Four or More Canals",
                "descriptor": "",
                "fee": "",
                "full_code": "33140"
              },
              {
                "description": "Four or more canals",
                "descriptor": "",
                "fee": "1636.2",
                "full_code": "33141"
              },
              {
                "description": "Difficult Access",
                "descriptor": "",
                "fee": "1876.83",
                "full_code": "33142"
              },
              {
                "description": "Exceptional Anatomy",
                "descriptor": "",
                "fee": "1876.83",
                "full_code": "33143"
              },
              {
                "description": "Calcified Canal",
                "descriptor": "",
                "fee": "1876.83",
                "full_code": "33144"
              },
              {
                "description": "Retreatment of Previously Completed Therapy",
                "descriptor": "",
                "fee": "1964.2",
                "full_code": "33145"
              }
            ],
            "descriptor": "",
            "description": "ROOT CANALS, PERMANENT TEETH/RETAINED PRIMARY TEETH  (Includes: Clinical procedures with appropriate radiographs, excluding final restoration.) Definitions: Uncomplicated - Virtually straight canal penetrated by size #15 file Difficult Access - Limited jaw opening, unfavourable tooth inclination, through complex restorations eg. Post/core buildups. Exceptional Anatomy - Canal size same as uncomplicated, but made complicated by dens-in-dente or partially developed roots, internal/external resorption. Calcified Canals - Unable to penetrate with size #10 file and not clearly dicernable on a radiograph Re-treatment - Re-treatment of previously completed therapy"
          },
          {
            "full_code": "33500",
            "children": [
              {
                "description": "One canal",
                "descriptor": "",
                "fee": "288.73",
                "full_code": "33501"
              },
              {
                "description": "Two canals",
                "descriptor": "",
                "fee": "433.1",
                "full_code": "33502"
              },
              {
                "description": "Three canals or more",
                "descriptor": "",
                "fee": "577.48",
                "full_code": "33503"
              }
            ],
            "descriptor": "",
            "description": "PULPAL REVASCULARIZATION",
            "fee": ""
          },
          {
            "full_code": "33600",
            "children": [
              {
                "description": "One canal",
                "descriptor": "",
                "fee": "300.22",
                "full_code": "33601"
              },
              {
                "description": "Two canals",
                "descriptor": "",
                "fee": "433.1",
                "full_code": "33602"
              },
              {
                "description": "Three canals",
                "descriptor": "",
                "fee": "577.48",
                "full_code": "33603"
              },
              {
                "description": "Four canals or more",
                "descriptor": "",
                "fee": "769.97",
                "full_code": "33604"
              },
              {
                "description": "Re-Insertion of Dentogenic Media Per Visit",
                "descriptor": "",
                "fee": "",
                "full_code": "33610"
              },
              {
                "description": "One canal",
                "descriptor": "",
                "fee": "144.35",
                "full_code": "33611"
              },
              {
                "description": "Two canals",
                "descriptor": "",
                "fee": "195.95",
                "full_code": "33612"
              },
              {
                "description": "Three canals",
                "descriptor": "",
                "fee": "293.66",
                "full_code": "33613"
              },
              {
                "description": "Four canals or more",
                "descriptor": "",
                "fee": "393.03",
                "full_code": "33614"
              }
            ],
            "descriptor": "",
            "description": "APEXIFICATION/APEXOGENESIS/INDUCTION OF HARD TISSUE REPAIR (to include biomechanical preparation and placement of dentogenic media)",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "ROOT CANAL THERAPY To include: treatment plan, clinical procedures (ie. pulpectomy, biomechanical preparation, chemotherapeutic treatment and obturation), with appropriate radiographs, excluding final restoration."
      },
      {
        "full_code": "34000",
        "children": [
          {
            "full_code": "34100",
            "children": [
              {
                "description": "Maxillary Anterior",
                "descriptor": "",
                "fee": "",
                "full_code": "34110"
              },
              {
                "description": "One root",
                "descriptor": "",
                "fee": "607.67",
                "full_code": "34111"
              },
              {
                "description": "Two roots",
                "descriptor": "",
                "fee": "749.79",
                "full_code": "34112"
              },
              {
                "description": "Maxillary Bicuspid",
                "descriptor": "",
                "fee": "",
                "full_code": "34120"
              },
              {
                "description": "One root",
                "descriptor": "",
                "fee": "749.42",
                "full_code": "34121"
              },
              {
                "description": "Two roots",
                "descriptor": "",
                "fee": "872.02",
                "full_code": "34122"
              },
              {
                "description": "Three roots",
                "descriptor": "",
                "fee": "1071.61",
                "full_code": "34123"
              },
              {
                "description": "Maxillary Molar",
                "descriptor": "",
                "fee": "",
                "full_code": "34130"
              },
              {
                "description": "One root",
                "descriptor": "",
                "fee": "728.73",
                "full_code": "34131"
              },
              {
                "description": "Two roots",
                "descriptor": "",
                "fee": "853.63",
                "full_code": "34132"
              },
              {
                "description": "Three roots",
                "descriptor": "",
                "fee": "1287.32",
                "full_code": "34133"
              },
              {
                "description": "Mandibular Anterior",
                "descriptor": "",
                "fee": "",
                "full_code": "34140"
              },
              {
                "description": "One root",
                "descriptor": "",
                "fee": "631.27",
                "full_code": "34141"
              },
              {
                "description": "Two or more roots",
                "descriptor": "",
                "fee": "857.08",
                "full_code": "34142"
              },
              {
                "description": "Mandibular Bicuspid",
                "descriptor": "",
                "fee": "",
                "full_code": "34150"
              },
              {
                "description": "One root",
                "descriptor": "",
                "fee": "930.27",
                "full_code": "34151"
              },
              {
                "description": "Two roots",
                "descriptor": "",
                "fee": "965.51",
                "full_code": "34152"
              },
              {
                "description": "Three or more roots",
                "descriptor": "",
                "fee": "1178.89",
                "full_code": "34153"
              },
              {
                "description": "Mandibular Molar",
                "descriptor": "",
                "fee": "",
                "full_code": "34160"
              },
              {
                "description": "One root",
                "descriptor": "",
                "fee": "747.49",
                "full_code": "34161"
              },
              {
                "description": "Two roots",
                "descriptor": "",
                "fee": "944.81",
                "full_code": "34162"
              },
              {
                "description": "Three roots",
                "descriptor": "",
                "fee": "1287.32",
                "full_code": "34163"
              }
            ],
            "descriptor": "",
            "description": "APICOECTOMY/APICAL CURETTAGE"
          },
          {
            "full_code": "34200",
            "children": [
              {
                "description": "Maxillary Anterior",
                "descriptor": "",
                "fee": "",
                "full_code": "34210"
              },
              {
                "description": "One canal",
                "descriptor": "",
                "fee": "114.39",
                "full_code": "34211"
              },
              {
                "description": "Two or more canals",
                "descriptor": "",
                "fee": "203.52",
                "full_code": "34212"
              },
              {
                "description": "Maxillary Bicuspid",
                "descriptor": "",
                "fee": "",
                "full_code": "34220"
              },
              {
                "description": "One canal",
                "descriptor": "",
                "fee": "114.39",
                "full_code": "34221"
              },
              {
                "description": "Two canals",
                "descriptor": "",
                "fee": "203.52",
                "full_code": "34222"
              },
              {
                "description": "Three canals",
                "descriptor": "",
                "fee": "307.58",
                "full_code": "34223"
              },
              {
                "description": "Four or more canals",
                "descriptor": "",
                "fee": "409.33",
                "full_code": "34224"
              },
              {
                "description": "Maxillary Molar",
                "descriptor": "",
                "fee": "",
                "full_code": "34230"
              },
              {
                "description": "One canal",
                "descriptor": "",
                "fee": "127.04",
                "full_code": "34231"
              },
              {
                "description": "Two canals",
                "descriptor": "",
                "fee": "203.52",
                "full_code": "34232"
              },
              {
                "description": "Three canals",
                "descriptor": "",
                "fee": "307.58",
                "full_code": "34233"
              },
              {
                "description": "Four or more canals",
                "descriptor": "",
                "fee": "409.33",
                "full_code": "34234"
              },
              {
                "description": "Mandibular Anterior",
                "descriptor": "",
                "fee": "",
                "full_code": "34240"
              },
              {
                "description": "One canal",
                "descriptor": "",
                "fee": "129.34",
                "full_code": "34241"
              },
              {
                "description": "Two or more canals",
                "descriptor": "",
                "fee": "203.52",
                "full_code": "34242"
              },
              {
                "description": "Mandibular Bicuspid",
                "descriptor": "",
                "fee": "",
                "full_code": "34250"
              },
              {
                "description": "One canal",
                "descriptor": "",
                "fee": "101.75",
                "full_code": "34251"
              },
              {
                "description": "Two canals",
                "descriptor": "",
                "fee": "203.52",
                "full_code": "34252"
              },
              {
                "description": "Three canals",
                "descriptor": "",
                "fee": "307.58",
                "full_code": "34253"
              },
              {
                "description": "Four or more canals",
                "descriptor": "",
                "fee": "409.33",
                "full_code": "34254"
              },
              {
                "description": "Mandibular Molar",
                "descriptor": "",
                "fee": "",
                "full_code": "34260"
              },
              {
                "description": "One canal",
                "descriptor": "",
                "fee": "101.75",
                "full_code": "34261"
              },
              {
                "description": "Two canals",
                "descriptor": "",
                "fee": "203.52",
                "full_code": "34262"
              },
              {
                "description": "Three canals",
                "descriptor": "",
                "fee": "307.58",
                "full_code": "34263"
              },
              {
                "description": "Four or more canals",
                "descriptor": "",
                "fee": "409.33",
                "full_code": "34264"
              }
            ],
            "descriptor": "",
            "description": "RETROFILLING"
          },
          {
            "full_code": "34300",
            "children": [
              {
                "description": "Maxillary Anterior",
                "descriptor": "",
                "fee": "",
                "full_code": "34310"
              },
              {
                "description": "One root",
                "descriptor": "",
                "fee": "615.17",
                "full_code": "34311"
              },
              {
                "description": "Two roots",
                "descriptor": "",
                "fee": "857.08",
                "full_code": "34312"
              },
              {
                "description": "Maxillary Bicuspid",
                "descriptor": "",
                "fee": "",
                "full_code": "34320"
              },
              {
                "description": "One root",
                "descriptor": "",
                "fee": "749.79",
                "full_code": "34321"
              },
              {
                "description": "Two roots",
                "descriptor": "",
                "fee": "1017.99",
                "full_code": "34322"
              },
              {
                "description": "Three roots",
                "descriptor": "",
                "fee": "1287.32",
                "full_code": "34323"
              },
              {
                "description": "Maxillary Molar",
                "descriptor": "",
                "fee": "",
                "full_code": "34330"
              },
              {
                "description": "One root",
                "descriptor": "",
                "fee": "749.79",
                "full_code": "34331"
              },
              {
                "description": "Two roots",
                "descriptor": "",
                "fee": "1017.99",
                "full_code": "34332"
              },
              {
                "description": "Three roots",
                "descriptor": "",
                "fee": "1500.74",
                "full_code": "34333"
              },
              {
                "description": "Mandibular Anterior",
                "descriptor": "",
                "fee": "",
                "full_code": "34340"
              },
              {
                "description": "One root",
                "descriptor": "",
                "fee": "770.7",
                "full_code": "34341"
              },
              {
                "description": "Two or more roots",
                "descriptor": "",
                "fee": "1071.61",
                "full_code": "34342"
              },
              {
                "description": "Mandibular Bicuspid",
                "descriptor": "",
                "fee": "",
                "full_code": "34350"
              },
              {
                "description": "One root",
                "descriptor": "",
                "fee": "857.08",
                "full_code": "34351"
              },
              {
                "description": "Two roots",
                "descriptor": "",
                "fee": "1178.89",
                "full_code": "34352"
              },
              {
                "description": "Three roots",
                "descriptor": "",
                "fee": "1393.45",
                "full_code": "34353"
              },
              {
                "description": "Mandibular Molar",
                "descriptor": "",
                "fee": "",
                "full_code": "34360"
              },
              {
                "description": "One root",
                "descriptor": "",
                "fee": "857.08",
                "full_code": "34361"
              },
              {
                "description": "Two roots",
                "descriptor": "",
                "fee": "1126.08",
                "full_code": "34362"
              },
              {
                "description": "Three roots",
                "descriptor": "",
                "fee": "1500.74",
                "full_code": "34363"
              }
            ],
            "descriptor": "",
            "description": "RE-TREATMENT, APICOECTOMY/APICAL CURETTAGE"
          },
          {
            "full_code": "34400",
            "children": [
              {
                "description": "Amputations, Root (includes recontouring tooth and furca)",
                "descriptor": "",
                "fee": "",
                "full_code": "34410"
              },
              {
                "description": "One root",
                "descriptor": "",
                "fee": "421.39",
                "full_code": "34411"
              },
              {
                "description": "Two roots",
                "descriptor": "",
                "fee": "513.4",
                "full_code": "34412"
              },
              {
                "description": "Hemisection",
                "descriptor": "",
                "fee": "",
                "full_code": "34420"
              },
              {
                "description": "Maxillary Bicuspid",
                "descriptor": "",
                "fee": "307.58",
                "full_code": "34421"
              },
              {
                "description": "Maxillary Molar",
                "descriptor": "",
                "fee": "300.68",
                "full_code": "34422"
              },
              {
                "description": "Mandibular Molar",
                "descriptor": "",
                "fee": "300.68",
                "full_code": "34423"
              },
              {
                "description": "Decompression, Perio-Radicular Lesion",
                "descriptor": "",
                "fee": "",
                "full_code": "34430"
              },
              {
                "description": "First visit",
                "descriptor": "",
                "fee": "409.33",
                "full_code": "34431"
              },
              {
                "description": "Each Additional visit",
                "descriptor": "",
                "fee": "203.52",
                "full_code": "34432"
              },
              {
                "description": "Surgery, Endodontic, Exploratory",
                "descriptor": "",
                "fee": "",
                "full_code": "34440"
              },
              {
                "description": "Maxillary Anterior",
                "descriptor": "",
                "fee": "307.58",
                "full_code": "34441"
              },
              {
                "description": "Maxillary Bicuspid",
                "descriptor": "",
                "fee": "409.33",
                "full_code": "34442"
              },
              {
                "description": "Maxillary Molar",
                "descriptor": "",
                "fee": "513.4",
                "full_code": "34443"
              },
              {
                "description": "Mandibular Anterior",
                "descriptor": "",
                "fee": "307.58",
                "full_code": "34444"
              },
              {
                "description": "Mandibular Bicuspid",
                "descriptor": "",
                "fee": "409.33",
                "full_code": "34445"
              },
              {
                "description": "Mandibular Molar",
                "descriptor": "",
                "fee": "513.4",
                "full_code": "34446"
              },
              {
                "description": "Removal, Intentional, of Tooth, Apical Filling and Replantation (splinting additional)",
                "descriptor": "",
                "fee": "",
                "full_code": "34450"
              },
              {
                "description": "Single rooted tooth",
                "descriptor": "",
                "fee": "427.95",
                "full_code": "34451"
              },
              {
                "description": "Two rooted tooth",
                "descriptor": "",
                "fee": "643.66",
                "full_code": "34452"
              },
              {
                "description": "Three rooted tooth or more",
                "descriptor": "",
                "fee": "857.08",
                "full_code": "34453"
              }
            ],
            "descriptor": "",
            "description": "SURGICAL SERVICES, MISCELLANEOUS"
          },
          {
            "full_code": "34500",
            "children": [
              {
                "description": "Perforation/Resorptive Defect(s), Pulp Chamber Repair, or Root Repair, Non-Surgical",
                "descriptor": "",
                "fee": "",
                "full_code": "34510"
              },
              {
                "description": "per tooth",
                "descriptor": "",
                "fee": "93.02",
                "full_code": "34511"
              },
              {
                "description": "Perforation/Resorptive Defect(s), Pulp Chamber Repair, or Root Repair, Surgical",
                "descriptor": "",
                "fee": "",
                "full_code": "34520"
              },
              {
                "description": "Anterior Tooth",
                "descriptor": "",
                "fee": "101.75",
                "full_code": "34521"
              },
              {
                "description": "Bicuspid Tooth",
                "descriptor": "",
                "fee": "204.06",
                "full_code": "34522"
              },
              {
                "description": "Molar Tooth",
                "descriptor": "",
                "fee": "305.28",
                "full_code": "34523"
              }
            ],
            "descriptor": "",
            "description": "PERFORATIONS"
          },
          {
            "full_code": "34600",
            "children": [
              {
                "description": "In Previously Filled Tooth when Root Canal Treatment Done by Another Practitioner",
                "descriptor": "",
                "fee": "97.38",
                "full_code": "34601"
              },
              {
                "description": "In Calcified Canals",
                "descriptor": "",
                "fee": "293.33",
                "full_code": "34602"
              }
            ],
            "descriptor": "",
            "description": "ENLARGEMENT, CANAL AND/OR PULP CHAMBER (Preparation of Post Space)",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "PERIAPICAL SERVICES"
      },
      {
        "full_code": "39000",
        "children": [
          {
            "full_code": "39100",
            "children": [
              {
                "description": "Banding and/or Coronal Buildup of Tooth/Teeth and/or Contouring of Tissue Surrounding Tooth/Teeth\nto Maintain Aseptic Operating Field (per tooth)",
                "descriptor": "",
                "fee": "183.75",
                "full_code": "39101"
              }
            ],
            "descriptor": "",
            "description": "ISOLATION OF ENDODONTIC TOOTH/TEETH FOR ASEPSIS",
            "fee": ""
          },
          {
            "full_code": "39200",
            "children": [
              {
                "description": "Anteriors and Bicuspids",
                "descriptor": "",
                "fee": "87.53",
                "full_code": "39201"
              },
              {
                "description": "Molars",
                "descriptor": "",
                "fee": "87.53",
                "full_code": "39202"
              },
              {
                "description": "Opening Through Artificial Crown (In addition to Procedures)",
                "descriptor": "",
                "fee": "",
                "full_code": "39210"
              },
              {
                "description": "Anteriors and Bicuspids",
                "descriptor": "",
                "fee": "96.71",
                "full_code": "39211"
              },
              {
                "description": "Molars",
                "descriptor": "",
                "fee": "96.71",
                "full_code": "39212"
              }
            ],
            "descriptor": "",
            "description": "OPEN AND DRAIN (Separate Emergency Procedures)",
            "fee": ""
          },
          {
            "full_code": "39300",
            "children": [
              {
                "description": "Bleaching Endodontically Treated Tooth/Teeth",
                "descriptor": "",
                "fee": "",
                "full_code": "39310"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "93.03",
                "full_code": "39311"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "186.06",
                "full_code": "39312"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "279.09",
                "full_code": "39313"
              },
              {
                "description": "Each additional unit over three",
                "descriptor": "",
                "fee": "93.03",
                "full_code": "39319"
              }
            ],
            "descriptor": "",
            "description": "BLEACHING, NON VITAL"
          },
          {
            "full_code": "39400",
            "children": [
              {
                "description": "Exploratory Access",
                "descriptor": "",
                "fee": "",
                "full_code": "39410"
              },
              {
                "description": "Anterior",
                "descriptor": "",
                "fee": "83.15",
                "full_code": "39411"
              },
              {
                "description": "Bicuspid",
                "descriptor": "",
                "fee": "83.15",
                "full_code": "39412"
              },
              {
                "description": "Molar",
                "descriptor": "",
                "fee": "174.64",
                "full_code": "39413"
              }
            ],
            "descriptor": "",
            "description": "EXPLORATORY ACCESS THROUGH CLINICAL CROWN OF PREVIOUSLY TREATED TOOTH"
          }
        ],
        "descriptor": "",
        "description": "ENDODONTIC, PROCEDURES, MISCELLANEOUS"
      }
    ],
    "descriptor": "",
    "description": "ENDODONTICS General Endodontic Procedures There are certain Endodontic cases, which, as a result of a previous treatment, tooth position, anatomy and/or stage of development, require additional time and care. Such situations could merit an additional fee. Conservative root canal therapy includes treatment plan, clinical procedures with appropriate follow up care. Excludes final restoration. Note: If Endodontic therapy is not completed it would be deemed reasonable to charge a portion of the suggested fee in relation to time expended in the procedure."
  },
  {
    "full_code": "40000",
    "children": [
      {
        "full_code": "41000",
        "children": [
          {
            "full_code": "41200",
            "children": [
              {
                "description": "Oral Manifestations, Oral Mucosal Disorders, Mucocutaneous disorders and diseases of localized mucosal conditions, e.g. lichen planus, aphthous stomatitis, benign mucous membrane pemphigoid, pemphigus, salivary gland tumours, leukoplakia with and without dysphasia, neoplasms, hairy leukoplakia, polyps, verrucae, fibroma etc.",
                "descriptor": "",
                "fee": "",
                "full_code": "41210"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "91.87",
                "full_code": "41211"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "183.74",
                "full_code": "41212"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "275.61",
                "full_code": "41213"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "367.48",
                "full_code": "41214"
              },
              {
                "description": "Each additional unit over four",
                "descriptor": "",
                "fee": "91.87",
                "full_code": "41219"
              },
              {
                "description": "Nervous and Muscular Disorders, Disorders of facial sensation and motor dysfunction at the jaw, e.g. trigeminal neuralgia, atypical facial pain, atypical odontologia, burning mouth syndrome, dyskenesia, post injection trismus, muscular and joint pain syndrome",
                "descriptor": "",
                "fee": "",
                "full_code": "41220"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "91.87",
                "full_code": "41221"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "183.74",
                "full_code": "41222"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "275.61",
                "full_code": "41223"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "367.48",
                "full_code": "41224"
              },
              {
                "description": "Each additional unit over four",
                "descriptor": "",
                "fee": "91.87",
                "full_code": "41229"
              },
              {
                "description": "Oral Manifestations of Systemic Disease or complications of medical therapy e.g. complications of chemotherapy, radiation therapy, post operative neuropathics, post surgical or radiation therapy, dysfunction, oral manifestations of lupus erythematosis and systemic disease including leukemia, diabetes and bleeding disorders (e.g. haemophilia)",
                "descriptor": "",
                "fee": "",
                "full_code": "41230"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "91.87",
                "full_code": "41231"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "183.74",
                "full_code": "41232"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "275.61",
                "full_code": "41233"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "367.48",
                "full_code": "41234"
              },
              {
                "description": "Each additional unit over four",
                "descriptor": "",
                "fee": "91.87",
                "full_code": "41239"
              }
            ],
            "descriptor": "",
            "description": "ORAL DISEASE, Management of"
          },
          {
            "full_code": "41300",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "91.87",
                "full_code": "41301"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "183.74",
                "full_code": "41302"
              },
              {
                "description": "Each additional unit over two",
                "descriptor": "",
                "fee": "91.87",
                "full_code": "41309"
              }
            ],
            "descriptor": "",
            "description": "DESENSITIZATION (This may involve application and burnishing of medicinal aids on the root or the use of a variety of therapeutic procedures.  More than one appointment may be necessary.)",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "PERIODONTAL SERVICES, NON SURGICAL"
      },
      {
        "full_code": "42000",
        "children": [
          {
            "full_code": "42100",
            "children": [
              {
                "description": "Surgical Curettage, To Include Definitive Root Planing",
                "descriptor": "",
                "fee": "",
                "full_code": "42110"
              },
              {
                "description": "Per sextant",
                "descriptor": "",
                "fee": "240.61",
                "full_code": "42111"
              }
            ],
            "descriptor": "",
            "description": "PERIODONTAL SURGERY, GINGIVAL CURETTAGE"
          },
          {
            "full_code": "42200",
            "children": [
              {
                "description": "Per sextant",
                "descriptor": "",
                "fee": "288.73",
                "full_code": "42201"
              }
            ],
            "descriptor": "",
            "description": "PERIODONTAL SURGERY, GINGIVOPLASTY (Does not include limited re-contouring to facilitate\nrestorative services)",
            "fee": ""
          },
          {
            "full_code": "42300",
            "children": [
              {
                "description": "Gingivectomy, Uncomplicated",
                "descriptor": "",
                "fee": "",
                "full_code": "42310"
              },
              {
                "description": "Per sextant",
                "descriptor": "",
                "fee": "328.03",
                "full_code": "42311"
              },
              {
                "description": "Gingivectomy, Complicated",
                "descriptor": "",
                "fee": "",
                "full_code": "42320"
              },
              {
                "description": "Per sextant",
                "descriptor": "",
                "fee": "484.51",
                "full_code": "42321"
              },
              {
                "description": "Gingival Fiber Incision (supra crestal fibrotomy)",
                "descriptor": "",
                "fee": "",
                "full_code": "42330"
              },
              {
                "description": "First tooth",
                "descriptor": "",
                "fee": "93.5",
                "full_code": "42331"
              },
              {
                "description": "Each additional tooth",
                "descriptor": "",
                "fee": "83.15",
                "full_code": "42339"
              }
            ],
            "descriptor": "",
            "description": "PERIODONTAL SURGERY, GINGIVECTOMY (The procedure by which gingival deformities are reshaped and reduced to create normal and functional form, when the pocket is uncomplicated by extension into the underlying bone; does not include limited re-contouring to facilitate restorative services)."
          },
          {
            "full_code": "42400",
            "children": [
              {
                "description": "Flap Approach, With Osteoplasty and/or Ostectomy",
                "descriptor": "",
                "fee": "",
                "full_code": "42410"
              },
              {
                "description": "Per sextant",
                "descriptor": "",
                "fee": "1181.58",
                "full_code": "42411"
              },
              {
                "description": "Flap Approach, With Curettage of Osseous Defect",
                "descriptor": "",
                "fee": "",
                "full_code": "42420"
              },
              {
                "description": "Per sextant",
                "descriptor": "",
                "fee": "782.01",
                "full_code": "42421"
              },
              {
                "description": "Flap Approach, With Curettage of Osseous Defect with Osteoplasty and/or Ostectomy",
                "descriptor": "",
                "fee": "",
                "full_code": "42430"
              },
              {
                "description": "Per sextant",
                "descriptor": "",
                "fee": "1114.13",
                "full_code": "42431"
              },
              {
                "description": "Flap Approach, Exploratory (for diagnosis)",
                "descriptor": "",
                "fee": "",
                "full_code": "42440"
              },
              {
                "description": "Per site",
                "descriptor": "",
                "fee": "601.39",
                "full_code": "42441"
              }
            ],
            "descriptor": "",
            "description": "PERIODONTAL SURGERY, FLAP APPROACH"
          },
          {
            "full_code": "42500",
            "children": [
              {
                "description": "Grafts, Soft Tissue, Pedicle (including apically or lateral sliding and rotated flaps.)",
                "descriptor": "",
                "fee": "",
                "full_code": "42510"
              },
              {
                "description": "Per site",
                "descriptor": "",
                "fee": "734.65",
                "full_code": "42511"
              },
              {
                "description": "Periosteal stimulation in addition to 42511",
                "descriptor": "",
                "fee": "87.51",
                "full_code": "42512"
              },
              {
                "description": "Grafts, Soft Tissue, Pedicle (Coronally Positioned)",
                "descriptor": "",
                "fee": "",
                "full_code": "42520"
              },
              {
                "description": "Per site",
                "descriptor": "",
                "fee": "734.65",
                "full_code": "42521"
              },
              {
                "description": "Periosteal stimulation in addition to 42521",
                "descriptor": "",
                "fee": "87.51",
                "full_code": "42522"
              },
              {
                "description": "Grafts Free Soft Tissue",
                "descriptor": "",
                "fee": "",
                "full_code": "42530"
              },
              {
                "description": "Adjacent to teeth or edentulous area, per site.",
                "descriptor": "",
                "fee": "1109.41",
                "full_code": "42531"
              },
              {
                "description": "Grafts, Soft Tissue, Pedicle, With Free Graft Placed In Pedicle Donor Site",
                "descriptor": "",
                "fee": "",
                "full_code": "42540"
              },
              {
                "description": "Per site",
                "descriptor": "",
                "fee": "1341.05",
                "full_code": "42541"
              },
              {
                "description": "Grafts, For Root or Implant Coverage",
                "descriptor": "",
                "fee": "",
                "full_code": "42550"
              },
              {
                "description": "Autograft (subepithelial connective tissue or epithelialized gingival graft), for root\ncoverage, includes harvesting from donor site - Per site",
                "descriptor": "",
                "fee": "1053.66",
                "full_code": "42551"
              },
              {
                "description": "Allograft, for root coverage \u2013 per site",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "42552"
              },
              {
                "description": "Autograft (subepithelial connective tissue or epithelialized gingival graft), adjacent to an implant,\nincludes harvesting from donor site \u2013 per site",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "42556"
              },
              {
                "description": "Allograft, adjacent to an implant \u2013 per site",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "42557"
              },
              {
                "description": "Grafts, For Ridge Augmentation",
                "descriptor": "",
                "fee": "",
                "full_code": "42560"
              },
              {
                "description": "Autograft (free connective tissue), includes harvesting from donor site \u2013 per site.",
                "descriptor": "",
                "fee": "1299.39",
                "full_code": "42561"
              },
              {
                "description": "Allograft \u2013 per site",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "42562"
              },
              {
                "description": "Grafts, Connective Tissue, Pedicle with Free Graft for Root Coverage",
                "descriptor": "",
                "fee": "",
                "full_code": "42570"
              },
              {
                "description": "Per site",
                "descriptor": "",
                "fee": "1005.97",
                "full_code": "42571"
              },
              {
                "description": "Grafts, Gingival Onlay (for ridge augmentation)",
                "descriptor": "",
                "fee": "",
                "full_code": "42580"
              },
              {
                "description": "Per site",
                "descriptor": "",
                "fee": "1040.73",
                "full_code": "42581"
              },
              {
                "description": "Grafts, Dermal, Onlay, for Ridge Augmentation",
                "descriptor": "",
                "fee": "",
                "full_code": "42590"
              },
              {
                "description": "Autograft \u2013 per site",
                "descriptor": "",
                "fee": "1040.73",
                "full_code": "42591"
              },
              {
                "description": "Allograft \u2013 per site",
                "descriptor": "+E",
                "fee": "1040.74",
                "full_code": "42592"
              }
            ],
            "descriptor": "",
            "description": "PERIODONTAL SURGERY, FLAPS, GRAFTS, SOFT TISSUE"
          },
          {
            "full_code": "42600",
            "children": [
              {
                "description": "Grafts, Osseous, Autograft (Including Flap Entry, Closure and Donor Site)",
                "descriptor": "",
                "fee": "",
                "full_code": "42610"
              },
              {
                "description": "Per site",
                "descriptor": "",
                "fee": "1224.41",
                "full_code": "42611"
              },
              {
                "description": "Grafts, Osseous, Allograft (Including Flap Entry and Closure)",
                "descriptor": "",
                "fee": "",
                "full_code": "42620"
              },
              {
                "description": "Per site",
                "descriptor": "+E",
                "fee": "1224.41",
                "full_code": "42621"
              },
              {
                "description": "Grafts, Osseous, Xenograft (Including Flap Entry and Closure)",
                "descriptor": "",
                "fee": "",
                "full_code": "42630"
              },
              {
                "description": "Per Site",
                "descriptor": "+E",
                "fee": "1224.41",
                "full_code": "42631"
              }
            ],
            "descriptor": "",
            "description": "PERIODONTAL SURGERY, FLAPS, GRAFTS, OSSEOUS TISSUE"
          },
          {
            "full_code": "42700",
            "children": [
              {
                "description": "Guided Tissue Regeneration \u2013 Non-resorbable Membrane \u2013 per site",
                "descriptor": "+E",
                "fee": "1858.84",
                "full_code": "42701"
              },
              {
                "description": "Guided Tissue Regeneration \u2013 Resorbable Membrane",
                "descriptor": "+E",
                "fee": "1858.84",
                "full_code": "42702"
              },
              {
                "description": "Guided Tissue Regeneration \u2013 Non-resorbable Membrane, Surgical Re-entry for Removal",
                "descriptor": "+E",
                "fee": "1858.84",
                "full_code": "42703"
              },
              {
                "description": "Biological Materials to Aid in Soft and Osseous Tissue Regeneration (not including surgical entry and\nclosure)",
                "descriptor": "",
                "fee": "",
                "full_code": "42720"
              },
              {
                "description": "Per site",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "42721"
              }
            ],
            "descriptor": "",
            "description": "GUIDED TISSUE REGENERATION",
            "fee": ""
          },
          {
            "full_code": "42800",
            "children": [
              {
                "description": "Proximal Wedge Procedure (as a separate procedure)",
                "descriptor": "",
                "fee": "",
                "full_code": "42810"
              },
              {
                "description": "With Flap Curettage, per site",
                "descriptor": "",
                "fee": "557.67",
                "full_code": "42811"
              },
              {
                "description": "With Flap Curettage and Ostectomy/Osteoplasty, per site",
                "descriptor": "",
                "fee": "673.42",
                "full_code": "42819"
              },
              {
                "description": "Post Surgical Periodontal Treatment Visit Per Dressing Change (by dentist other than operating dentist)",
                "descriptor": "",
                "fee": "",
                "full_code": "42820"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "87.51",
                "full_code": "42821"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "175.02",
                "full_code": "42822"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "262.53",
                "full_code": "42823"
              },
              {
                "description": "Each additional unit over three",
                "descriptor": "",
                "fee": "87.51",
                "full_code": "42829"
              },
              {
                "description": "Periodontal Abscess or Pericoronitis, May Include Any of The Following Procedures: Lancing, Scaling,\nCurettage, Surgery or Medication",
                "descriptor": "",
                "fee": "",
                "full_code": "42830"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "91.87",
                "full_code": "42831"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "183.74",
                "full_code": "42832"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "275.61",
                "full_code": "42833"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "367.48",
                "full_code": "42834"
              },
              {
                "description": "Each additional unit over four",
                "descriptor": "",
                "fee": "91.87",
                "full_code": "42839"
              },
              {
                "description": "Flap Approach for Creation of Interdental Papillae",
                "descriptor": "",
                "fee": "",
                "full_code": "42840"
              },
              {
                "description": "Per Site",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "42841"
              },
              {
                "description": "Flapless Approach, with Osteoplasty/Ostectomy for Crown Lengthening",
                "descriptor": "",
                "fee": "",
                "full_code": "42850"
              },
              {
                "description": "Per site",
                "descriptor": "",
                "fee": "183.75",
                "full_code": "42851"
              }
            ],
            "descriptor": "",
            "description": "PERIODONTAL SURGERY, MISCELLANEOUS PROCEDURES"
          }
        ],
        "descriptor": "",
        "description": "PERIODONTAL SERVICES, SURGICAL (Includes local anaesthetic, suturing and the placement and removal of initial surgical dressing.  A surgical site is an area that lends itself to one or more procedures.  It is considered to include a full quadrant, sextant or group of  teeth or in some cases a single tooth.)"
      },
      {
        "full_code": "43000",
        "children": [
          {
            "full_code": "43100",
            "children": [
              {
                "description": "\"A\" Splint (restorative material plus wire, fibre ribbon or rope)",
                "descriptor": "",
                "fee": "",
                "full_code": "43110"
              },
              {
                "description": "Per joint",
                "descriptor": "+E",
                "fee": "177.32",
                "full_code": "43111"
              }
            ],
            "descriptor": "",
            "description": "PERIODONTAL SPLINTING OR LIGATION, INTRA CORONAL  Note: This procedure is in addition to the\nusual code for the tooth preparation on either side"
          },
          {
            "full_code": "43200",
            "children": [
              {
                "description": "Bonded, Interproximal Enamel Splint",
                "descriptor": "",
                "fee": "",
                "full_code": "43220"
              },
              {
                "description": "Per joint",
                "descriptor": "",
                "fee": "87.51",
                "full_code": "43221"
              },
              {
                "description": "Wire Ligation",
                "descriptor": "",
                "fee": "",
                "full_code": "43230"
              },
              {
                "description": "Per joint",
                "descriptor": "",
                "fee": "87.51",
                "full_code": "43231"
              },
              {
                "description": "Wire Ligation, Restorative Material Covered",
                "descriptor": "",
                "fee": "",
                "full_code": "43240"
              },
              {
                "description": "Per joint",
                "descriptor": "",
                "fee": "87.51",
                "full_code": "43241"
              },
              {
                "description": "Orthodontic Band Splint",
                "descriptor": "",
                "fee": "",
                "full_code": "43260"
              },
              {
                "description": "Per band",
                "descriptor": "+E",
                "fee": "87.51",
                "full_code": "43261"
              },
              {
                "description": "Cast/Soldered/Ceramic/Polymer Glass/Wire/Fibre Ribbon, Splint Bonded",
                "descriptor": "",
                "fee": "",
                "full_code": "43270"
              },
              {
                "description": "Indirect, Per abutment",
                "descriptor": "+L",
                "fee": "87.51",
                "full_code": "43271"
              },
              {
                "description": "Direct, Per abutment",
                "descriptor": "+E",
                "fee": "87.51",
                "full_code": "43272"
              },
              {
                "description": "Removal of Fixed Periodontal Splints",
                "descriptor": "",
                "fee": "",
                "full_code": "43280"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "87.51",
                "full_code": "43281"
              },
              {
                "description": "Each additional unit of time",
                "descriptor": "",
                "fee": "87.51",
                "full_code": "43289"
              }
            ],
            "descriptor": "",
            "description": "PERIODONTAL SPLINTING OR LIGATION,  EXTRA CORONAL"
          },
          {
            "full_code": "43400",
            "children": [
              {
                "description": "Root Planing",
                "descriptor": "",
                "fee": "",
                "full_code": "43420"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "83.55",
                "full_code": "43421"
              },
              {
                "description": "Two units of time",
                "descriptor": "",
                "fee": "167.1",
                "full_code": "43422"
              },
              {
                "description": "Three units of time",
                "descriptor": "",
                "fee": "250.65",
                "full_code": "43423"
              },
              {
                "description": "Four units of time",
                "descriptor": "",
                "fee": "334.2",
                "full_code": "43424"
              },
              {
                "description": "Five units of time",
                "descriptor": "",
                "fee": "417.75",
                "full_code": "43425"
              },
              {
                "description": "Six units of time",
                "descriptor": "",
                "fee": "501.3",
                "full_code": "43426"
              },
              {
                "description": "1/2 unit of time",
                "descriptor": "",
                "fee": "41.78",
                "full_code": "43427"
              },
              {
                "description": "Each additional unit over six",
                "descriptor": "",
                "fee": "83.55",
                "full_code": "43429"
              }
            ],
            "descriptor": "",
            "description": "ROOT PLANING, PERIODONTAL"
          },
          {
            "full_code": "43500",
            "children": [
              {
                "description": "Chemotherapeutic and/or Antimicrobial Agents, Topical Application",
                "descriptor": "",
                "fee": "",
                "full_code": "43510"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "87.51",
                "full_code": "43511"
              },
              {
                "description": "Each additional unit of time",
                "descriptor": "",
                "fee": "87.51",
                "full_code": "43519"
              },
              {
                "description": "Chemotherapeutic and/or Antimicrobial Therapy, Intra-Sulcular Application",
                "descriptor": "",
                "fee": "",
                "full_code": "43520"
              },
              {
                "description": "One unit of time",
                "descriptor": "+E",
                "fee": "91.87",
                "full_code": "43521"
              },
              {
                "description": "Each additional unit of time",
                "descriptor": "+E",
                "fee": "91.87",
                "full_code": "43529"
              }
            ],
            "descriptor": "",
            "description": "CHEMOTHERAPEUTIC and/or ANTIMICROBIAL AGENTS"
          }
        ],
        "descriptor": "",
        "description": "PERIODONTAL PROCEDURES, ADJUNCTIVE (when per joint is designated, the corresponding tooth code is represented by the mesial of the tooth involved, except at the midline, where the tooth to the right of the joint is utilized)"
      },
      {
        "full_code": "49000",
        "children": [
          {
            "full_code": "49100",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "87.51",
                "full_code": "49101"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "175.02",
                "full_code": "49102"
              },
              {
                "description": "Each additional unit over two",
                "descriptor": "",
                "fee": "87.51",
                "full_code": "49109"
              }
            ],
            "descriptor": "",
            "description": "PERIODONTAL RE-EVALUATION/EVALUATION Note: This follow-up service applies to the evaluation of ongoing periodontal treatment or to a post- surgical re-evaluation performed more than one (1) month after surgery, or if performed by another practitioner",
            "fee": ""
          },
          {
            "full_code": "49300",
            "children": [
              {
                "description": "Gingival Mask\n(Removable appliance to mask unaesthetic embrasures Note: For extensive gingival prostheses required after maxillofacial surgery see sub-classification 57300 Prosthesis Maxillofacial, other, code 57372 Gingival Prosthesis)",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "49301"
              }
            ],
            "descriptor": "",
            "description": "SOFT TISSUE PROSTHESIS",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "PERIODONTAL SERVICES, MISCELLANEOUS"
      }
    ],
    "descriptor": "",
    "description": "PERIODONTICS In the treatment of periodontal diseases, variables such as the severity of the patient's periodontal condition and the distribution (i.e. extent) of the condition may require a relatively wide selection of therapeutic procedures and involve considerable variation in time and expense.  In most instances the time required to perform a certain procedure could, and usually does, vary from one quadrant to another and therefore the amounts of time as outlined in the following guide could vary in the management of a particular case."
  },
  {
    "full_code": "50000",
    "children": [
      {
        "full_code": "51000",
        "children": [
          {
            "full_code": "51100",
            "children": [
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "931.16",
                "full_code": "51101"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "931.16",
                "full_code": "51102"
              },
              {
                "description": "Liners, Processed, Resilient, in addition to above",
                "descriptor": "",
                "fee": "LAB",
                "full_code": "51104"
              }
            ],
            "descriptor": "",
            "description": "DENTURE COMPLETE, STANDARD",
            "fee": ""
          },
          {
            "full_code": "51200",
            "children": [
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1924.96",
                "full_code": "51201"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1924.96",
                "full_code": "51202"
              },
              {
                "description": "Liners, Processed, Resilient in addition to above",
                "descriptor": "",
                "fee": "LAB",
                "full_code": "51204"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, COMPLETE, COMPLEX",
            "fee": ""
          },
          {
            "full_code": "51300",
            "children": [
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "931.16",
                "full_code": "51301"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "931.16",
                "full_code": "51302"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, SURGICAL, STANDARD, (IMMEDIATE) (includes first tissue conditioner, but not a processed reline)",
            "fee": ""
          },
          {
            "full_code": "51400",
            "children": [
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1317.07",
                "full_code": "51401"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1317.07",
                "full_code": "51402"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, SURGICAL, COMPLEX (IMMEDIATE) (includes first tissue conditioner, but not a processed reline)",
            "fee": ""
          },
          {
            "full_code": "51500",
            "children": [
              {
                "description": "Maxillary",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "51501"
              },
              {
                "description": "Mandibular",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "51502"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, COMPLETE, GNATHOLOGICAL (CAST BASE AND METAL OCCLUSALS)",
            "fee": ""
          },
          {
            "full_code": "51600",
            "children": [
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "643.22",
                "full_code": "51601"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "643.22",
                "full_code": "51602"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, COMPLETE, PROVISIONAL",
            "fee": ""
          },
          {
            "full_code": "51700",
            "children": [
              {
                "description": "Dentures, Complete, Overdentures, Tissue Borne, Supported by Natural Teeth with or without Coping\nCrowns, no Attachments",
                "descriptor": "",
                "fee": "",
                "full_code": "51710"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1215.77",
                "full_code": "51711"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1215.77",
                "full_code": "51712"
              },
              {
                "description": "Dentures, Complete, Overdentures, Tissue Borne, Supported by Implants with or without Coping\nCrowns, no Attachments",
                "descriptor": "",
                "fee": "",
                "full_code": "51720"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1215.77",
                "full_code": "51721"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1215.77",
                "full_code": "51722"
              },
              {
                "description": "Dentures, Complete, Overdentures Tissue Borne, Supported by a Combination of Natural Teeth and\nImplants with or without Coping Crowns, no Attachments",
                "descriptor": "",
                "fee": "",
                "full_code": "51730"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1215.77",
                "full_code": "51731"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1215.77",
                "full_code": "51732"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, COMPLETE, OVERDENTURES, TISSUE BORNE, SUPPORTED BY NATURAL TEETH OR\nIMPLANTS WITH OR WITHOUT COPING CROWNS, NO ATTACHMENTS"
          },
          {
            "full_code": "51800",
            "children": [
              {
                "description": "Dentures, Complete, Overdentures (Immediate), Tissue Borne, Supported by Natural Teeth with or without Implants with or without Coping Crowns, no Attachments (includes first tissue conditioner,\nbut not a processed reline)",
                "descriptor": "",
                "fee": "",
                "full_code": "51810"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1102.59",
                "full_code": "51811"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1102.59",
                "full_code": "51812"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, COMPLETE, OVERDENTURES, (IMMEDIATE), TISSUE BORNE, SUPPORTED BY NATURAL\nTEETH OR IMPLANTS WITH OR WITHOUT COPING CROWNS, NO ATTACHMENTS"
          },
          {
            "full_code": "51900",
            "children": [
              {
                "description": "Dentures, Complete, Overdentures, Tissue Borne, with Independent Attachments Secured to Natural\nTeeth with or without Coping Crowns",
                "descriptor": "",
                "fee": "",
                "full_code": "51910"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1102.59",
                "full_code": "51911"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1102.59",
                "full_code": "51912"
              },
              {
                "description": "Dentures, Complete, Overdentures, Tissue Borne, with Independent Attachments Secured to\nImplants with or without Coping Crowns",
                "descriptor": "",
                "fee": "",
                "full_code": "51920"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "51921"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "51922"
              },
              {
                "description": "Dentures, Complete, Overdentures, Tissue Borne, with Independent Attachments Secured to a\nCombination of Natural Teeth and Implants with or without Coping Crowns",
                "descriptor": "",
                "fee": "",
                "full_code": "51930"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "51931"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "51932"
              },
              {
                "description": "Dentures, Complete, Overdentures, Tissue Borne, with Retention from a Retentive Bar, Secured to\nCoping Crowns Supported by Implants",
                "descriptor": "",
                "fee": "",
                "full_code": "51950"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "51951"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "51952"
              },
              {
                "description": "Dentures, Complete, Overdentures, Tissue Borne, with Retention from a Retentive Bar, Secured to\nCoping Crowns Supported by a Combination of a Natural Teeth and Implants (see 62105 for Retentive Bar)",
                "descriptor": "",
                "fee": "",
                "full_code": "51960"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "51961"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "51962"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, COMPLETE, OVERDENTURES, TISSUE BORNE, SECURED  BY ATTACHMENTS TO NATURAL\nTEETH OR IMPLANTS"
          }
        ],
        "descriptor": "",
        "description": "DENTURE COMPLETE (includes: impressions, initial and final jaw relation records, try-in evaluation and check records, insertion and adjustments, including three month post insertion care)"
      },
      {
        "full_code": "52000",
        "children": [
          {
            "full_code": "52100",
            "children": [
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "268.07",
                "full_code": "52101"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "268.07",
                "full_code": "52102"
              },
              {
                "description": "Dentures, Partial, Acrylic Base (Immediate) (includes first tissue conditioner, but not a processed reline)",
                "descriptor": "",
                "fee": "",
                "full_code": "52110"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "268.07",
                "full_code": "52111"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "268.07",
                "full_code": "52112"
              }
            ],
            "descriptor": "",
            "description": "Dentures, Partial, Acrylic Base (Provisional)  (With or Without Clasps)",
            "fee": ""
          },
          {
            "full_code": "52200",
            "children": [
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "268.07",
                "full_code": "52201"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "268.07",
                "full_code": "52202"
              },
              {
                "description": "Dentures, Partial, Polymer, Resilient Retainer, (Immediate) (includes first tissue conditioner, but not a processed reline)",
                "descriptor": "",
                "fee": "",
                "full_code": "52210"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "268.07",
                "full_code": "52211"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "268.07",
                "full_code": "52212"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, PARTIAL, POLYMER, RESILIENT RETAINER",
            "fee": ""
          },
          {
            "full_code": "52300",
            "children": [
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "901.4",
                "full_code": "52301"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "901.4",
                "full_code": "52302"
              },
              {
                "description": "Dentures, Partial, Acrylic, with Metal Wrought/Cast Clasps and/or Rests, (Immediate) (includes first tissue conditioner, but not a processed reline)",
                "descriptor": "",
                "fee": "",
                "full_code": "52310"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "901.4",
                "full_code": "52311"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "901.4",
                "full_code": "52312"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, PARTIAL, ACRYLIC, WITH METAL WROUGHT/CAST CLASPS AND/OR RESTS",
            "fee": ""
          },
          {
            "full_code": "52400",
            "children": [
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "901.4",
                "full_code": "52401"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "901.4",
                "full_code": "52402"
              },
              {
                "description": "Dentures, Partial, Acrylic, with Metal Wrought Palatal/Lingual Bar and Clasps and/or Rests,\n(Immediate) (includes first tissue conditioner, but not a processed reline)",
                "descriptor": "",
                "fee": "",
                "full_code": "52410"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "901.4",
                "full_code": "52411"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "901.4",
                "full_code": "52412"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, PARTIAL, ACRYLIC, WITH METAL/WROUGHT PALATAL/LINGUAL BAR AND CLASPS\nAND/OR RESTS",
            "fee": ""
          },
          {
            "full_code": "52510",
            "children": [
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "661.19",
                "full_code": "52511"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "661.19",
                "full_code": "52512"
              }
            ],
            "descriptor": "",
            "description": "Dentures, Partial (Flexible, Non Metal, Non Acrylic)",
            "fee": ""
          },
          {
            "full_code": "52700",
            "children": [
              {
                "description": "Dentures, Partial, Overdentures, Acrylic, with Cast/Wrought Clasps and/or Rests supported by\nNatural Teeth with or without Coping Crowns, no attachments",
                "descriptor": "",
                "fee": "",
                "full_code": "52710"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1105.48",
                "full_code": "52711"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1105.48",
                "full_code": "52712"
              },
              {
                "description": "Dentures, Partial, Overdentures, Acrylic, with Cast/ Wrought Clasps and/or Rests,  Supported by\nImplants with or without Coping Crowns, no attachments",
                "descriptor": "",
                "fee": "",
                "full_code": "52720"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1105.48",
                "full_code": "52721"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1105.48",
                "full_code": "52722"
              },
              {
                "description": "Dentures, Partial, Overdentures, Acrylic, with Cast/Wrought Clasps and/or Rests Supported by a Combination of Natural Teeth and Implants with or without Coping Crowns,  no attachments",
                "descriptor": "",
                "fee": "",
                "full_code": "52730"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1105.48",
                "full_code": "52731"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1105.48",
                "full_code": "52732"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, PARTIAL, OVERDENTURES, ACRYLIC, WITH CAST/WROUGHT CLASPS AND/OR RESTS\nSUPPORTED BY NATURAL TEETH OR IMPLANTS WITH OR WITHOUT COPING CROWNS, NO ATTACHMENTS"
          },
          {
            "full_code": "52800",
            "children": [
              {
                "description": "Dentures, Partial, Overdentures (Immediate), Acrylic, with Cast/Wrought Clasps and/or Rests Supported by Natural Teeth with or without Coping Crowns, no Attachments (includes first tissue\nconditioner, but not a processed reline)",
                "descriptor": "",
                "fee": "",
                "full_code": "52810"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1105.48",
                "full_code": "52811"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1105.48",
                "full_code": "52812"
              },
              {
                "description": "Dentures, Partial, Overdentures (Immediate), Acrylic, with Cast/Wrought Clasps and/or Rests\nSupported by Implants with or without Coping Crowns, no Attachments (includes first tissue conditioner, but not a processed reline)",
                "descriptor": "",
                "fee": "",
                "full_code": "52820"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1105.48",
                "full_code": "52821"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1105.48",
                "full_code": "52822"
              },
              {
                "description": "Dentures, Partial, Overdentures (Immediate), Acrylic with Cast/Wrought Clasps and/or Rests Secured by a Combination of Natural Teeth and Implants with or without Coping Crowns, no Attachments\n(includes first tissue conditioner,  but not a processed reline)",
                "descriptor": "",
                "fee": "",
                "full_code": "52830"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1105.48",
                "full_code": "52831"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1105.48",
                "full_code": "52832"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, PARTIAL, OVERDENTURES (IMMEDIATE), ACRYLIC, WITH CAST/WROUGHT CLASPS AND/OR RESTS SUPPORTED BY NATURAL TEETH OR  IMPLANTS WITH OR WITHOUT COPING\nCROWNS, NO ATTACHMENTS"
          },
          {
            "full_code": "52900",
            "children": [
              {
                "description": "Dentures, Partial, Overdentures, Acrylic, with Cast/Wrought Clasps and/or Rests with Independent Attachments Secured by Attachments to Natural Teeth with or without Coping Crowns",
                "descriptor": "",
                "fee": "",
                "full_code": "52910"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1105.48",
                "full_code": "52911"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1105.48",
                "full_code": "52912"
              },
              {
                "description": "Dentures, Partial, Overdentures, Acrylic, with Cast/Wrought Clasps and/or Rests, with Independent\nAttachments Secured to Implants with or without Coping Crowns",
                "descriptor": "",
                "fee": "",
                "full_code": "52920"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1105.48",
                "full_code": "52921"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1105.48",
                "full_code": "52922"
              },
              {
                "description": "Dentures, Partial, Overdentures, Acrylic, with Cast/Wrought Clasps and/or Rests, with Independent Attachments Secured to a Combination of Natural Teeth and Implants with or without Coping Crowns [used with 26101, 26103 (Mesostructures); or 28221, 28225, 28226 (Cast Metal Coping\nCrowns) with or without Attachments]",
                "descriptor": "",
                "fee": "",
                "full_code": "52930"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1105.48",
                "full_code": "52931"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1105.48",
                "full_code": "52932"
              },
              {
                "description": "Dentures, Partial, Overdentures, Acrylic, with Cast/Wrought Clasps and/or Rests, with Retention  from a Retentive Bar, Secured to Coping Crowns Supported by Natural Teeth (see 62104 for Retentive\nBar)",
                "descriptor": "",
                "fee": "",
                "full_code": "52940"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1105.48",
                "full_code": "52941"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1105.48",
                "full_code": "52942"
              },
              {
                "description": "Dentures, Partial, Overdentures, Acrylic, with Cast/Wrought Clasps and/or Rests, with Retention from a Retentive Bar, Secured to Coping Crowns Supported by Implants (see 62105 for Retentive Bar)",
                "descriptor": "",
                "fee": "",
                "full_code": "52950"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1105.48",
                "full_code": "52951"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1105.48",
                "full_code": "52952"
              },
              {
                "description": "Dentures, Partial, Overdentures, Acrylic, with Cast/Wrought Clasps and/or Rests, with Retention from a Retentive Bar, Secured to Coping Crowns Supported by a combination of  Natural Teeth and\nImplants (see 62105 for Retentive Bar)",
                "descriptor": "",
                "fee": "",
                "full_code": "52960"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1105.48",
                "full_code": "52961"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1105.48",
                "full_code": "52962"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, PARTIAL, OVERDENTURES, ACRYLIC, WITH CAST/WROUGHT CLASPS AND/OR RESTS\nSECURED BY NATURAL TEETH OR IMPLANTS"
          }
        ],
        "descriptor": "",
        "description": "DENTURES, PARTIAL, ACRYLIC"
      },
      {
        "full_code": "53000",
        "children": [
          {
            "full_code": "53100",
            "children": [
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "930.12",
                "full_code": "53101"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "930.12",
                "full_code": "53102"
              },
              {
                "description": "Altered Cast Impression technique in conjunction with 53101 and 53102",
                "descriptor": "+L",
                "fee": "98.77",
                "full_code": "53104"
              },
              {
                "description": "Dentures, Partial, Free End, Cast Frame/Connector, Clasps and Rests, (Immediate) (includes first\ntissue conditioner, but not a processed reline)",
                "descriptor": "",
                "fee": "",
                "full_code": "53110"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1102.59",
                "full_code": "53111"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1102.6",
                "full_code": "53112"
              },
              {
                "description": "Dentures, Partial Free End, Swing Lock/Connector",
                "descriptor": "",
                "fee": "",
                "full_code": "53120"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1154.98",
                "full_code": "53121"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1154.98",
                "full_code": "53122"
              },
              {
                "description": "Dentures, Partial, Free End, Cast Frame/Connector, Clasps and Rests (Equilibrated)",
                "descriptor": "",
                "fee": "",
                "full_code": "53130"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "2213.7",
                "full_code": "53131"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "2213.7",
                "full_code": "53132"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, PARTIAL, FREE END, CAST FRAME/CONNECTOR, CLASPS AND RESTS",
            "fee": ""
          },
          {
            "full_code": "53200",
            "children": [
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1102.59",
                "full_code": "53201"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1102.59",
                "full_code": "53202"
              },
              {
                "description": "Unilateral, one piece casting, clasps and pontics",
                "descriptor": "+L",
                "fee": "643.16",
                "full_code": "53205"
              },
              {
                "description": "Dentures, Partial, Tooth Borne, Cast Frame/Connector, Clasps and Rests, (Immediate) (includes first\ntissue conditioner, but not a processed reline)",
                "descriptor": "",
                "fee": "",
                "full_code": "53210"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1102.59",
                "full_code": "53211"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1102.59",
                "full_code": "53212"
              },
              {
                "description": "Unilateral, one piece casting, clasps and pontics",
                "descriptor": "+L",
                "fee": "643.15",
                "full_code": "53215"
              },
              {
                "description": "Dentures, Partial, Tooth Borne, Cast Frame/Connector, Clasps and Rests (Equilibrated)",
                "descriptor": "",
                "fee": "",
                "full_code": "53220"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "2213.7",
                "full_code": "53221"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "2213.7",
                "full_code": "53222"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, PARTIAL, TOOTH BORNE, CAST FRAME/CONNECTOR, CLASPS AND RESTS",
            "fee": ""
          },
          {
            "full_code": "53400",
            "children": [
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "53401"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "53402"
              },
              {
                "description": "Altered Cast Impression Technique done in conjunction with the above mentioned codes",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "53404"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, PARTIAL, CAST, PRECISION ATTACHMENTS",
            "fee": ""
          },
          {
            "full_code": "53500",
            "children": [
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "53501"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "53502"
              },
              {
                "description": "Altered Cast Impression Technique done in conjunction with the above mentioned codes",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "53504"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, PARTIAL, CAST, SEMI-PRECISION ATTACHMENTS",
            "fee": ""
          },
          {
            "full_code": "53600",
            "children": [
              {
                "description": "Denture, Cast Partial, Maxillary, Stress Breaker Attachments",
                "descriptor": "",
                "fee": "",
                "full_code": "53610"
              },
              {
                "description": "Maxillary (resilient)",
                "descriptor": "+L",
                "fee": "1102.59",
                "full_code": "53611"
              },
              {
                "description": "Maxillary (one hinge)",
                "descriptor": "+L",
                "fee": "1102.59",
                "full_code": "53612"
              },
              {
                "description": "Maxillary (two hinges)",
                "descriptor": "+L",
                "fee": "1102.59",
                "full_code": "53613"
              },
              {
                "description": "Altered Cast Impression Technique done in conjunction with the above mentioned codes",
                "descriptor": "",
                "fee": "98.77",
                "full_code": "53614"
              },
              {
                "description": "Dentures, Cast Partial, Mandibular, Stress Breaker Attachments",
                "descriptor": "",
                "fee": "",
                "full_code": "53620"
              },
              {
                "description": "Mandibular (resilient)",
                "descriptor": "+L",
                "fee": "1102.59",
                "full_code": "53621"
              },
              {
                "description": "Mandibular (one hinge)",
                "descriptor": "+L",
                "fee": "1102.59",
                "full_code": "53622"
              },
              {
                "description": "Mandibular (two hinges)",
                "descriptor": "+L",
                "fee": "1102.59",
                "full_code": "53623"
              },
              {
                "description": "Altered Cast Impression Technique done in conjunction with the above mentioned codes",
                "descriptor": "",
                "fee": "98.77",
                "full_code": "53624"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, PARTIAL, CAST, STRESS BREAKER ATTACHMENTS"
          },
          {
            "full_code": "53700",
            "children": [
              {
                "description": "Dentures, Partial, Cast, Overdentures, Supported by Natural Teeth with or without Coping Crowns,\nno Attachments",
                "descriptor": "",
                "fee": "",
                "full_code": "53710"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1102.59",
                "full_code": "53711"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1102.59",
                "full_code": "53712"
              },
              {
                "description": "Altered Cast Impression technique done in conjunction with the above mentioned codes",
                "descriptor": "",
                "fee": "98.77",
                "full_code": "53714"
              },
              {
                "description": "Dentures, Partial, Casts, Overdentures, Supported by Implants with or without Coping Crowns, No\nAttachments",
                "descriptor": "",
                "fee": "",
                "full_code": "53720"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1102.6",
                "full_code": "53721"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1102.6",
                "full_code": "53722"
              },
              {
                "description": "Altered Cast Impression technique done in conjunction with the above mentioned codes",
                "descriptor": "",
                "fee": "98.77",
                "full_code": "53724"
              },
              {
                "description": "Dentures, Partial, Casts, Overdentures, Supported by a Combination of Natural Teeth and Implants\nwith or without Coping Crowns, No Attachments",
                "descriptor": "",
                "fee": "",
                "full_code": "53730"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1102.6",
                "full_code": "53731"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1102.6",
                "full_code": "53732"
              },
              {
                "description": "Altered Cast Impression technique done in conjunction with the above mentioned codes",
                "descriptor": "",
                "fee": "98.77",
                "full_code": "53734"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, PARTIAL, CAST, OVERDENTURES, SUPPORTED BY NATURAL TEETH OR IMPLANTS WITH\nOR WITHOUT COPING CROWNS, NO ATTACHMENTS"
          },
          {
            "full_code": "53800",
            "children": [
              {
                "description": "Dentures, Partial, Cast, Overdentures (Immediate), Supported by Natural Teeth with or without Coping Crowns, No Attachments (includes first tissue conditioner, but not a processed reline)",
                "descriptor": "",
                "fee": "",
                "full_code": "53810"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1102.6",
                "full_code": "53811"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1102.6",
                "full_code": "53812"
              },
              {
                "description": "Altered Cast Impression technique done in conjunction with the above mentioned codes",
                "descriptor": "",
                "fee": "98.77",
                "full_code": "53814"
              },
              {
                "description": "Dentures, Partial, Cast, Overdentures (Immediate), Supported by Implants with or without Coping\nCrowns, No Attachments (includes first tissue conditioner, but not a processed reline)",
                "descriptor": "",
                "fee": "",
                "full_code": "53820"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1102.6",
                "full_code": "53821"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1102.6",
                "full_code": "53822"
              },
              {
                "description": "Altered Cast Impression technique done in conjunction with the above mentioned codes",
                "descriptor": "",
                "fee": "98.77",
                "full_code": "53824"
              },
              {
                "description": "Dentures, Partial, Cast, Overdentures (Immediate), Supported by a Combination of Natural Teeth and Implants with or without Coping Crowns, No Attachments (includes first tissue conditioner, but not a\nprocessed reline)",
                "descriptor": "",
                "fee": "",
                "full_code": "53830"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1102.6",
                "full_code": "53831"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1102.6",
                "full_code": "53832"
              },
              {
                "description": "Altered Cast Impression technique done in conjunction with the above mentioned codes",
                "descriptor": "",
                "fee": "98.77",
                "full_code": "53834"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, PARTIAL, CAST, OVERDENTURES (IMMEDIATE), SUPPORTED BY NATURAL TEETH OR\nIMPLANTS WITH OR WITHOUT COPING CROWNS, NO ATTACHMENTS"
          },
          {
            "full_code": "53900",
            "children": [
              {
                "description": "Dentures, Partial, Cast, Overdentures, with Independent Attachments Secured to Natural Teeth, with\nor without Coping Crowns",
                "descriptor": "",
                "fee": "",
                "full_code": "53910"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1194.56",
                "full_code": "53911"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1194.56",
                "full_code": "53912"
              },
              {
                "description": "Altered Cast Impression technique done in conjunction with the above mentioned codes",
                "descriptor": "",
                "fee": "98.77",
                "full_code": "53914"
              },
              {
                "description": "Dentures, Partial, Cast, Overdentures, with Independent Attachments Secured to Implants, with or\nwithout Coping Crowns",
                "descriptor": "",
                "fee": "",
                "full_code": "53920"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1194.56",
                "full_code": "53921"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1194.56",
                "full_code": "53922"
              },
              {
                "description": "Altered Cast Impression technique done in conjunction with the above mentioned codes",
                "descriptor": "",
                "fee": "98.77",
                "full_code": "53924"
              },
              {
                "description": "Dentures, Partial, Cast, Overdentures, with Independent Attachments Secured to a Combination of\nNatural Teeth and Implants, with or without Coping Crowns",
                "descriptor": "",
                "fee": "",
                "full_code": "53930"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1194.56",
                "full_code": "53931"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1194.56",
                "full_code": "53932"
              },
              {
                "description": "Altered Cast Impression technique done in conjunction with the above mentioned codes",
                "descriptor": "",
                "fee": "98.77",
                "full_code": "53934"
              },
              {
                "description": "Dentures, Partial, Cast, Overdentures, with Retention from a Retentive Bar, Secured to Coping\nCrowns Supported by Natural Teeth (see 62104 for Retentive Bar)",
                "descriptor": "",
                "fee": "",
                "full_code": "53940"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1194.56",
                "full_code": "53941"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1194.56",
                "full_code": "53942"
              },
              {
                "description": "Dentures, Partial, Cast, Overdentures, with Retention from a Retentive Bar, Secured to Coping\nCrowns Supported by Implants (see 62105 for Retentive Bar)",
                "descriptor": "",
                "fee": "",
                "full_code": "53950"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1194.56",
                "full_code": "53951"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1194.56",
                "full_code": "53952"
              },
              {
                "description": "Altered Cast Impression technique done in conjunction with the above mentioned codes",
                "descriptor": "",
                "fee": "98.77",
                "full_code": "53954"
              },
              {
                "description": "Dentures, Partial, Cast, Overdentures, with Retention from a Retentive Bar, Secured to Coping Crowns Supported by a Combination of Natural Teeth and Implants (see 62105 for Retentive Bar)",
                "descriptor": "",
                "fee": "",
                "full_code": "53960"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "1194.56",
                "full_code": "53961"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "1194.56",
                "full_code": "53962"
              },
              {
                "description": "Altered Cast Impression technique done in conjunction with the above mentioned codes",
                "descriptor": "",
                "fee": "98.77",
                "full_code": "53964"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, PARTIAL, CAST, OVERDENTURES, SECURED BY ATTACHMENTS TO NATURAL TEETH OR\nIMPLANTS"
          }
        ],
        "descriptor": "",
        "description": "DENTURES, PARTIAL, CAST WITH ACRYLIC BASE"
      },
      {
        "full_code": "54000",
        "children": [
          {
            "full_code": "54200",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "+L",
                "fee": "80.24",
                "full_code": "54201"
              },
              {
                "description": "Two units",
                "descriptor": "+L",
                "fee": "160.48",
                "full_code": "54202"
              },
              {
                "description": "Each additional unit over two",
                "descriptor": "",
                "fee": "80.24",
                "full_code": "54209"
              }
            ],
            "descriptor": "",
            "description": "DENTURE ADJUSTMENTS, PARTIAL OR COMPLETE DENTURE, MINOR",
            "fee": ""
          },
          {
            "full_code": "54300",
            "children": [
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "795.98",
                "full_code": "54301"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "795.98",
                "full_code": "54302"
              }
            ],
            "descriptor": "",
            "description": "DENTURE ADJUSTMENTS, PARTIAL, OR COMPLETE DENTURE, REMOUNT AND OCCLUSAL\nEQUILIBRATION",
            "fee": ""
          },
          {
            "full_code": "54400",
            "children": [
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "795.98",
                "full_code": "54401"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "795.98",
                "full_code": "54402"
              }
            ],
            "descriptor": "",
            "description": "DENTURE ADJUSTMENTS, COMPLETE DENTURE, WITH CAST METAL OCCLUSAL SURFACES, REMOUNT\nAND OCCLUSAL EQUILIBRATION",
            "fee": ""
          },
          {
            "full_code": "54500",
            "children": [
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "795.98",
                "full_code": "54501"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "795.98",
                "full_code": "54502"
              }
            ],
            "descriptor": "",
            "description": "DENTURE, ADJUSTMENTS, PARTIAL DENTURE, WITH CAST METAL OCCLUSAL SURFACES, REMOUNT\nAND OCCLUSAL EQUILIBRATION",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "DENTURES, ADJUSTMENTS (after three months insertion or by other than the dentist providing prosthesis)"
      },
      {
        "full_code": "55000",
        "children": [
          {
            "full_code": "55100",
            "children": [
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "88.66",
                "full_code": "55101"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "88.66",
                "full_code": "55102"
              }
            ],
            "descriptor": "",
            "description": "DENTURE, REPAIRS, COMPLETE DENTURE, NO IMPRESSION REQUIRED",
            "fee": ""
          },
          {
            "full_code": "55200",
            "children": [
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "162.16",
                "full_code": "55201"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "162.16",
                "full_code": "55202"
              }
            ],
            "descriptor": "",
            "description": "DENTURE, REPAIRS, COMPLETE DENTURE, IMPRESSION REQUIRED",
            "fee": ""
          },
          {
            "full_code": "55300",
            "children": [
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "90.95",
                "full_code": "55301"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "90.95",
                "full_code": "55302"
              }
            ],
            "descriptor": "",
            "description": "DENTURE, REPAIRS/ADDITIONS, PARTIAL DENTURE, NO IMPRESSION REQUIRED",
            "fee": ""
          },
          {
            "full_code": "55400",
            "children": [
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "179.62",
                "full_code": "55401"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "179.62",
                "full_code": "55402"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, REPAIRS/ADDITIONS, PARTIAL DENTURE, IMPRESSION REQUIRED",
            "fee": ""
          },
          {
            "full_code": "55500",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "+L",
                "fee": "90.05",
                "full_code": "55501"
              },
              {
                "description": "Each additional unit of time",
                "descriptor": "",
                "fee": "90.05",
                "full_code": "55509"
              }
            ],
            "descriptor": "",
            "description": "DENTURES/IMPLANT RETAINED PROSTHESIS PROPHYLAXIS AND POLISHING",
            "fee": ""
          },
          {
            "full_code": "55600",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "91.87",
                "full_code": "55601"
              },
              {
                "description": "Each addition unit of time",
                "descriptor": "",
                "fee": "91.87",
                "full_code": "55609"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, REBUILDING WORN ACRYLIC DENTURE TEETH (DIRECT CHAIRSIDE) WITH TOOTH\nCOLOURED MATERIALS",
            "fee": ""
          },
          {
            "full_code": "55700",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "98.77",
                "full_code": "55701"
              },
              {
                "description": "Each addition unit of time",
                "descriptor": "",
                "fee": "98.77",
                "full_code": "55709"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, CUSTOM STAINED (PIGMENTED) DENTURE BASES (DIRECT CHAIRSIDE)",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "DENTURES, REPAIRS/ADDITIONS"
      },
      {
        "full_code": "56000",
        "children": [
          {
            "full_code": "56100",
            "children": [
              {
                "description": "Dentures, Replication, Complete Denture, Provisional (No Intra-oral Impression Required)",
                "descriptor": "",
                "fee": "",
                "full_code": "56110"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "188.82",
                "full_code": "56111"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "188.82",
                "full_code": "56112"
              },
              {
                "description": "Dentures, Replication, Partial Denture (Provisional) (No Intra-oral Impression Required)",
                "descriptor": "",
                "fee": "",
                "full_code": "56120"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "188.82",
                "full_code": "56121"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "188.82",
                "full_code": "56122"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, REPLICATION, PROVISIONAL"
          },
          {
            "full_code": "56200",
            "children": [
              {
                "description": "Denture, Reline, Direct Complete Denture",
                "descriptor": "",
                "fee": "",
                "full_code": "56210"
              },
              {
                "description": "Maxillary",
                "descriptor": "",
                "fee": "242.19",
                "full_code": "56211"
              },
              {
                "description": "Mandibular",
                "descriptor": "",
                "fee": "242.19",
                "full_code": "56212"
              },
              {
                "description": "Denture, Reline, Direct, Partial Denture",
                "descriptor": "",
                "fee": "",
                "full_code": "56220"
              },
              {
                "description": "Maxillary",
                "descriptor": "",
                "fee": "262.53",
                "full_code": "56221"
              },
              {
                "description": "Mandibular",
                "descriptor": "",
                "fee": "262.53",
                "full_code": "56222"
              },
              {
                "description": "Denture, Reline, Processed, Complete Denture",
                "descriptor": "",
                "fee": "",
                "full_code": "56230"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "262.53",
                "full_code": "56231"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "262.53",
                "full_code": "56232"
              },
              {
                "description": "Denture, Reline, Processed, Partial Denture",
                "descriptor": "",
                "fee": "",
                "full_code": "56240"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "262.53",
                "full_code": "56241"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "262.53",
                "full_code": "56242"
              },
              {
                "description": "Denture, Reline, Processed, Functional Impression Requiring Three Appointments, Complete Denture",
                "descriptor": "",
                "fee": "",
                "full_code": "56250"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "437.58",
                "full_code": "56251"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "437.58",
                "full_code": "56252"
              },
              {
                "description": "Denture, Reline, Processed, Functional Impression Requiring Three Appointments, Partial Denture",
                "descriptor": "",
                "fee": "",
                "full_code": "56260"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "437.58",
                "full_code": "56261"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "437.58",
                "full_code": "56262"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, RELINING (Does not include Remount - see 54000 series)"
          },
          {
            "full_code": "56300",
            "children": [
              {
                "description": "Denture, Rebase Complete Denture",
                "descriptor": "",
                "fee": "",
                "full_code": "56310"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "262.53",
                "full_code": "56311"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "262.53",
                "full_code": "56312"
              },
              {
                "description": "Denture, Rebase Partial Denture",
                "descriptor": "",
                "fee": "",
                "full_code": "56320"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "262.53",
                "full_code": "56321"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "262.53",
                "full_code": "56322"
              },
              {
                "description": "Denture, Rebase, Complete Denture, Processed, Functional Impression Requiring Three\nAppointments",
                "descriptor": "",
                "fee": "",
                "full_code": "56330"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "437.58",
                "full_code": "56331"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "437.58",
                "full_code": "56332"
              },
              {
                "description": "Denture, Rebase, Partial Denture, Processed, Functional Impression, Requiring Three Appointments",
                "descriptor": "",
                "fee": "",
                "full_code": "56340"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "437.58",
                "full_code": "56341"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "437.58",
                "full_code": "56342"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, REBASING (Where the vestibular tissue-contacting surfaces are modified)"
          },
          {
            "full_code": "56400",
            "children": [
              {
                "description": "Dentures, Remake, Using Existing Framework, Partial Denture (equilibration)",
                "descriptor": "",
                "fee": "",
                "full_code": "56410"
              },
              {
                "description": "Maxillary\n",
                "descriptor": "+L",
                "fee": "350.07 to 569.47",
                "full_code": "56411"
              },
              {
                "description": "Mandibular\n",
                "descriptor": "+L",
                "fee": "350.07 to 569.47",
                "full_code": "56412"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, REMAKE"
          },
          {
            "full_code": "56500",
            "children": [
              {
                "description": "Denture, Therapeutic Tissue Conditioning, per appointment, Complete Denture",
                "descriptor": "",
                "fee": "",
                "full_code": "56510"
              },
              {
                "description": "Maxillary",
                "descriptor": "",
                "fee": "175.02",
                "full_code": "56511"
              },
              {
                "description": "Mandibular",
                "descriptor": "",
                "fee": "175.02",
                "full_code": "56512"
              },
              {
                "description": "Denture, Therapeutic Tissue Conditioning, per appointment, Partial Denture",
                "descriptor": "",
                "fee": "",
                "full_code": "56520"
              },
              {
                "description": "Maxillary",
                "descriptor": "",
                "fee": "175.02",
                "full_code": "56521"
              },
              {
                "description": "Mandibular",
                "descriptor": "",
                "fee": "175.02",
                "full_code": "56522"
              },
              {
                "description": "Dentures, Tissue Conditioning, per appointment, Complete Overdenture, Supported by Natural Teeth",
                "descriptor": "",
                "fee": "",
                "full_code": "56530"
              },
              {
                "description": "Maxillary",
                "descriptor": "",
                "fee": "188.82",
                "full_code": "56531"
              },
              {
                "description": "Mandibular",
                "descriptor": "",
                "fee": "188.82",
                "full_code": "56532"
              },
              {
                "description": "Dentures, Tissue Conditioning, per appointment, Complete Overdenture, Implant Supported",
                "descriptor": "",
                "fee": "",
                "full_code": "56540"
              },
              {
                "description": "Maxillary",
                "descriptor": "",
                "fee": "188.82",
                "full_code": "56541"
              },
              {
                "description": "Mandibular",
                "descriptor": "",
                "fee": "188.82",
                "full_code": "56542"
              },
              {
                "description": "Dentures, Tissue Conditioning, per appointment, Partial Overdenture, Supported by Natural Teeth",
                "descriptor": "",
                "fee": "",
                "full_code": "56550"
              },
              {
                "description": "Maxillary",
                "descriptor": "",
                "fee": "188.82",
                "full_code": "56551"
              },
              {
                "description": "Mandibular",
                "descriptor": "",
                "fee": "188.82",
                "full_code": "56552"
              },
              {
                "description": "Dentures, Tissue Conditioning, per appointment, Partial Overdenture, Implant Supported",
                "descriptor": "",
                "fee": "",
                "full_code": "56560"
              },
              {
                "description": "Maxillary",
                "descriptor": "",
                "fee": "188.82",
                "full_code": "56561"
              },
              {
                "description": "Mandibular",
                "descriptor": "",
                "fee": "188.82",
                "full_code": "56562"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, THERAPEUTIC TISSUE CONDITIONING"
          },
          {
            "full_code": "56600",
            "children": [
              {
                "description": "Resilient Liner, in Relined or Rebased Denture (in addition to reline or rebase of denture)",
                "descriptor": "+L",
                "fee": "LAB",
                "full_code": "56601"
              },
              {
                "description": "Resetting of Teeth (not including reline or rebase of denture)",
                "descriptor": "+L",
                "fee": "367.51",
                "full_code": "56602"
              },
              {
                "description": "Cast occlusal surfaces (includes remount and equilibration)",
                "descriptor": "+L",
                "fee": "773.74",
                "full_code": "56603"
              }
            ],
            "descriptor": "",
            "description": "DENTURES, MISCELLANEOUS SERVICES",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "DENTURES, REPLICATION, RELINING AND REBASING"
      },
      {
        "full_code": "57000",
        "children": [
          {
            "full_code": "57100",
            "children": [
              {
                "description": "Orbital\n",
                "descriptor": "+L",
                "fee": "2715.74 to 6398.11",
                "full_code": "57101"
              },
              {
                "description": "Nose\n",
                "descriptor": "+L",
                "fee": "2125.35 to 4350.25",
                "full_code": "57102"
              },
              {
                "description": "Ear\n",
                "descriptor": "+L",
                "fee": "2125.35 to 4350.25",
                "full_code": "57103"
              },
              {
                "description": "Patch",
                "descriptor": "+L",
                "fee": "638.65",
                "full_code": "57104"
              },
              {
                "description": "Facial, Complex\n",
                "descriptor": "+L",
                "fee": "2715.74 to 5245.75",
                "full_code": "57105"
              },
              {
                "description": "Facial Moulage Impression, Complete",
                "descriptor": "",
                "fee": "417.17",
                "full_code": "57106"
              },
              {
                "description": "Facial Moulage Impression, Sectional",
                "descriptor": "",
                "fee": "312.87",
                "full_code": "57107"
              },
              {
                "description": "Ocular Conformer Prosthesis (temporary post-surgical)",
                "descriptor": "+L",
                "fee": "638.65",
                "full_code": "57108"
              },
              {
                "description": "Ocular Prosthesis\n",
                "descriptor": "+L",
                "fee": "826.52 to 3453.59",
                "full_code": "57109"
              }
            ],
            "descriptor": "",
            "description": "PROSTHESIS, FACIAL",
            "fee": ""
          },
          {
            "full_code": "57200",
            "children": [
              {
                "description": "Obturator, Cleft Palate (prosthesis extra)\n",
                "descriptor": "+L",
                "fee": "118.07 to 511.38",
                "full_code": "57201"
              },
              {
                "description": "Obturator, Palatal (prosthesis extra)\n",
                "descriptor": "+L",
                "fee": "118.07 to 511.38",
                "full_code": "57202"
              },
              {
                "description": "Obturator, Post-Maxillectomy (prosthesis extra)\n",
                "descriptor": "+L",
                "fee": "118.07 to 1278.46",
                "full_code": "57203"
              },
              {
                "description": "Obturator, Temporary Palatal (prosthesis extra)\n",
                "descriptor": "+L",
                "fee": "118.07 to 1278.46",
                "full_code": "57204"
              },
              {
                "description": "Obturator, Resilient (prosthesis extra)\n",
                "descriptor": "+L",
                "fee": "118.07 to 1278.46",
                "full_code": "57205"
              },
              {
                "description": "Obturator, Hollow Bulb (prosthesis extra)\n",
                "descriptor": "+L",
                "fee": "118.07 to 1278.46",
                "full_code": "57206"
              },
              {
                "description": "Obturator, Inflatable (prosthesis extra)\n",
                "descriptor": "+L",
                "fee": "472.29 to 1535.31",
                "full_code": "57207"
              },
              {
                "description": "Obturator Prosthesis, Modification (relines or repairs)\n",
                "descriptor": "+L",
                "fee": "472.29 to 895.5",
                "full_code": "57208"
              },
              {
                "description": "Speech Aid Prosthesis\n",
                "descriptor": "+L",
                "fee": "826.52 to 1662.58",
                "full_code": "57209"
              }
            ],
            "descriptor": "",
            "description": "PROSTHESIS, MAXILLOFACIAL, OBTURATORS",
            "fee": ""
          },
          {
            "full_code": "57300",
            "children": [
              {
                "description": "Velar Bulb (prosthesis and obturator extra)\n",
                "descriptor": "+L",
                "fee": "118.07 to 1278.46",
                "full_code": "57301"
              },
              {
                "description": "Velar Lift Button, Mechanical (prosthesis and obturator extra)\n",
                "descriptor": "+L",
                "fee": "118.07 to 1278.46",
                "full_code": "57302"
              },
              {
                "description": "Retention, Spiral Spring (prosthesis extra)",
                "descriptor": "+L",
                "fee": "767.08",
                "full_code": "57303"
              },
              {
                "description": "Retention, Magnetic (prosthesis extra)",
                "descriptor": "+L",
                "fee": "381.82",
                "full_code": "57304"
              },
              {
                "description": "Guide Plane, Condylar (prosthesis extra)\n",
                "descriptor": "+L",
                "fee": "118.07 to 768.27",
                "full_code": "57305"
              },
              {
                "description": "Implant, Silastic Chin",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "57306"
              },
              {
                "description": "Mesh Prosthesis, Chrome Cobalt Mandibular Mesh",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "57307"
              },
              {
                "description": "Skull Plate, Customized",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "57308"
              },
              {
                "description": "Akerman, Pseudotemporomandibular Joint (prosthesis extra)",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "57309"
              },
              {
                "description": "Feeding Appliance (for infants with cleft palate)\n",
                "descriptor": "+L",
                "fee": "590.36 to 1278.46",
                "full_code": "57311"
              },
              {
                "description": "Lingual Prosthesis\n",
                "descriptor": "+L",
                "fee": "1889.2 to 3838.85",
                "full_code": "57321"
              },
              {
                "description": "Mandibular Resection Prosthesis with Guide Flange\n",
                "descriptor": "+L",
                "fee": "1180.74 to 2047.84",
                "full_code": "57341"
              },
              {
                "description": "Mandibular Resection Prosthesis without Guide Flange\n",
                "descriptor": "+L",
                "fee": "708.45 to 1534.16",
                "full_code": "57342"
              },
              {
                "description": "Prosthesis, Maxillofacial, Fixed",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "57351"
              },
              {
                "description": "Palatal Augmentation Prosthesis\n",
                "descriptor": "+L",
                "fee": "826.52 to 1919.43",
                "full_code": "57361"
              },
              {
                "description": "Palatal Life Prosthesis, Modification (relines or repairs)\n",
                "descriptor": "+L",
                "fee": "236.14 to 895.5",
                "full_code": "57371"
              },
              {
                "description": "Gingival Prosthesis\nNote: For removable appliance used to mask unaesthetic embrasures see sub-classification 49300 soft tissue prosthesis, code 49301 Gingival Mask",
                "descriptor": "+L",
                "fee": "417.17",
                "full_code": "57372"
              }
            ],
            "descriptor": "",
            "description": "PROSTHESIS, MAXILLOFACIAL, OTHER",
            "fee": ""
          },
          {
            "full_code": "57400",
            "children": [
              {
                "description": "Exercisers, Trismus, Therapy\n",
                "descriptor": "+L",
                "fee": "944.59 to 1534.16",
                "full_code": "57401"
              },
              {
                "description": "Splints, Permanent Cast Occlusal\n",
                "descriptor": "+L",
                "fee": "2361.51 to 3838.85",
                "full_code": "57402"
              }
            ],
            "descriptor": "",
            "description": "PROSTHESIS, TEMPOROMANDIBULAR JOINT",
            "fee": ""
          },
          {
            "full_code": "57500",
            "children": [
              {
                "description": "Stout",
                "descriptor": "+L",
                "fee": "1137.14",
                "full_code": "57501"
              },
              {
                "description": "Cast Capped",
                "descriptor": "+L",
                "fee": "1592.71",
                "full_code": "57502"
              },
              {
                "description": "Gunning (upper and lower)",
                "descriptor": "+L",
                "fee": "1592.71",
                "full_code": "57503"
              },
              {
                "description": "Bar Splint, Cast, Labial and Lingual",
                "descriptor": "+L",
                "fee": "1592.71",
                "full_code": "57504"
              },
              {
                "description": "Scaffolding, Rhinoplastic",
                "descriptor": "+L",
                "fee": "1592.71",
                "full_code": "57505"
              },
              {
                "description": "Cast, Adjustable",
                "descriptor": "+L",
                "fee": "1592.71",
                "full_code": "57506"
              },
              {
                "description": "Commissure Splint\n",
                "descriptor": "+L",
                "fee": "354.23 to 1663.73",
                "full_code": "57508"
              }
            ],
            "descriptor": "",
            "description": "PROSTHESIS, SPLINTS",
            "fee": ""
          },
          {
            "full_code": "57600",
            "children": [
              {
                "description": "Ridge Extension",
                "descriptor": "+L",
                "fee": "1137.14",
                "full_code": "57601"
              },
              {
                "description": "Palatal",
                "descriptor": "+L",
                "fee": "1137.14",
                "full_code": "57602"
              },
              {
                "description": "Skin Grafts",
                "descriptor": "+L",
                "fee": "1137.14",
                "full_code": "57603"
              },
              {
                "description": "Mucous Membrane Grafts",
                "descriptor": "+L",
                "fee": "1137.14",
                "full_code": "57604"
              },
              {
                "description": "Prosthesis, Radiation Appliances",
                "descriptor": "",
                "fee": "",
                "full_code": "57650"
              },
              {
                "description": "Radiation Vehicle Carrier\n",
                "descriptor": "+L",
                "fee": "1049.77 to 3413.77",
                "full_code": "57651"
              },
              {
                "description": "Radiation Protection Shield (extra-oral)",
                "descriptor": "+L",
                "fee": "1137.14",
                "full_code": "57652"
              },
              {
                "description": "Radiation Protection Shield (intra-oral)",
                "descriptor": "+L",
                "fee": "1137.14",
                "full_code": "57653"
              },
              {
                "description": "Radiation Cone Locator\n",
                "descriptor": "+L",
                "fee": "354.23 to 2047.84",
                "full_code": "57654"
              },
              {
                "description": "Prosthesis, Stents, Decompression",
                "descriptor": "",
                "fee": "",
                "full_code": "57660"
              },
              {
                "description": "Decompression Stent, Localized",
                "descriptor": "+L",
                "fee": "1137.14",
                "full_code": "57661"
              },
              {
                "description": "Decompression Stent, (prosthesis extra)",
                "descriptor": "+L",
                "fee": "682.75",
                "full_code": "57662"
              }
            ],
            "descriptor": "",
            "description": "PROSTHESIS, STENTS",
            "fee": ""
          },
          {
            "full_code": "57700",
            "children": [
              {
                "description": "Orthopedic Prosthesis (extraoral)\n",
                "descriptor": "+L",
                "fee": "590.36 to 1278.46",
                "full_code": "57701"
              },
              {
                "description": "Orthopedic Prosthesis (intraoral)\n",
                "descriptor": "+L",
                "fee": "708.45 to 1534.16",
                "full_code": "57702"
              }
            ],
            "descriptor": "",
            "description": "PROSTHESIS, ORTHOPEDIC",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "PROSTHESIS, MAXILLOFACIAL"
      }
    ],
    "descriptor": "",
    "description": "PROSTHODONTICS - REMOVABLE Special Aesthetic and anatomical considerations involving additional chair time and/or responsibility may require an increase over the basic fee. Special aesthetic and functional laboratory costs beyond normal laboratory charges will require an increase over the basic fee. EXAMINATION, DIAGNOSIS AND TREATMENT PLAN - Refer to Diagnostic Services, separate fee."
  },
  {
    "full_code": "60000",
    "children": [
      {
        "full_code": "62000",
        "children": [
          {
            "full_code": "62100",
            "children": [
              {
                "description": "Pontics, Cast Metal",
                "descriptor": "+L",
                "fee": "503.45",
                "full_code": "62101"
              },
              {
                "description": "Pontics, Cast Metal Framework with Separate Porcelain/Ceramic/Polymer Glass Jacket Pontic",
                "descriptor": "+L",
                "fee": "503.45",
                "full_code": "62102"
              },
              {
                "description": "Pontics, Prefabricated Attachable Facing",
                "descriptor": "+L",
                "fee": "391.57",
                "full_code": "62103"
              },
              {
                "description": "Pontics, Retentive Bar, Pre-fabricated or Custom (Dolder or Hader) Bar, Attached to Retainer",
                "descriptor": "+L +E",
                "fee": "503.45",
                "full_code": "62104"
              },
              {
                "description": "Pontics, Retentive Bar, Pre-fabricated or Custom (Dolder or Hader) Bar, Attached to Implant-supported\nRetainer to Retain Removable Prosthesis, Each Bar",
                "descriptor": "+L +E",
                "fee": "I.C.",
                "full_code": "62105"
              }
            ],
            "descriptor": "",
            "description": "PONTICS, CAST METAL",
            "fee": ""
          },
          {
            "full_code": "62500",
            "children": [
              {
                "description": "Pontics, Porcelain/Ceramic/Polymer Glass, Fused to  Metal",
                "descriptor": "+L",
                "fee": "504.55",
                "full_code": "62501"
              },
              {
                "description": "Pontics, Porcelain/Ceramic/Polymer Glass, Aluminous",
                "descriptor": "+L",
                "fee": "504.55",
                "full_code": "62502"
              }
            ],
            "descriptor": "",
            "description": "PONTICS, PORCELAIN/CERAMIC/POLYMER GLASS",
            "fee": ""
          },
          {
            "full_code": "62700",
            "children": [
              {
                "description": "Pontics, Acrylic/Composite/Compomer, Processed to Metal",
                "descriptor": "+L",
                "fee": "392.72",
                "full_code": "62701"
              },
              {
                "description": "Pontics, Acrylic/Composite/Compomer, Indirect  (Provisional)",
                "descriptor": "+L",
                "fee": "115.54",
                "full_code": "62702"
              },
              {
                "description": "Pontics, Acrylic/Composite/Compomer, Bonded to adjacent Teeth Direct (Provisional)",
                "descriptor": "+E",
                "fee": "115.54",
                "full_code": "62703"
              },
              {
                "description": "Pontics, Acrylic/Composite/Compomer",
                "descriptor": "+L",
                "fee": "115.54",
                "full_code": "62704"
              }
            ],
            "descriptor": "",
            "description": "PONTICS, ACRYLIC/COMPOSITE /COMPOMER",
            "fee": ""
          },
          {
            "full_code": "62800",
            "children": [
              {
                "description": "Pontics, Natural Tooth Crown, Direct, Bonded to Adjacent Teeth (Provisional)",
                "descriptor": "",
                "fee": "195.24",
                "full_code": "62801"
              }
            ],
            "descriptor": "",
            "description": "PONTICS, NATURAL TOOTH",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "PONTICS, BRIDGE"
      },
      {
        "full_code": "63000",
        "children": [
          {
            "full_code": "63001",
            "children": [
              {
                "description": "Each additional unit of time",
                "descriptor": "",
                "fee": "91.87",
                "full_code": "63009"
              }
            ],
            "descriptor": "",
            "description": "One unit of time",
            "fee": "91.87"
          }
        ],
        "descriptor": "",
        "description": "RECONTOURING OF RETAINER/PONTICS, (of existing bridgework)",
        "fee": ""
      },
      {
        "full_code": "64000",
        "children": [
          {
            "full_code": "64100",
            "children": [
              {
                "description": "Master Cast Techniques, True Hinge Axis Registration and Transfer",
                "descriptor": "",
                "fee": "",
                "full_code": "64120"
              },
              {
                "description": "One unit of time",
                "descriptor": "+L",
                "fee": "87.75",
                "full_code": "64121"
              },
              {
                "description": "Each additional unit of time",
                "descriptor": "+L",
                "fee": "87.75",
                "full_code": "64129"
              },
              {
                "description": "Master Cast Techniques, Centric Registration Recording",
                "descriptor": "",
                "fee": "",
                "full_code": "64130"
              },
              {
                "description": "One unit of time",
                "descriptor": "+L",
                "fee": "87.75",
                "full_code": "64131"
              },
              {
                "description": "Each additional unit of time",
                "descriptor": "+L",
                "fee": "87.75",
                "full_code": "64139"
              },
              {
                "description": "Master Cast Techniques, Three Dimensional Recordings of Mandibular Movement (Pantograph or\nStereograph)",
                "descriptor": "",
                "fee": "",
                "full_code": "64140"
              },
              {
                "description": "One unit of time",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "64141"
              },
              {
                "description": "Each additional unit of time",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "64149"
              }
            ],
            "descriptor": "",
            "description": "MASTER CAST, TECHNIQUES, MAXILLO-MANDIBULAR REGISTRATIONS"
          },
          {
            "full_code": "64200",
            "children": [
              {
                "description": "Master Cast Mounting with Arbitrary Facebow Transfer",
                "descriptor": "",
                "fee": "",
                "full_code": "64220"
              },
              {
                "description": "One unit of time",
                "descriptor": "+L",
                "fee": "87.75",
                "full_code": "64221"
              },
              {
                "description": "Each additional unit of time",
                "descriptor": "+L",
                "fee": "87.75",
                "full_code": "64229"
              },
              {
                "description": "Master Cast Mounting with Kinematic Facebow Transfer",
                "descriptor": "",
                "fee": "",
                "full_code": "64230"
              },
              {
                "description": "One unit of time",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "64231"
              },
              {
                "description": "Each additional unit of time",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "64239"
              }
            ],
            "descriptor": "",
            "description": "MASTER CAST MOUNTING TECHNIQUES"
          },
          {
            "full_code": "64300",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "64301"
              },
              {
                "description": "Each additional unit of time",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "64309"
              }
            ],
            "descriptor": "",
            "description": "MASTER CAST GNATHOLOGICAL WAX-UP",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "MASTER CAST TECHNIQUES"
      },
      {
        "full_code": "66000",
        "children": [
          {
            "full_code": "66100",
            "children": [
              {
                "description": "Replace Broken Prefabricated Attachable Facings",
                "descriptor": "",
                "fee": "",
                "full_code": "66110"
              },
              {
                "description": "One unit of time",
                "descriptor": "+L",
                "fee": "91.87",
                "full_code": "66111"
              },
              {
                "description": "Two units",
                "descriptor": "+L",
                "fee": "183.74",
                "full_code": "66112"
              },
              {
                "description": "Three units",
                "descriptor": "+L",
                "fee": "275.61",
                "full_code": "66113"
              },
              {
                "description": "Four units",
                "descriptor": "+L",
                "fee": "367.48",
                "full_code": "66114"
              },
              {
                "description": "Each additional unit over four",
                "descriptor": "",
                "fee": "91.87",
                "full_code": "66119"
              }
            ],
            "descriptor": "",
            "description": "REPAIRS, REPLACEMENT"
          },
          {
            "full_code": "66200",
            "children": [
              {
                "description": "Repairs, Removal, Fixed Bridge/Prosthesis - To be re-cemented",
                "descriptor": "",
                "fee": "",
                "full_code": "66210"
              },
              {
                "description": "One unit of time",
                "descriptor": "+L",
                "fee": "101.75",
                "full_code": "66211"
              },
              {
                "description": "Two units",
                "descriptor": "+L",
                "fee": "203.5",
                "full_code": "66212"
              },
              {
                "description": "Three units",
                "descriptor": "+L",
                "fee": "305.25",
                "full_code": "66213"
              },
              {
                "description": "Four units",
                "descriptor": "+L",
                "fee": "407",
                "full_code": "66214"
              },
              {
                "description": "Each additional unit over four",
                "descriptor": "+L",
                "fee": "101.75",
                "full_code": "66219"
              },
              {
                "description": "Repairs, Removal Fixed Bridge/Prosthesis- To Be Replaced by a new Prosthesis",
                "descriptor": "",
                "fee": "",
                "full_code": "66220"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "94.17",
                "full_code": "66221"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "188.34",
                "full_code": "66222"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "282.51",
                "full_code": "66223"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "376.68",
                "full_code": "66224"
              },
              {
                "description": "Each additional unit over four",
                "descriptor": "",
                "fee": "94.17",
                "full_code": "66229"
              }
            ],
            "descriptor": "",
            "description": "REPAIRS, REMOVAL OF EXISTING FIXED BRIDGE/PROSTHESIS"
          },
          {
            "full_code": "66300",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "+L",
                "fee": "94.17",
                "full_code": "66301"
              },
              {
                "description": "Two units",
                "descriptor": "+L",
                "fee": "188.34",
                "full_code": "66302"
              },
              {
                "description": "Three units",
                "descriptor": "+L",
                "fee": "282.51",
                "full_code": "66303"
              },
              {
                "description": "Four units",
                "descriptor": "+L",
                "fee": "376.68",
                "full_code": "66304"
              },
              {
                "description": "Each additional unit over four",
                "descriptor": "+L",
                "fee": "94.17",
                "full_code": "66309"
              }
            ],
            "descriptor": "",
            "description": "REPAIRS, REINSERTION/RECEMENTATION (+L where laboratory charges are incurred during repair of bridge)",
            "fee": ""
          },
          {
            "full_code": "66700",
            "children": [
              {
                "description": "Repairs, Fixed Bridge/Prosthesis, Porcelain/Ceramic/Polymer Glass/Acrylic/Composite/Compomer,\nDirect",
                "descriptor": "",
                "fee": "",
                "full_code": "66710"
              },
              {
                "description": "First tooth",
                "descriptor": "",
                "fee": "192.49",
                "full_code": "66711"
              },
              {
                "description": "Each additional tooth",
                "descriptor": "",
                "fee": "192.49",
                "full_code": "66719"
              },
              {
                "description": "Repairs, Solder Indexing to Repair Broken Solder Joint",
                "descriptor": "",
                "fee": "",
                "full_code": "66720"
              },
              {
                "description": "One unit of time",
                "descriptor": "+L",
                "fee": "97.62",
                "full_code": "66721"
              },
              {
                "description": "Each additional unit of time",
                "descriptor": "",
                "fee": "97.62",
                "full_code": "66729"
              },
              {
                "description": "Repair Fractured Porcelain/Metal Pontic With Telescoping Type Crown (pontic prepared, impression\nmade and processed crown seated over metal)",
                "descriptor": "",
                "fee": "",
                "full_code": "66730"
              },
              {
                "description": "First pontic",
                "descriptor": "+L",
                "fee": "514.94",
                "full_code": "66731"
              },
              {
                "description": "Each additional pontic",
                "descriptor": "",
                "fee": "503.45",
                "full_code": "66739"
              }
            ],
            "descriptor": "",
            "description": "REPAIRS, FIXED BRIDGE/PROSTHESIS"
          }
        ],
        "descriptor": "",
        "description": "REPAIRS"
      },
      {
        "full_code": "67000",
        "children": [
          {
            "full_code": "67100",
            "children": [
              {
                "description": "Retainers, Acrylic, Composite/Compomer, Indirect",
                "descriptor": "",
                "fee": "",
                "full_code": "67110"
              },
              {
                "description": "Retainers, Acrylic, Composite/Compomer, Indirect",
                "descriptor": "+L",
                "fee": "751.43",
                "full_code": "67111"
              },
              {
                "description": "Retainers, Acrylic, Composite/Compomer, Complicated, Indirect",
                "descriptor": "+L",
                "fee": "966.45",
                "full_code": "67112"
              },
              {
                "description": "Retainers, Acrylic, Composite/Compomer, Provisional, Indirect (lab fabricated/relined intra-orally)",
                "descriptor": "+L",
                "fee": "321.39",
                "full_code": "67113"
              },
              {
                "description": "Retainers, Acrylic, Composite/Compomer, Implant-supported Indirect",
                "descriptor": "+L",
                "fee": "751.43",
                "full_code": "67115"
              },
              {
                "description": "Retainers, Acrylic, Composite/Compomer, Direct (provisional during healing, done at chair-side)",
                "descriptor": "",
                "fee": "",
                "full_code": "67120"
              },
              {
                "description": "Retainers, Acrylic, Composite/Compomer, Direct (provisional during healing, done at chair-side )",
                "descriptor": "+E",
                "fee": "211.57",
                "full_code": "67121"
              },
              {
                "description": "Retainers, Acrylic, Composite/Compomer, (provisional during healing, done at chair-side), Implant-\nsupported, Direct",
                "descriptor": "+E",
                "fee": "212.72",
                "full_code": "67125"
              },
              {
                "description": "Retainers, Acrylic, Composite/Compomer, Cast Metal Base, Indirect",
                "descriptor": "",
                "fee": "",
                "full_code": "67130"
              },
              {
                "description": "Retainer, Compomer/Composite Resin/Acrylic, Processed to Cast Metal, Indirect",
                "descriptor": "+L",
                "fee": "734.86",
                "full_code": "67131"
              },
              {
                "description": "Retainer, Compomer/Composite Resin/Acrylic, Processed to Metal, Indirect, Implant-supported",
                "descriptor": "+L +E",
                "fee": "783.14",
                "full_code": "67135"
              },
              {
                "description": "Retainers, Acrylic/Composite/Compomer, Two surface Inlay, Indirect Bonded",
                "descriptor": "",
                "fee": "",
                "full_code": "67160"
              },
              {
                "description": "Retainers, Acrylic/Composite/Compomer, Two surface Inlay, Bonded, Indirect",
                "descriptor": "+L",
                "fee": "672.41",
                "full_code": "67161"
              },
              {
                "description": "Retainers, Acrylic/Composite/Compomer, Three surface Inlay, Bonded, Indirect",
                "descriptor": "",
                "fee": "",
                "full_code": "67170"
              },
              {
                "description": "Retainers, Acrylic/Composite/Compomer, Three surface Inlay, Bonded, Indirect",
                "descriptor": "+L",
                "fee": "828.79",
                "full_code": "67171"
              },
              {
                "description": "Retainers, Acrylic/Composite/Compomer, Onlay, Bonded, Indirect",
                "descriptor": "",
                "fee": "",
                "full_code": "67180"
              },
              {
                "description": "Retainers, Acrylic/Composite/Compomer, Onlay, Bonded, Indirect",
                "descriptor": "+L",
                "fee": "986.2",
                "full_code": "67181"
              }
            ],
            "descriptor": "",
            "description": "RETAINERS, ACRYLIC/COMPOSITE/ COMPOMER WITH, OR WITHOUT CAST OR PREFABRICATED\nMETAL BASES"
          },
          {
            "full_code": "67200",
            "children": [
              {
                "description": "Retainer, Porcelain/Ceramic/Polymer Glass",
                "descriptor": "+L",
                "fee": "1135.65",
                "full_code": "67201"
              },
              {
                "description": "Retainer, Porcelain/Ceramic/Polymer Glass, Complicated",
                "descriptor": "+L",
                "fee": "1154.77",
                "full_code": "67202"
              },
              {
                "description": "Retainer, Porcelain/Ceramic/Polymer Glass, Implant-supported",
                "descriptor": "+L +E",
                "fee": "1135.65",
                "full_code": "67205"
              },
              {
                "description": "Retainers, Porcelain/Ceramic/Polymer Glass, Fused To Metal Base",
                "descriptor": "",
                "fee": "",
                "full_code": "67210"
              },
              {
                "description": "Retainers, Porcelain/Ceramic/Polymer Glass, Fused to Metal Base",
                "descriptor": "+L",
                "fee": "1037.49",
                "full_code": "67211"
              },
              {
                "description": "Retainers, Porcelain/Ceramic/Polymer Glass, Fused to Metal Base, Complicated",
                "descriptor": "+L",
                "fee": "1154.77",
                "full_code": "67212"
              },
              {
                "description": "Retainers, Porcelain/Ceramic/Polymer Glass, Fused to Metal Base, Implant-supported",
                "descriptor": "+L +E",
                "fee": "1037.49",
                "full_code": "67215"
              },
              {
                "description": "Retainers, Porcelain/Ceramic/Polymer Glass, Partial Coverage, Bonded (External Retention- e.g.\n\u201cMaryland Bridge\u201d)",
                "descriptor": "",
                "fee": "",
                "full_code": "67220"
              },
              {
                "description": "Retainer, Porcelain/Ceramic/Polymer Glass, Partial Coverage, Bonded (External Retention- e.g.\n\u201cMaryland Bridge\u201d)",
                "descriptor": "+L",
                "fee": "629.88",
                "full_code": "67221"
              },
              {
                "description": "Retainers, Porcelain/Ceramic/Polymer Glass, Two surface Inlay, Bonded",
                "descriptor": "",
                "fee": "",
                "full_code": "67230"
              },
              {
                "description": "Retainers, Porcelain/Ceramic/Polymer Glass, Two surface Inlay, Bonded",
                "descriptor": "+L",
                "fee": "727.2",
                "full_code": "67231"
              },
              {
                "description": "Retainers, Porcelain/Ceramic/Polymer Glass, Three surface Inlay, Bonded",
                "descriptor": "",
                "fee": "",
                "full_code": "67240"
              },
              {
                "description": "Retainers, Porcelain/Ceramic/Polymer Glass, Three surface Inlay, Bonded",
                "descriptor": "+L",
                "fee": "896.17",
                "full_code": "67241"
              },
              {
                "description": "Retainers, Porcelain/Ceramic/Polymer Glass, Onlay, Bonded (where one or more cusps are restored)",
                "descriptor": "",
                "fee": "",
                "full_code": "67250"
              },
              {
                "description": "Retainers, Porcelain/Ceramic/Polymer Glass, Onlay, Bonded",
                "descriptor": "+L",
                "fee": "1063.98",
                "full_code": "67251"
              }
            ],
            "descriptor": "",
            "description": "RETAINER, PORCELAIN/CERAMIC/POLYMER GLASS",
            "fee": ""
          },
          {
            "full_code": "67300",
            "children": [
              {
                "description": "Retainers, Cast Metal",
                "descriptor": "+L",
                "fee": "1082.9",
                "full_code": "67301"
              },
              {
                "description": "Retainers, Cast Metal, Complicated",
                "descriptor": "+L",
                "fee": "1154.77",
                "full_code": "67302"
              },
              {
                "description": "Retainers, Cast Metal, Implant-Supported",
                "descriptor": "+L +E",
                "fee": "1082.9",
                "full_code": "67305"
              },
              {
                "description": "Retainer, \u00be Cast Metal",
                "descriptor": "",
                "fee": "",
                "full_code": "67310"
              },
              {
                "description": "Retainers, \u00be, Cast Metal",
                "descriptor": "+L",
                "fee": "1082.9",
                "full_code": "67311"
              },
              {
                "description": "Retainers, \u00be, Cast Metal, Complicated",
                "descriptor": "+L",
                "fee": "1154.77",
                "full_code": "67312"
              },
              {
                "description": "Retainers, Cast Metal Inlay (used with broken stress technique)",
                "descriptor": "",
                "fee": "",
                "full_code": "67320"
              },
              {
                "description": "Retainer,Cast Metal Inlay, Two Surfaces",
                "descriptor": "+L",
                "fee": "782.76",
                "full_code": "67321"
              },
              {
                "description": "Retainer, Cast Metal Inlay, Three or More Surfaces",
                "descriptor": "+L",
                "fee": "1035.63",
                "full_code": "67322"
              },
              {
                "description": "Retainers, Cast Metal Onlay (internal retention type)",
                "descriptor": "",
                "fee": "",
                "full_code": "67330"
              },
              {
                "description": "Retainers, Cast Metal, Onlay",
                "descriptor": "+L",
                "fee": "1082.9",
                "full_code": "67331"
              },
              {
                "description": "Retainers, Cast Metal, Onlay (bonded external retention/partial coverage - e.g. Maryland Bridge)",
                "descriptor": "",
                "fee": "",
                "full_code": "67340"
              },
              {
                "description": "Retainer, Cast Metal, Onlay, with or without Perforations, Bonded to Abutment Tooth, (Pontic extra)",
                "descriptor": "+L",
                "fee": "524.9",
                "full_code": "67341"
              }
            ],
            "descriptor": "",
            "description": "RETAINERS, CAST METAL",
            "fee": ""
          },
          {
            "full_code": "67400",
            "children": [
              {
                "description": "Retainer, Metal, Pre-fabricated or Custom Cast, Implant-supported, with or without Mesostructure\nwith no Occlusal Component (see 62105 for retentive bar)",
                "descriptor": "+L +E",
                "fee": "I.C.",
                "full_code": "67415"
              }
            ],
            "descriptor": "",
            "description": "RETAINERS, OVERDENTURES, CUSTOM CAST OR PRE-FABRICATED WITH NO OCCLUSAL COMPONENT"
          },
          {
            "full_code": "67500",
            "children": [
              {
                "description": "Retainer Made to an Existing Partial Denture Clasp (additional to retainer, per retainer)",
                "descriptor": "+L",
                "fee": "87.75",
                "full_code": "67501"
              },
              {
                "description": "Telescoping Crown Unit",
                "descriptor": "+L",
                "fee": "391.66",
                "full_code": "67502"
              }
            ],
            "descriptor": "",
            "description": "FIXED PROSTHETICS, ABUTMENTS/RETAINERS, MISCELLANEOUS SERVICES",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "FIXED BRIDGE RETAINERS It is appropriate to use Fixed Bridge Retainer codes, rather than codes for single tooth restorations, where two, or more single tooth inlays/onlays or crowns are joined (Splinted) together and do not support a pontic"
      },
      {
        "full_code": "69000",
        "children": [
          {
            "full_code": "69100",
            "children": [
              {
                "description": "Fixed Prosthesis, Porcelain, to Replace a Substantial Portion of the Alveolar Process (in addition to\nretainer and pontics)",
                "descriptor": "+L",
                "fee": "1119.23",
                "full_code": "69101"
              }
            ],
            "descriptor": "",
            "description": "FIXED PROSTHETICS, MISCELLANEOUS SERVICES",
            "fee": ""
          },
          {
            "full_code": "69200",
            "children": [
              {
                "description": "Splinting, for Extensive or Complicated Restorative Dentistry (per tooth)",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "69201"
              }
            ],
            "descriptor": "",
            "description": "FIXED PROSTHETICS, SPLINTING",
            "fee": ""
          },
          {
            "full_code": "69300",
            "children": [
              {
                "description": "One pin/restoration",
                "descriptor": "+L",
                "fee": "51.27",
                "full_code": "69301"
              },
              {
                "description": "Two pins/restoration",
                "descriptor": "+L",
                "fee": "97.96",
                "full_code": "69302"
              },
              {
                "description": "Three pins/restoration",
                "descriptor": "+L",
                "fee": "155.18",
                "full_code": "69303"
              },
              {
                "description": "Four pins/restoration",
                "descriptor": "+L",
                "fee": "190.07",
                "full_code": "69304"
              },
              {
                "description": "Five pins or more/restoration",
                "descriptor": "+L",
                "fee": "223.82",
                "full_code": "69305"
              }
            ],
            "descriptor": "",
            "description": "FIXED PROSTHETICS, RETENTIVE PINS (for retainers in addition to restoration)",
            "fee": ""
          },
          {
            "full_code": "69600",
            "children": [
              {
                "description": "Provisional, immediate, implant-supported, screw retained, polymer base with denture teeth,\nwithout a reinforcing framework.",
                "descriptor": "",
                "fee": "",
                "full_code": "69610"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "69611"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "69612"
              },
              {
                "description": "Final prosthesis, full arch, denture teeth and acrylic (also known as \"hybrid prosthesis\"), with\nreinforcing framework, implant-supported, screw retained.",
                "descriptor": "",
                "fee": "",
                "full_code": "69620"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "69621"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "69622"
              }
            ],
            "descriptor": "",
            "description": "FIXED PROSTHODONTICS, WHERE AN ENTIRE ARCH IS RECONSTRUCTED (used in extensive or\ncomplicated fixed restorative dentistry)"
          },
          {
            "full_code": "69700",
            "children": [
              {
                "description": "Abutment Tooth",
                "descriptor": "+L",
                "fee": "321.38",
                "full_code": "69701"
              },
              {
                "description": "Pontic",
                "descriptor": "+L",
                "fee": "106.35",
                "full_code": "69702"
              }
            ],
            "descriptor": "",
            "description": "FIXED PROSTHETICS, PROVISIONAL COVERAGE (in extensive or complicated restorative dentistry)",
            "fee": ""
          },
          {
            "full_code": "69800",
            "children": [
              {
                "description": "Fixed Prosthodontic Framework, Osseo-Integrated,  Attached with Screws Or Cement and\nIncorporating Teeth (Porcelain/Ceramic/Polymer Glass Bonded to Metal, Acrylic/Composite/Compomer Processed to Metal or Full Metal Crowns)",
                "descriptor": "",
                "fee": "",
                "full_code": "69820"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "69821"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "69822"
              }
            ],
            "descriptor": "",
            "description": "FIXED PROSTHODONTIC FRAMEWORKS, OSSEO-INTEGRATED IMPLANT-SUPPORTED"
          }
        ],
        "descriptor": "",
        "description": "FIXED PROSTHETICS, OTHER SERVICES"
      }
    ],
    "descriptor": "",
    "description": "PROSTHODONTICS - FIXED Initial description: Fixed prosthodontic therapy requires the use of a variety of technical and therapeutic procedures depending on the nature of the problems presented in each  individual case. The range of these procedures extends into many areas of treatment in order to provide comprehensive therapy for the patient. Many of the procedures used vary considerably in their difficulty, time, involvement and expense. The amount of time involved in a procedure may vary considerably from those outlined in the following guide. The individual components (abutment, retianer and pontic) of a multi-unit fixed prosthesis each constitute seperate units of that restoration and must be coded individually."
  },
  {
    "full_code": "70000",
    "children": [
      {
        "full_code": "71000",
        "children": [
          {
            "full_code": "71100",
            "children": [
              {
                "description": "Single tooth, Uncomplicated",
                "descriptor": "",
                "fee": "154.42",
                "full_code": "71101"
              },
              {
                "description": "Each additional tooth, same quadrant, same appointment",
                "descriptor": "",
                "fee": "154.42",
                "full_code": "71109"
              }
            ],
            "descriptor": "",
            "description": "REMOVALS, ERUPTED TEETH, UNCOMPLICATED",
            "fee": ""
          },
          {
            "full_code": "71200",
            "children": [
              {
                "description": "Odontectomy, (extraction), Erupted Tooth, Surgical Approach, Requiring Surgical Flap and/or\nSectioning of Tooth",
                "descriptor": "",
                "fee": "278.49",
                "full_code": "71201"
              },
              {
                "description": "Each additional tooth, same quadrant",
                "descriptor": "",
                "fee": "278.49",
                "full_code": "71209"
              },
              {
                "description": "Requiring elevation of a Flap, Removal of Bone and may include Sectioning of Tooth for Removal of\nTooth",
                "descriptor": "",
                "fee": "",
                "full_code": "71210"
              },
              {
                "description": "Single Tooth",
                "descriptor": "",
                "fee": "303.64",
                "full_code": "71211"
              },
              {
                "description": "Each Additional tooth, same quadrant",
                "descriptor": "",
                "fee": "303.64",
                "full_code": "71219"
              }
            ],
            "descriptor": "",
            "description": "REMOVALS, ERUPTED TEETH, COMPLICATED",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "REMOVALS, (EXTRACTIONS), ERUPTED TEETH"
      },
      {
        "full_code": "72000",
        "children": [
          {
            "full_code": "72100",
            "children": [
              {
                "description": "Removals, Impaction, Requiring Incision of Overlying Soft Tissue and Removal of The Tooth",
                "descriptor": "",
                "fee": "",
                "full_code": "72110"
              },
              {
                "description": "Single tooth",
                "descriptor": "",
                "fee": "278.49",
                "full_code": "72111"
              },
              {
                "description": "Each additional tooth, same quadrant",
                "descriptor": "",
                "fee": "278.49",
                "full_code": "72119"
              }
            ],
            "descriptor": "",
            "description": "REMOVALS, IMPACTIONS, SOFT TISSUE COVERAGE"
          },
          {
            "full_code": "72200",
            "children": [
              {
                "description": "Removals, Impaction, Requiring Incision of Overlying Soft Tissue, Elevation of A Flap and Either Removal of Bone and Tooth or Sectioning and Removal of Tooth ( Partial Bone Impaction)",
                "descriptor": "",
                "fee": "",
                "full_code": "72210"
              },
              {
                "description": "Single tooth",
                "descriptor": "",
                "fee": "412.97",
                "full_code": "72211"
              },
              {
                "description": "Each additional tooth, same quadrant",
                "descriptor": "",
                "fee": "412.97",
                "full_code": "72219"
              },
              {
                "description": "Removals, Impaction, Requiring Incision of Overlying Soft Tissue, Elevation of A Flap, Removal of\nBone and Sectioning of Tooth For Removal (Complete Bone Impaction)",
                "descriptor": "",
                "fee": "",
                "full_code": "72220"
              },
              {
                "description": "Single tooth",
                "descriptor": "",
                "fee": "550.64",
                "full_code": "72221"
              },
              {
                "description": "Each additional tooth, same quadrant",
                "descriptor": "",
                "fee": "550.64",
                "full_code": "72229"
              },
              {
                "description": "Removals, Impactions, Requiring Incision of Overlaying Soft Tissue, Elevation of A Flap, Removal of\nBone, And/Or Sectioning of the Tooth for Removal And/Or Presemts Unusual Difficulties and Circumstances",
                "descriptor": "",
                "fee": "",
                "full_code": "72230"
              },
              {
                "description": "Single tooth",
                "descriptor": "",
                "fee": "750.74",
                "full_code": "72231"
              },
              {
                "description": "Each additional tooth, same quadrant",
                "descriptor": "",
                "fee": "750.74",
                "full_code": "72239"
              },
              {
                "description": "Coronectomy (Deliberate Vital Root Retention)",
                "descriptor": "",
                "fee": "",
                "full_code": "72240"
              },
              {
                "description": "Coronectomy (Deliberate Vital Root Retention of Unerupted Mandibular Molar)",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "72241"
              },
              {
                "description": "Coronectomy (Deliberate Vital Root Retention to Prevent Complications Associated with Extraction)",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "72242"
              }
            ],
            "descriptor": "",
            "description": "REMOVALS, IMPACTIONS, INVOLVING TISSUE AND/OR BONE COVERAGE"
          },
          {
            "full_code": "72300",
            "children": [
              {
                "description": "Removals, Residual Roots, Erupted",
                "descriptor": "",
                "fee": "",
                "full_code": "72310"
              },
              {
                "description": "First tooth",
                "descriptor": "",
                "fee": "127.47",
                "full_code": "72311"
              },
              {
                "description": "Each additional tooth, same quadrant",
                "descriptor": "",
                "fee": "127.47",
                "full_code": "72319"
              },
              {
                "description": "Removals, Residuals Roots, Soft Tissue Coverage",
                "descriptor": "",
                "fee": "",
                "full_code": "72320"
              },
              {
                "description": "First tooth",
                "descriptor": "",
                "fee": "187.75",
                "full_code": "72321"
              },
              {
                "description": "Each additional tooth, same quadrant",
                "descriptor": "",
                "fee": "187.75",
                "full_code": "72329"
              },
              {
                "description": "Removals, Residual Roots, Bone Tissue Coverage",
                "descriptor": "",
                "fee": "",
                "full_code": "72330"
              },
              {
                "description": "First tooth",
                "descriptor": "",
                "fee": "275.32",
                "full_code": "72331"
              },
              {
                "description": "Each additional tooth, same quadrant",
                "descriptor": "",
                "fee": "275.32",
                "full_code": "72339"
              }
            ],
            "descriptor": "",
            "description": "REMOVALS, (EXTRACTIONS), RESIDUAL ROOTS"
          },
          {
            "full_code": "72400",
            "children": [
              {
                "description": "Alveolar Bone Preservation \u2013 Autograft",
                "descriptor": "",
                "fee": "",
                "full_code": "72410"
              },
              {
                "description": "First tooth",
                "descriptor": "+E",
                "fee": "350.25",
                "full_code": "72411"
              },
              {
                "description": "Each additional tooth",
                "descriptor": "+E",
                "fee": "350.25",
                "full_code": "72419"
              },
              {
                "description": "Alveolar Bone Preservation - Allograft",
                "descriptor": "",
                "fee": "",
                "full_code": "72420"
              },
              {
                "description": "First tooth",
                "descriptor": "+E",
                "fee": "350.25",
                "full_code": "72421"
              },
              {
                "description": "Each additional tooth",
                "descriptor": "+E",
                "fee": "350.25",
                "full_code": "72429"
              },
              {
                "description": "Alveolar Bone Preservation \u2013 Xenograft",
                "descriptor": "",
                "fee": "",
                "full_code": "72430"
              },
              {
                "description": "First tooth",
                "descriptor": "+E",
                "fee": "350.25",
                "full_code": "72431"
              },
              {
                "description": "Each additional tooth",
                "descriptor": "+E",
                "fee": "350.25",
                "full_code": "72439"
              }
            ],
            "descriptor": "",
            "description": "ALVEOLAR BONE PRESERVATION"
          },
          {
            "full_code": "72500",
            "children": [
              {
                "description": "Surgical Exposures, Unerupted, Uncomplicated, Soft Tissue Coverage (includes operculectomy)",
                "descriptor": "",
                "fee": "",
                "full_code": "72510"
              },
              {
                "description": "Single tooth",
                "descriptor": "",
                "fee": "250.33",
                "full_code": "72511"
              },
              {
                "description": "Each additional tooth, same quadrant",
                "descriptor": "",
                "fee": "250.33",
                "full_code": "72519"
              },
              {
                "description": "Surgical Exposures, Complex, Hard Tissue Coverage",
                "descriptor": "",
                "fee": "",
                "full_code": "72520"
              },
              {
                "description": "Single tooth",
                "descriptor": "",
                "fee": "450.43",
                "full_code": "72521"
              },
              {
                "description": "Each additional tooth, same quadrant",
                "descriptor": "",
                "fee": "450.43",
                "full_code": "72529"
              },
              {
                "description": "Surgical Exposures, Unerupted Tooth, With Orthodontic Attachment",
                "descriptor": "",
                "fee": "",
                "full_code": "72530"
              },
              {
                "description": "Single tooth",
                "descriptor": "+E",
                "fee": "600.59",
                "full_code": "72531"
              },
              {
                "description": "Each additional tooth, same quadrant",
                "descriptor": "+E",
                "fee": "600.59",
                "full_code": "72539"
              },
              {
                "description": "Surgical Exposures, Unerupted Tooth, Soft Tissue Coverage With Positioning of Attached Gingivae",
                "descriptor": "",
                "fee": "",
                "full_code": "72540"
              },
              {
                "description": "Single tooth",
                "descriptor": "",
                "fee": "375.5",
                "full_code": "72541"
              },
              {
                "description": "Surgical Exposures, Unerupted Tooth, Hard Tissue Coverage With Positioning of Attached Gingivae",
                "descriptor": "",
                "fee": "",
                "full_code": "72550"
              },
              {
                "description": "Single tooth",
                "descriptor": "",
                "fee": "500.7",
                "full_code": "72551"
              },
              {
                "description": "Rigid Osseous Anchorage For Orthodontics",
                "descriptor": "",
                "fee": "",
                "full_code": "72560"
              },
              {
                "description": "Placement of anchorage device without elevation of a flap",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "72561"
              },
              {
                "description": "Placement of anchorage device with elevation of a flap",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "72562"
              },
              {
                "description": "Removal of anchorage device without elevation of a flap",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "72563"
              },
              {
                "description": "Removal of anchorage device with elevation of a flap",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "72564"
              }
            ],
            "descriptor": "",
            "description": "SURGICAL EXPOSURES OF TEETH"
          },
          {
            "full_code": "72600",
            "children": [
              {
                "description": "Transplantation of Erupted Tooth",
                "descriptor": "",
                "fee": "",
                "full_code": "72610"
              },
              {
                "description": "First tooth",
                "descriptor": "",
                "fee": "750.74",
                "full_code": "72611"
              },
              {
                "description": "Each additional tooth, same quadrant",
                "descriptor": "",
                "fee": "750.74",
                "full_code": "72619"
              },
              {
                "description": "Transplantation of Unerupted Tooth",
                "descriptor": "",
                "fee": "",
                "full_code": "72620"
              },
              {
                "description": "First tooth",
                "descriptor": "",
                "fee": "900.9",
                "full_code": "72621"
              },
              {
                "description": "Each additional tooth, same quadrant",
                "descriptor": "",
                "fee": "900.9",
                "full_code": "72629"
              },
              {
                "description": "Repositioning, Surgical",
                "descriptor": "",
                "fee": "",
                "full_code": "72630"
              },
              {
                "description": "First tooth",
                "descriptor": "",
                "fee": "550.64",
                "full_code": "72631"
              },
              {
                "description": "Each additional tooth, same quadrant",
                "descriptor": "",
                "fee": "550.64",
                "full_code": "72639"
              }
            ],
            "descriptor": "",
            "description": "SURGICAL MOVEMENT OF TEETH"
          },
          {
            "full_code": "72700",
            "children": [
              {
                "description": "Unerupted Tooth Follicle",
                "descriptor": "",
                "fee": "",
                "full_code": "72710"
              },
              {
                "description": "First tooth",
                "descriptor": "",
                "fee": "550.64",
                "full_code": "72711"
              },
              {
                "description": "Each additional tooth, same quadrant",
                "descriptor": "",
                "fee": "550.64",
                "full_code": "72719"
              }
            ],
            "descriptor": "",
            "description": "ENUCLEATION, SURGICAL"
          },
          {
            "full_code": "72800",
            "children": [
              {
                "description": "First tooth",
                "descriptor": "",
                "fee": "92.81",
                "full_code": "72801"
              },
              {
                "description": "Each Additional Tooth",
                "descriptor": "",
                "fee": "92.81",
                "full_code": "72809"
              }
            ],
            "descriptor": "",
            "description": "REMOVAL OF FRACTURED CUSP AS A SEPARATE PROCEDURE, NOT IN CONJUCTION WITH SURGICAL\nOR RESTORATIVE PROCEDURES ON THE SAME TOOTH",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "REMOVALS, (EXTRACTIONS), SURGICAL"
      },
      {
        "full_code": "73000",
        "children": [
          {
            "full_code": "73100",
            "children": [
              {
                "description": "Alveoloplasty, In Conjunction with Extractions",
                "descriptor": "",
                "fee": "",
                "full_code": "73110"
              },
              {
                "description": "Per sextant",
                "descriptor": "",
                "fee": "128.61",
                "full_code": "73111"
              },
              {
                "description": "Alveoloplasty, Not In Conjunction with Extractions",
                "descriptor": "",
                "fee": "",
                "full_code": "73120"
              },
              {
                "description": "Per sextant",
                "descriptor": "",
                "fee": "250.33",
                "full_code": "73121"
              },
              {
                "description": "Remodelling of Bone",
                "descriptor": "",
                "fee": "",
                "full_code": "73140"
              },
              {
                "description": "Mylohyoid Ridge Remodelling",
                "descriptor": "",
                "fee": "487.9",
                "full_code": "73141"
              },
              {
                "description": "Genial Tubercle Remodelling",
                "descriptor": "",
                "fee": "469.18",
                "full_code": "73142"
              },
              {
                "description": "Excision of Bone",
                "descriptor": "",
                "fee": "",
                "full_code": "73150"
              },
              {
                "description": "Nasal Spine, Excision",
                "descriptor": "",
                "fee": "469.18",
                "full_code": "73151"
              },
              {
                "description": "Torus Palatinus, Excision",
                "descriptor": "",
                "fee": "550.64",
                "full_code": "73152"
              },
              {
                "description": "Torus Mandibularis, Unilateral, Excision",
                "descriptor": "",
                "fee": "412.97",
                "full_code": "73153"
              },
              {
                "description": "Torus Mandibularis, Bilateral, Excision",
                "descriptor": "",
                "fee": "688.29",
                "full_code": "73154"
              },
              {
                "description": "Removal of Bone, Exostosis, Multiple",
                "descriptor": "",
                "fee": "",
                "full_code": "73160"
              },
              {
                "description": "Per quadrant\n",
                "descriptor": "",
                "fee": "412.97 to 825.97",
                "full_code": "73161"
              },
              {
                "description": "Reduction of Bone, Tuberosity",
                "descriptor": "",
                "fee": "",
                "full_code": "73170"
              },
              {
                "description": "Unilateral, Reduction",
                "descriptor": "",
                "fee": "250.33",
                "full_code": "73171"
              },
              {
                "description": "Bilateral, Reduction",
                "descriptor": "",
                "fee": "500.7",
                "full_code": "73172"
              },
              {
                "description": "Augmentation of Bone",
                "descriptor": "",
                "fee": "",
                "full_code": "73180"
              },
              {
                "description": "Unilateral, Pterygomaxillary Tuberosity, Augmentation",
                "descriptor": "+E",
                "fee": "487.9",
                "full_code": "73181"
              },
              {
                "description": "Bilateral, Pterygomaxillary Tuberosity, Augmentation",
                "descriptor": "+E",
                "fee": "975.83",
                "full_code": "73182"
              },
              {
                "description": "Unilateral, Mandibular Ridge, Augmentation\n",
                "descriptor": "+E",
                "fee": "600.3 to 800.4",
                "full_code": "73183"
              },
              {
                "description": "Bilateral, Mandibular Ridge, Augmentation\n",
                "descriptor": "+E",
                "fee": "1200.59 to 1600.82",
                "full_code": "73184"
              }
            ],
            "descriptor": "",
            "description": "ALVEOLOPLASTY (Bone remodelling of ridge with soft tissue revisions)"
          },
          {
            "full_code": "73200",
            "children": [
              {
                "description": "Independent Procedure",
                "descriptor": "",
                "fee": "",
                "full_code": "73210"
              },
              {
                "description": "Per sextant",
                "descriptor": "",
                "fee": "275.32",
                "full_code": "73211"
              },
              {
                "description": "Miscellaneous Procedures",
                "descriptor": "",
                "fee": "",
                "full_code": "73220"
              },
              {
                "description": "Gingivoplasty, in Conjunction with Tooth Removal",
                "descriptor": "",
                "fee": "275.32",
                "full_code": "73221"
              },
              {
                "description": "Excision of Vestibular Hyperplasia (per sextant)",
                "descriptor": "",
                "fee": "275.32",
                "full_code": "73222"
              },
              {
                "description": "Surgical Shaving of Papillary Hyperplasia of the Palate",
                "descriptor": "",
                "fee": "487.9",
                "full_code": "73223"
              },
              {
                "description": "Excision of Pericoronal Gingiva (for retained tooth/implant) per tooth/implant",
                "descriptor": "",
                "fee": "137.64",
                "full_code": "73224"
              },
              {
                "description": "Removals, Tissue, Hyperplastic (includes the incision of the mucous membrane, the dissection and removal of hyperplastic tissue, the replacing and adapting of the mucous membrane)",
                "descriptor": "",
                "fee": "",
                "full_code": "73230"
              },
              {
                "description": "Per sextant",
                "descriptor": "",
                "fee": "275.32",
                "full_code": "73231"
              },
              {
                "description": "Removal, Mucosa, Excess (complete removal without dissection)",
                "descriptor": "",
                "fee": "",
                "full_code": "73240"
              },
              {
                "description": "Per sextant",
                "descriptor": "",
                "fee": "275.32",
                "full_code": "73241"
              }
            ],
            "descriptor": "",
            "description": "GINGIVOPLASTY AND/OR STOMATOPLASTY, ORAL SURGERY"
          },
          {
            "full_code": "73300",
            "children": [
              {
                "description": "Full Arch Lowering of the Floor of the Mouth",
                "descriptor": "",
                "fee": "2401.22",
                "full_code": "73301"
              },
              {
                "description": "Partial Arch Lowering of the Floor of the Mouth",
                "descriptor": "",
                "fee": "1200.59",
                "full_code": "73302"
              },
              {
                "description": "Reinsertion of the Mylohyoid Muscle",
                "descriptor": "",
                "fee": "1000.49",
                "full_code": "73303"
              }
            ],
            "descriptor": "",
            "description": "REMODELING, FLOOR OF THE MOUTH",
            "fee": ""
          },
          {
            "full_code": "73400",
            "children": [
              {
                "description": "Vestibuloplasty, Sub-Mucous",
                "descriptor": "",
                "fee": "",
                "full_code": "73410"
              },
              {
                "description": "Per sextant",
                "descriptor": "",
                "fee": "262.75",
                "full_code": "73411"
              },
              {
                "description": "Sulcus Deepening and Ridge Reconstruction",
                "descriptor": "",
                "fee": "",
                "full_code": "73420"
              },
              {
                "description": "Per sextant",
                "descriptor": "",
                "fee": "211.09",
                "full_code": "73421"
              },
              {
                "description": "Vestibuloplasty, with Secondary Epithelization",
                "descriptor": "",
                "fee": "",
                "full_code": "73430"
              },
              {
                "description": "Per sextant",
                "descriptor": "",
                "fee": "325.27",
                "full_code": "73431"
              },
              {
                "description": "Vestibuloplasty, with Labial Inverted Flap",
                "descriptor": "",
                "fee": "",
                "full_code": "73440"
              },
              {
                "description": "Per sextant",
                "descriptor": "",
                "fee": "487.9",
                "full_code": "73441"
              },
              {
                "description": "Vestibuloplasty, with Skin Graft",
                "descriptor": "",
                "fee": "",
                "full_code": "73450"
              },
              {
                "description": "Per sextant",
                "descriptor": "",
                "fee": "600.3",
                "full_code": "73451"
              },
              {
                "description": "Vestibuloplasty, with Mucosal Graft",
                "descriptor": "",
                "fee": "",
                "full_code": "73460"
              },
              {
                "description": "Per sextant",
                "descriptor": "",
                "fee": "600.3",
                "full_code": "73461"
              },
              {
                "description": "Vestibuloplasty \u2013 with Dermal Graft - Autograft",
                "descriptor": "",
                "fee": "",
                "full_code": "73470"
              },
              {
                "description": "Per Sextant",
                "descriptor": "+E",
                "fee": "211.09",
                "full_code": "73471"
              },
              {
                "description": "Vestibuloplasty \u2013 with Dermal Graft - Allograft",
                "descriptor": "",
                "fee": "",
                "full_code": "73480"
              },
              {
                "description": "Per Sextant",
                "descriptor": "",
                "fee": "211.09",
                "full_code": "73481"
              },
              {
                "description": "Vestibuloplasty \u2013 with Connective Tissue for Ridge Augmentation",
                "descriptor": "",
                "fee": "",
                "full_code": "73490"
              },
              {
                "description": "Per sextant",
                "descriptor": "",
                "fee": "211.09",
                "full_code": "73491"
              }
            ],
            "descriptor": "",
            "description": "VESTIBULOPLASTY"
          },
          {
            "full_code": "73500",
            "children": [
              {
                "description": "Reconstruction, Alveolar Ridge, with Autogenous Bone",
                "descriptor": "",
                "fee": "",
                "full_code": "73510"
              },
              {
                "description": "Per sextant",
                "descriptor": "+E",
                "fee": "800.4",
                "full_code": "73511"
              },
              {
                "description": "Reconstruction, Alveolar Ridge, with Alloplastic Material",
                "descriptor": "",
                "fee": "",
                "full_code": "73520"
              },
              {
                "description": "Per sextant",
                "descriptor": "+E",
                "fee": "800.4",
                "full_code": "73521"
              }
            ],
            "descriptor": "",
            "description": "RECONSTRUCTION, ALVEOLAR RIDGE"
          },
          {
            "full_code": "73600",
            "children": [
              {
                "description": "Extensions, Mucous Folds with Secondary Epithelization",
                "descriptor": "",
                "fee": "",
                "full_code": "73610"
              },
              {
                "description": "Per sextant",
                "descriptor": "",
                "fee": "581.55",
                "full_code": "73611"
              },
              {
                "description": "Extensions, Mucous Folds, with Skin Grafts",
                "descriptor": "",
                "fee": "",
                "full_code": "73620"
              },
              {
                "description": "Per sextant",
                "descriptor": "",
                "fee": "581.55",
                "full_code": "73621"
              },
              {
                "description": "Extensions, Mucous Folds, with Mucous Graft",
                "descriptor": "",
                "fee": "",
                "full_code": "73630"
              },
              {
                "description": "Per sextant",
                "descriptor": "",
                "fee": "581.55",
                "full_code": "73631"
              }
            ],
            "descriptor": "",
            "description": "EXTENSIONS, MUCOUS FOLDS"
          }
        ],
        "descriptor": "",
        "description": "REMODELLING AND RECONTOURING ORAL TISSUES IN PREPARATION FOR REMOVABLE PROSTHESES\n(To include codes 73110, 73120, 73140, 73150, 73160, 73170, 73180)"
      },
      {
        "full_code": "74000",
        "children": [
          {
            "full_code": "74100",
            "children": [
              {
                "description": "Tumors, Benign, Scar Tissue, Inflammatory or Congenital Lesions of Soft Tissue of the Oral Cavity",
                "descriptor": "",
                "fee": "",
                "full_code": "74110"
              },
              {
                "description": "1 cm. and under",
                "descriptor": "",
                "fee": "375.37",
                "full_code": "74111"
              },
              {
                "description": "1-2 cm.",
                "descriptor": "",
                "fee": "487.9",
                "full_code": "74112"
              },
              {
                "description": "2-3 cm.",
                "descriptor": "",
                "fee": "591.08",
                "full_code": "74113"
              },
              {
                "description": "3-4 cm.",
                "descriptor": "",
                "fee": "675.52",
                "full_code": "74114"
              },
              {
                "description": "4-6 cm.",
                "descriptor": "",
                "fee": "816.17",
                "full_code": "74115"
              },
              {
                "description": "6-9 cm.",
                "descriptor": "",
                "fee": "906.85",
                "full_code": "74116"
              },
              {
                "description": "9-15 cm.",
                "descriptor": "",
                "fee": "1031.88",
                "full_code": "74117"
              },
              {
                "description": "15 cm. and over",
                "descriptor": "",
                "fee": "1163.13",
                "full_code": "74118"
              },
              {
                "description": "Tumors, Benign, Bone Tissue",
                "descriptor": "",
                "fee": "",
                "full_code": "74120"
              },
              {
                "description": "1 cm. and under",
                "descriptor": "",
                "fee": "450.43",
                "full_code": "74121"
              },
              {
                "description": "1-2 cm.",
                "descriptor": "",
                "fee": "625.58",
                "full_code": "74122"
              },
              {
                "description": "2-3 cm.",
                "descriptor": "",
                "fee": "813.2",
                "full_code": "74123"
              },
              {
                "description": "3-4 cm.",
                "descriptor": "",
                "fee": "1013.3",
                "full_code": "74124"
              },
              {
                "description": "4-6 cm.",
                "descriptor": "",
                "fee": "1182.17",
                "full_code": "74125"
              },
              {
                "description": "6-9 cm.",
                "descriptor": "",
                "fee": "1401.02",
                "full_code": "74126"
              },
              {
                "description": "9-15 cm.",
                "descriptor": "",
                "fee": "1576.13",
                "full_code": "74127"
              },
              {
                "description": "15 cm. and over",
                "descriptor": "",
                "fee": "1813.69",
                "full_code": "74128"
              }
            ],
            "descriptor": "",
            "description": "SURGICAL EXCISIONS, TUMORS, BENIGN"
          },
          {
            "full_code": "74200",
            "children": [
              {
                "description": "Tumors, Malignant, Soft Tissue, Oral Cavity",
                "descriptor": "",
                "fee": "",
                "full_code": "74210"
              },
              {
                "description": "1 cm. and under",
                "descriptor": "",
                "fee": "350.25",
                "full_code": "74211"
              },
              {
                "description": "1-2 cm.",
                "descriptor": "",
                "fee": "525.36",
                "full_code": "74212"
              },
              {
                "description": "2-3 cm.",
                "descriptor": "",
                "fee": "725.46",
                "full_code": "74213"
              },
              {
                "description": "3-4 cm.",
                "descriptor": "",
                "fee": "906.85",
                "full_code": "74214"
              },
              {
                "description": "4-6 cm.",
                "descriptor": "",
                "fee": "1125.66",
                "full_code": "74215"
              },
              {
                "description": "6-9 cm.",
                "descriptor": "",
                "fee": "1313.28",
                "full_code": "74216"
              },
              {
                "description": "9-15 cm.",
                "descriptor": "",
                "fee": "1550.85",
                "full_code": "74217"
              },
              {
                "description": "15 cm. and over",
                "descriptor": "",
                "fee": "1744.71",
                "full_code": "74218"
              },
              {
                "description": "Tumors, Malignant, Bone Tissue",
                "descriptor": "",
                "fee": "",
                "full_code": "74220"
              },
              {
                "description": "1 cm. and under",
                "descriptor": "",
                "fee": "525.36",
                "full_code": "74221"
              },
              {
                "description": "1-2 cm.",
                "descriptor": "",
                "fee": "700.51",
                "full_code": "74222"
              },
              {
                "description": "2-3 cm.",
                "descriptor": "",
                "fee": "906.85",
                "full_code": "74223"
              },
              {
                "description": "3-4 cm.",
                "descriptor": "",
                "fee": "1088.23",
                "full_code": "74224"
              },
              {
                "description": "4-6 cm.",
                "descriptor": "",
                "fee": "1313.28",
                "full_code": "74225"
              },
              {
                "description": "6-9 cm.",
                "descriptor": "",
                "fee": "1500.9",
                "full_code": "74226"
              },
              {
                "description": "9-15 cm.",
                "descriptor": "",
                "fee": "1744.71",
                "full_code": "74227"
              },
              {
                "description": "15 cm. and over",
                "descriptor": "",
                "fee": "2001.02",
                "full_code": "74228"
              },
              {
                "description": "Selective neck dissection",
                "descriptor": "",
                "fee": "",
                "full_code": "74230"
              },
              {
                "description": "Unilateral",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "74231"
              },
              {
                "description": "Bilateral",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "74232"
              },
              {
                "description": "Radical neck dissection",
                "descriptor": "",
                "fee": "",
                "full_code": "74240"
              },
              {
                "description": "Unilateral",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "74241"
              },
              {
                "description": "Bilateral",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "74242"
              }
            ],
            "descriptor": "",
            "description": "SURGICAL EXCISION, TUMORS, MALIGNANT"
          },
          {
            "full_code": "74300",
            "children": [
              {
                "description": "Lips, Throat, Face, Skull",
                "descriptor": "",
                "fee": "",
                "full_code": "74310"
              },
              {
                "description": "Cheiloplasty, Partial (Lip Shave)",
                "descriptor": "",
                "fee": "700.51",
                "full_code": "74311"
              },
              {
                "description": "Cheiloplasty, Total (Lip Shave)\n",
                "descriptor": "",
                "fee": "1050.76 to 1401.02",
                "full_code": "74312"
              }
            ],
            "descriptor": "",
            "description": "SURGICAL EXCISIONS, MAXILLOFACIAL COMPLEX, TRAUMA, TUMORS, BENIGN, MALIGNANT"
          },
          {
            "full_code": "74400",
            "children": [
              {
                "description": "Autograft \u2013 per site \u2013 Maxilla or Mandible",
                "descriptor": "+E",
                "fee": "800.4",
                "full_code": "74401"
              },
              {
                "description": "Allograft \u2013 per site \u2013 Maxilla or Mandible",
                "descriptor": "+E",
                "fee": "800.4",
                "full_code": "74402"
              },
              {
                "description": "Xenograft \u2013 per site \u2013 Maxilla or Mandible",
                "descriptor": "+E",
                "fee": "800.4",
                "full_code": "74403"
              }
            ],
            "descriptor": "",
            "description": "HARD TISSUE GRAFTS TO THE JAW",
            "fee": ""
          },
          {
            "full_code": "74500",
            "children": [
              {
                "description": "Augmentation, Synthetic, of the Jaw",
                "descriptor": "",
                "fee": "",
                "full_code": "74520"
              },
              {
                "description": "Augmentation, of the Chin",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "74521"
              }
            ],
            "descriptor": "",
            "description": "AUGMENTATIONS, PROSTHETIC, OF THE JAW"
          },
          {
            "full_code": "74600",
            "children": [
              {
                "description": "Enucleation of Cyst/Granuloma, Odontogenic and Non-Odontogenic, Requiring Prior Removal of\nBony Tissue and Subsequent Suture(s)",
                "descriptor": "",
                "fee": "",
                "full_code": "74610"
              },
              {
                "description": "1 cm. and under",
                "descriptor": "",
                "fee": "431.72",
                "full_code": "74611"
              },
              {
                "description": "1-2 cm.",
                "descriptor": "",
                "fee": "600.59",
                "full_code": "74612"
              },
              {
                "description": "2-3 cm.",
                "descriptor": "",
                "fee": "781.97",
                "full_code": "74613"
              },
              {
                "description": "3-4 cm.",
                "descriptor": "",
                "fee": "975.83",
                "full_code": "74614"
              },
              {
                "description": "4-6 cm.",
                "descriptor": "",
                "fee": "1182.17",
                "full_code": "74615"
              },
              {
                "description": "6-9 cm.",
                "descriptor": "",
                "fee": "1401.02",
                "full_code": "74616"
              },
              {
                "description": "9-15 cm.",
                "descriptor": "",
                "fee": "1632.34",
                "full_code": "74617"
              },
              {
                "description": "15 cm. and over",
                "descriptor": "",
                "fee": "1876.15",
                "full_code": "74618"
              },
              {
                "description": "Marsupialization",
                "descriptor": "",
                "fee": "",
                "full_code": "74620"
              },
              {
                "description": "Cyst, Marsupialization",
                "descriptor": "",
                "fee": "550.64",
                "full_code": "74621"
              },
              {
                "description": "Excision of Cyst",
                "descriptor": "",
                "fee": "",
                "full_code": "74630"
              },
              {
                "description": "1 cm. and under",
                "descriptor": "",
                "fee": "431.72",
                "full_code": "74631"
              },
              {
                "description": "1-2 cm.",
                "descriptor": "",
                "fee": "600.59",
                "full_code": "74632"
              },
              {
                "description": "2-3 cm.",
                "descriptor": "",
                "fee": "781.97",
                "full_code": "74633"
              },
              {
                "description": "3-4 cm.",
                "descriptor": "",
                "fee": "975.83",
                "full_code": "74634"
              },
              {
                "description": "4-6 cm.",
                "descriptor": "",
                "fee": "1182.17",
                "full_code": "74635"
              },
              {
                "description": "6-9 cm.",
                "descriptor": "",
                "fee": "1401.02",
                "full_code": "74636"
              },
              {
                "description": "9-15 cm.",
                "descriptor": "",
                "fee": "1632.34",
                "full_code": "74637"
              },
              {
                "description": "15 cm. and over",
                "descriptor": "",
                "fee": "1876.15",
                "full_code": "74638"
              }
            ],
            "descriptor": "",
            "description": "SURGICAL EXCISION, CYSTS/GRANULOMAS"
          }
        ],
        "descriptor": "",
        "description": "SURGICAL EXCISIONS (not in conjunction with tooth removal, including biopsy)"
      },
      {
        "full_code": "75000",
        "children": [
          {
            "full_code": "75100",
            "children": [
              {
                "description": "Surgical Incision and Drainage and/or Exploration, Intraoral Soft Tissue",
                "descriptor": "",
                "fee": "",
                "full_code": "75110"
              },
              {
                "description": "Intraoral, Surgical Exploration, Soft Tissue",
                "descriptor": "",
                "fee": "275.32",
                "full_code": "75111"
              },
              {
                "description": "Intraoral, Abscess, Soft Tissue",
                "descriptor": "",
                "fee": "275.32",
                "full_code": "75112"
              },
              {
                "description": "Intraoral, Abscess, In Major Anatomical area with Drain",
                "descriptor": "",
                "fee": "469.18",
                "full_code": "75113"
              },
              {
                "description": "Surgical Incision and Drainage and/or Exploration, Intraoral Hard Tissue",
                "descriptor": "",
                "fee": "",
                "full_code": "75120"
              },
              {
                "description": "Intraoral, Abscess, Hard Tissue, Trephination and Drainage",
                "descriptor": "",
                "fee": "287.8",
                "full_code": "75121"
              },
              {
                "description": "Intraoral, Surgical Exploration, Hard Tissue",
                "descriptor": "",
                "fee": "450.43",
                "full_code": "75122"
              },
              {
                "description": "Intraoral, Abscess, Hard Tissue, Trephination and Drainage in a Major Anatomical Area",
                "descriptor": "",
                "fee": "625.58",
                "full_code": "75123"
              }
            ],
            "descriptor": "",
            "description": "SURGICAL INCISION AND DRAINAGE AND/OR EXPLORATION, INTRAORAL"
          },
          {
            "full_code": "75200",
            "children": [
              {
                "description": "Surgical Incision and Drainage and/or Exploration, Extraoral, Soft Tissue",
                "descriptor": "",
                "fee": "",
                "full_code": "75210"
              },
              {
                "description": "Extraoral, Abscess, Superficial",
                "descriptor": "",
                "fee": "650.53",
                "full_code": "75211"
              },
              {
                "description": "Extraoral, Abscess, Deep",
                "descriptor": "",
                "fee": "813.2",
                "full_code": "75212"
              },
              {
                "description": "Surgical Incision and Drainage and/or Exploration, Extraoral Hard Tissue",
                "descriptor": "",
                "fee": "",
                "full_code": "75220"
              },
              {
                "description": "Extraoral, Surgical Exploration, Hard Tissue",
                "descriptor": "",
                "fee": "650.53",
                "full_code": "75221"
              }
            ],
            "descriptor": "",
            "description": "SURGICAL INCISION AND DRAINAGE AND/OR EXPLORATION, EXTRAORAL"
          },
          {
            "full_code": "75300",
            "children": [
              {
                "description": "Removal, from Skin or Subcutaneous Alveolar Tissue\n",
                "descriptor": "",
                "fee": "875.62 to 1751.27",
                "full_code": "75301"
              },
              {
                "description": "Removal, of Reaction Producing Foreign Bodies\n",
                "descriptor": "",
                "fee": "875.62 to 1751.27",
                "full_code": "75302"
              },
              {
                "description": "Removal, of Needle from Musculo-skeletal System\n",
                "descriptor": "",
                "fee": "875.62 to 1751.27",
                "full_code": "75303"
              }
            ],
            "descriptor": "",
            "description": "SURGICAL INCISION FOR REMOVAL OF FOREIGN BODIES",
            "fee": ""
          },
          {
            "full_code": "75400",
            "children": [
              {
                "description": "Intraoral Sequestrectomy",
                "descriptor": "",
                "fee": "600.59",
                "full_code": "75401"
              },
              {
                "description": "Saucerization",
                "descriptor": "",
                "fee": "1050.76",
                "full_code": "75402"
              },
              {
                "description": "Osteomyelitis, Non Surgical Treatment of",
                "descriptor": "",
                "fee": "225.22",
                "full_code": "75403"
              },
              {
                "description": "Extraoral Sequestrectomy",
                "descriptor": "",
                "fee": "",
                "full_code": "75410"
              },
              {
                "description": "3 cm. and less",
                "descriptor": "",
                "fee": "600.59",
                "full_code": "75411"
              },
              {
                "description": "3-4 cm.",
                "descriptor": "",
                "fee": "750.74",
                "full_code": "75412"
              },
              {
                "description": "4-6 cm.",
                "descriptor": "",
                "fee": "938.36",
                "full_code": "75413"
              },
              {
                "description": "6-9 cm.",
                "descriptor": "",
                "fee": "1094.76",
                "full_code": "75414"
              },
              {
                "description": "9 cm. and over",
                "descriptor": "",
                "fee": "1301.1",
                "full_code": "75415"
              }
            ],
            "descriptor": "",
            "description": "SEQUESTRECTOMY (FOR OSTEOMYELITIS)",
            "fee": ""
          },
          {
            "full_code": "75500",
            "children": [
              {
                "description": "Mandibulectomy",
                "descriptor": "",
                "fee": "",
                "full_code": "75510"
              },
              {
                "description": "3 cm. or less",
                "descriptor": "",
                "fee": "525.36",
                "full_code": "75511"
              },
              {
                "description": "3-4 cm.",
                "descriptor": "",
                "fee": "700.51",
                "full_code": "75512"
              },
              {
                "description": "4-6 cm.",
                "descriptor": "",
                "fee": "906.85",
                "full_code": "75513"
              },
              {
                "description": "6-9 cm.",
                "descriptor": "",
                "fee": "1125.66",
                "full_code": "75514"
              },
              {
                "description": "9-12 cm.",
                "descriptor": "",
                "fee": "1356.99",
                "full_code": "75515"
              },
              {
                "description": "12-15 cm.",
                "descriptor": "",
                "fee": "1600.82",
                "full_code": "75516"
              },
              {
                "description": "15 cm. and over",
                "descriptor": "",
                "fee": "1800.92",
                "full_code": "75517"
              },
              {
                "description": "Total Mandibulectomy\n",
                "descriptor": "",
                "fee": "2201.12 to 2851.46",
                "full_code": "75518"
              }
            ],
            "descriptor": "",
            "description": "MANDIBULECTOMY"
          },
          {
            "full_code": "75600",
            "children": [
              {
                "description": "Maxillectomy",
                "descriptor": "",
                "fee": "",
                "full_code": "75610"
              },
              {
                "description": "3 cm. or less",
                "descriptor": "",
                "fee": "875.62",
                "full_code": "75611"
              },
              {
                "description": "3-4 cm.",
                "descriptor": "",
                "fee": "1050.76",
                "full_code": "75612"
              },
              {
                "description": "4-6 cm.",
                "descriptor": "",
                "fee": "1269.58",
                "full_code": "75613"
              },
              {
                "description": "6-9 cm.",
                "descriptor": "",
                "fee": "1500.9",
                "full_code": "75614"
              },
              {
                "description": "9-12 cm.",
                "descriptor": "",
                "fee": "1744.71",
                "full_code": "75615"
              },
              {
                "description": "12-15 cm.",
                "descriptor": "",
                "fee": "2001.02",
                "full_code": "75616"
              },
              {
                "description": "15 cm. and over",
                "descriptor": "",
                "fee": "2301.17",
                "full_code": "75617"
              },
              {
                "description": "Total Maxillectomy\n",
                "descriptor": "",
                "fee": "2551.31 to 3401.75",
                "full_code": "75618"
              }
            ],
            "descriptor": "",
            "description": "MAXILLECTOMY"
          }
        ],
        "descriptor": "",
        "description": "SURGICAL INCISIONS"
      },
      {
        "full_code": "76000",
        "children": [
          {
            "full_code": "76100",
            "children": [
              {
                "description": "Splints Per Arch, One or More Per Jaw",
                "descriptor": "",
                "fee": "",
                "full_code": "76110"
              },
              {
                "description": "Wiring of Dentures or Arch Bar",
                "descriptor": "",
                "fee": "450.43",
                "full_code": "76111"
              },
              {
                "description": "Acrylic Prosthesis or Cap Splint",
                "descriptor": "",
                "fee": "450.43",
                "full_code": "76112"
              },
              {
                "description": "Circumzygomatic Wiring, Unilateral",
                "descriptor": "",
                "fee": "150.12",
                "full_code": "76113"
              },
              {
                "description": "Perialveolar or Transpalatal Wiring",
                "descriptor": "",
                "fee": "150.12",
                "full_code": "76114"
              },
              {
                "description": "Intra or Periosseous Splinting for Pericranial Suspension",
                "descriptor": "",
                "fee": "150.12",
                "full_code": "76115"
              },
              {
                "description": "Intermaxillary Fixation",
                "descriptor": "",
                "fee": "450.43",
                "full_code": "76116"
              },
              {
                "description": "Intra Maxillary Suspension (Wiring)",
                "descriptor": "",
                "fee": "",
                "full_code": "76120"
              },
              {
                "description": "Nasal Spine Wiring",
                "descriptor": "",
                "fee": "150.12",
                "full_code": "76121"
              },
              {
                "description": "Piriform Apertures Suspension",
                "descriptor": "",
                "fee": "150.12",
                "full_code": "76122"
              },
              {
                "description": "Frontal Suspension",
                "descriptor": "",
                "fee": "650.53",
                "full_code": "76123"
              },
              {
                "description": "Orbital Rim Suspension, Bilateral",
                "descriptor": "",
                "fee": "650.53",
                "full_code": "76124"
              },
              {
                "description": "Head Frame Suspension",
                "descriptor": "",
                "fee": "1050.76",
                "full_code": "76125"
              },
              {
                "description": "Circummandibular Wiring",
                "descriptor": "",
                "fee": "",
                "full_code": "76130"
              },
              {
                "description": "Wiring, one",
                "descriptor": "",
                "fee": "150.12",
                "full_code": "76131"
              },
              {
                "description": "Wiring, two",
                "descriptor": "",
                "fee": "300.28",
                "full_code": "76132"
              },
              {
                "description": "Wiring, three or over",
                "descriptor": "",
                "fee": "450.43",
                "full_code": "76133"
              },
              {
                "description": "Splints/Wires, Removal of",
                "descriptor": "",
                "fee": "",
                "full_code": "76140"
              },
              {
                "description": "Removal of Wire",
                "descriptor": "",
                "fee": "250.33",
                "full_code": "76141"
              },
              {
                "description": "Removal of Arch Splint (one or more per jaw)",
                "descriptor": "",
                "fee": "250.33",
                "full_code": "76142"
              },
              {
                "description": "Removal of Interosseous Ligature or Bone Plate",
                "descriptor": "",
                "fee": "600.59",
                "full_code": "76143"
              },
              {
                "description": "Removal of Intra or Periosseous Rod or Wire for Pericranial Suspension and/or Pericranial Apparatus",
                "descriptor": "",
                "fee": "600.59",
                "full_code": "76144"
              },
              {
                "description": "Removal of Acrylic Prosthesis or Cap Splint, Attached to Maxilla or to Teeth (one or more per jaw)",
                "descriptor": "",
                "fee": "469.18",
                "full_code": "76145"
              },
              {
                "description": "Removal of Wire Plate or Screw used in Osteosynthesis (one or more at the same site)",
                "descriptor": "",
                "fee": "600.59",
                "full_code": "76146"
              }
            ],
            "descriptor": "",
            "description": "INTERMAXILLARY FIXATION (WIRING)"
          },
          {
            "full_code": "76200",
            "children": [
              {
                "description": "Reduction, Mandibular, Closed\n",
                "descriptor": "",
                "fee": "1201.21 to 1501.49",
                "full_code": "76201"
              },
              {
                "description": "Reduction, Mandibular, Open, Single",
                "descriptor": "",
                "fee": "1751.27",
                "full_code": "76202"
              },
              {
                "description": "Reduction, Mandibular, Open, Double",
                "descriptor": "",
                "fee": "2101.52",
                "full_code": "76203"
              },
              {
                "description": "Reduction, Mandibular, Open, Multiple",
                "descriptor": "",
                "fee": "2326.29",
                "full_code": "76204"
              }
            ],
            "descriptor": "",
            "description": "FRACTURES, REDUCTIONS, MANDIBULAR",
            "fee": ""
          },
          {
            "full_code": "76300",
            "children": [
              {
                "description": "Reduction, Maxillary, Closed",
                "descriptor": "",
                "fee": "1201.21",
                "full_code": "76301"
              },
              {
                "description": "Reduction, Maxillary, Open, Single",
                "descriptor": "",
                "fee": "1751.27",
                "full_code": "76302"
              },
              {
                "description": "Reduction, Maxillary, Open, Double",
                "descriptor": "",
                "fee": "2101.52",
                "full_code": "76303"
              },
              {
                "description": "Reduction, Maxillary, Open, Multiple\n",
                "descriptor": "",
                "fee": "2401.22 to 3201.65",
                "full_code": "76304"
              },
              {
                "description": "Reduction, Compound Fracture of Maxilla (requiring reduction and soft tissue repair)\n",
                "descriptor": "",
                "fee": "3401.75 to 4252.18",
                "full_code": "76305"
              }
            ],
            "descriptor": "",
            "description": "FRACTURES, REDUCTIONS, MAXILLARY, HORIZONTAL LE FORT'S l",
            "fee": ""
          },
          {
            "full_code": "76400",
            "children": [
              {
                "description": "Reduction, Maxillary, Closed",
                "descriptor": "",
                "fee": "1401.02",
                "full_code": "76401"
              },
              {
                "description": "Reduction, Maxillary, Open, Unilateral",
                "descriptor": "",
                "fee": "1401.02",
                "full_code": "76402"
              },
              {
                "description": "Reduction, Maxillary, Open, Bilateral",
                "descriptor": "",
                "fee": "2101.52",
                "full_code": "76403"
              }
            ],
            "descriptor": "",
            "description": "FRACTURES, REDUCTIONS, MAXILLARY, PYRAMIDAL LE FORT'S ll",
            "fee": ""
          },
          {
            "full_code": "76500",
            "children": [
              {
                "description": "Reduction, Closed Unilateral",
                "descriptor": "",
                "fee": "1088.23",
                "full_code": "76501"
              },
              {
                "description": "Reduction, Closed Bilateral",
                "descriptor": "",
                "fee": "2176.46",
                "full_code": "76502"
              },
              {
                "description": "Reduction, Naso-orbital, Open, External Approach",
                "descriptor": "",
                "fee": "1938.57",
                "full_code": "76503"
              },
              {
                "description": "Reduction, Naso-orbital, Open, Sinusal Approach",
                "descriptor": "",
                "fee": "1938.57",
                "full_code": "76504"
              },
              {
                "description": "Reduction, Naso-orbital, Open, Orbital Approach with Insertion of Subperiosteal Implant",
                "descriptor": "",
                "fee": "2132.43",
                "full_code": "76505"
              },
              {
                "description": "Exploration, of Orbital Blowout Fracture",
                "descriptor": "",
                "fee": "1401.02",
                "full_code": "76506"
              },
              {
                "description": "Exploration, of Orbital Blowout Fracture and Reconstruction with Insertion of a Subperiosteal Implant",
                "descriptor": "",
                "fee": "2326.29",
                "full_code": "76507"
              }
            ],
            "descriptor": "",
            "description": "FRACTURES, REDUCTIONS, NASO-ORBITAL",
            "fee": ""
          },
          {
            "full_code": "76600",
            "children": [
              {
                "description": "Reduction, Malar Bone, Closed",
                "descriptor": "",
                "fee": "600.59",
                "full_code": "76601"
              },
              {
                "description": "Reduction, Malar Bone, Open, by Simple Elevation",
                "descriptor": "",
                "fee": "900.9",
                "full_code": "76602"
              },
              {
                "description": "Reduction, Malar Bone, Open, by Osteosynthesis",
                "descriptor": "",
                "fee": "1600.82",
                "full_code": "76603"
              },
              {
                "description": "Reduction, Malar Bone, Open, by Sinus Approach",
                "descriptor": "",
                "fee": "1313.28",
                "full_code": "76604"
              },
              {
                "description": "Reduction, Malar Bone, Simple Fracture, (open reduction with antrostomy and packing)",
                "descriptor": "",
                "fee": "1313.28",
                "full_code": "76605"
              }
            ],
            "descriptor": "",
            "description": "FRACTURES, REDUCTIONS, MALAR BONE",
            "fee": ""
          },
          {
            "full_code": "76700",
            "children": [
              {
                "description": "Reduction, Zygomatic Arch, Intraoral Approach",
                "descriptor": "",
                "fee": "600.59",
                "full_code": "76701"
              },
              {
                "description": "Reduction, Zygomatic Arch, Temporal Approach",
                "descriptor": "",
                "fee": "1401.02",
                "full_code": "76702"
              },
              {
                "description": "Reduction, Zygomatico-Maxillary Fracture Dislocation, Complex, Closed Reduction",
                "descriptor": "",
                "fee": "900.9",
                "full_code": "76703"
              },
              {
                "description": "Reduction, Zygomatico-Maxillary Fracture Dislocation, Open Reduction",
                "descriptor": "",
                "fee": "1751.27",
                "full_code": "76704"
              }
            ],
            "descriptor": "",
            "description": "FRACTURES, REDUCTIONS, ZYGOMATIC ARCH",
            "fee": ""
          },
          {
            "full_code": "76800",
            "children": [
              {
                "description": "Reduction, Craniofacial Dysjunction, Closed",
                "descriptor": "",
                "fee": "2401.22",
                "full_code": "76801"
              },
              {
                "description": "Reduction, Craniofacial Dysjunction, Open",
                "descriptor": "",
                "fee": "3401.75",
                "full_code": "76802"
              }
            ],
            "descriptor": "",
            "description": "FRACTURES, REDUCTIONS, CRANIOFACIAL OTHER (specify type of procedure according to previous\ncode used for fracture)",
            "fee": ""
          },
          {
            "full_code": "76900",
            "children": [
              {
                "description": "Fracture, Alveolar, Debridement, Teeth Removed",
                "descriptor": "",
                "fee": "",
                "full_code": "76910"
              },
              {
                "description": "3 cm. or less\n",
                "descriptor": "",
                "fee": "750.74 to 1501.49",
                "full_code": "76911"
              },
              {
                "description": "3-6 cm.\n",
                "descriptor": "",
                "fee": "750.74 to 1501.49",
                "full_code": "76912"
              },
              {
                "description": "6 cm. and over\n",
                "descriptor": "",
                "fee": "781.97 to 1563.94",
                "full_code": "76913"
              },
              {
                "description": "Reduction, Alveolar, Closed, with Teeth",
                "descriptor": "",
                "fee": "",
                "full_code": "76920"
              },
              {
                "description": "3 cm. and less\n",
                "descriptor": "+E",
                "fee": "750.74 to 1501.49",
                "full_code": "76921"
              },
              {
                "description": "3-6 cm.\n",
                "descriptor": "+E",
                "fee": "750.74 to 1501.49",
                "full_code": "76922"
              },
              {
                "description": "6-9 cm.\n",
                "descriptor": "+E",
                "fee": "781.97 to 1563.94",
                "full_code": "76923"
              },
              {
                "description": "9 cm. and over\n",
                "descriptor": "+E",
                "fee": "781.97 to 1563.94",
                "full_code": "76924"
              },
              {
                "description": "Reduction, Alveolar, Open, with Teeth",
                "descriptor": "",
                "fee": "",
                "full_code": "76930"
              },
              {
                "description": "3 cm. and less\n",
                "descriptor": "+E",
                "fee": "750.74 to 1501.49",
                "full_code": "76931"
              },
              {
                "description": "3-6 cm.\n",
                "descriptor": "+E",
                "fee": "750.74 to 1501.49",
                "full_code": "76932"
              },
              {
                "description": "6-9 cm.\n",
                "descriptor": "+E",
                "fee": "781.97 to 1563.94",
                "full_code": "76933"
              },
              {
                "description": "9 cm. and over\n",
                "descriptor": "+E",
                "fee": "813.2 to 1626.39",
                "full_code": "76934"
              },
              {
                "description": "Replantation, Avulsed Tooth/Teeth (including splinting)",
                "descriptor": "",
                "fee": "",
                "full_code": "76940"
              },
              {
                "description": "Replantation, first tooth",
                "descriptor": "",
                "fee": "469.18",
                "full_code": "76941"
              },
              {
                "description": "Each additional tooth",
                "descriptor": "",
                "fee": "469.18",
                "full_code": "76949"
              },
              {
                "description": "Repositioning of Traumatically Displaced Teeth",
                "descriptor": "",
                "fee": "",
                "full_code": "76950"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "143.88",
                "full_code": "76951"
              },
              {
                "description": "Two units of time",
                "descriptor": "",
                "fee": "287.76",
                "full_code": "76952"
              },
              {
                "description": "Each additional unit over two",
                "descriptor": "",
                "fee": "143.88",
                "full_code": "76959"
              },
              {
                "description": "Repairs, Lacerations, Uncomplicated, Intraoral or Extraoral",
                "descriptor": "",
                "fee": "",
                "full_code": "76960"
              },
              {
                "description": "2 cm. or less",
                "descriptor": "",
                "fee": "300.28",
                "full_code": "76961"
              },
              {
                "description": "2-4 cm.",
                "descriptor": "",
                "fee": "337.84",
                "full_code": "76962"
              },
              {
                "description": "4-6 cm.",
                "descriptor": "",
                "fee": "375.37",
                "full_code": "76963"
              },
              {
                "description": "6-9 cm.",
                "descriptor": "",
                "fee": "412.9",
                "full_code": "76964"
              },
              {
                "description": "9-12 cm.",
                "descriptor": "",
                "fee": "469.18",
                "full_code": "76965"
              },
              {
                "description": "12-16 cm.",
                "descriptor": "",
                "fee": "508.26",
                "full_code": "76966"
              },
              {
                "description": "16-20 cm.",
                "descriptor": "",
                "fee": "547.38",
                "full_code": "76967"
              },
              {
                "description": "20-25 cm.",
                "descriptor": "",
                "fee": "609.9",
                "full_code": "76968"
              },
              {
                "description": "25 cm. and over",
                "descriptor": "",
                "fee": "650.53",
                "full_code": "76969"
              },
              {
                "description": "Repairs, Lacerations, Through and Through",
                "descriptor": "",
                "fee": "",
                "full_code": "76970"
              },
              {
                "description": "2 cm. or less",
                "descriptor": "",
                "fee": "325.27",
                "full_code": "76971"
              },
              {
                "description": "2-4 cm.",
                "descriptor": "",
                "fee": "365.93",
                "full_code": "76972"
              },
              {
                "description": "4-6 cm.",
                "descriptor": "",
                "fee": "406.6",
                "full_code": "76973"
              },
              {
                "description": "6-9 cm.",
                "descriptor": "",
                "fee": "447.23",
                "full_code": "76974"
              },
              {
                "description": "9-12 cm.",
                "descriptor": "",
                "fee": "506.65",
                "full_code": "76975"
              },
              {
                "description": "12-16 cm.",
                "descriptor": "",
                "fee": "548.87",
                "full_code": "76976"
              },
              {
                "description": "16-20 cm.",
                "descriptor": "",
                "fee": "591.08",
                "full_code": "76977"
              },
              {
                "description": "20-25 cm.",
                "descriptor": "",
                "fee": "656.71",
                "full_code": "76978"
              },
              {
                "description": "25 cm. and over",
                "descriptor": "",
                "fee": "700.51",
                "full_code": "76979"
              },
              {
                "description": "Repairs, Lacerations, Complicated (local tissue shifts)",
                "descriptor": "",
                "fee": "",
                "full_code": "76980"
              },
              {
                "description": "2 cm. or less",
                "descriptor": "",
                "fee": "350.25",
                "full_code": "76981"
              },
              {
                "description": "2-4 cm.",
                "descriptor": "",
                "fee": "394.02",
                "full_code": "76982"
              },
              {
                "description": "4-6 cm.",
                "descriptor": "",
                "fee": "437.79",
                "full_code": "76983"
              },
              {
                "description": "6-9 cm.",
                "descriptor": "",
                "fee": "481.6",
                "full_code": "76984"
              },
              {
                "description": "9-12 cm.",
                "descriptor": "",
                "fee": "544.11",
                "full_code": "76985"
              },
              {
                "description": "12-16 cm.",
                "descriptor": "",
                "fee": "589.44",
                "full_code": "76986"
              },
              {
                "description": "16-20 cm.",
                "descriptor": "",
                "fee": "634.79",
                "full_code": "76987"
              },
              {
                "description": "20-25 cm.",
                "descriptor": "",
                "fee": "703.55",
                "full_code": "76988"
              },
              {
                "description": "25 cm. and over",
                "descriptor": "",
                "fee": "750.45",
                "full_code": "76989"
              }
            ],
            "descriptor": "",
            "description": "FRACTURES, REDUCTIONS, ALVEOLAR"
          }
        ],
        "descriptor": "",
        "description": "FRACTURES, TREATMENT OF"
      },
      {
        "full_code": "77000",
        "children": [
          {
            "full_code": "77100",
            "children": [
              {
                "description": "Osteotomy, Subcondylar, Closed",
                "descriptor": "",
                "fee": "5352.76",
                "full_code": "77101"
              },
              {
                "description": "Osteotomy, Subcondylar, Open",
                "descriptor": "",
                "fee": "5352.76",
                "full_code": "77102"
              },
              {
                "description": "Osteotomy, Ramus of the Mandible, Oblique, Extraoral",
                "descriptor": "",
                "fee": "5352.76",
                "full_code": "77103"
              },
              {
                "description": "Osteotomy, Ramus of the Mandible, Oblique, Intraoral",
                "descriptor": "",
                "fee": "5352.76",
                "full_code": "77104"
              },
              {
                "description": "Osteotomy/Ostectomy, Body of the Mandible",
                "descriptor": "",
                "fee": "5352.76",
                "full_code": "77105"
              },
              {
                "description": "Osteotomy, Coronoidectomy",
                "descriptor": "",
                "fee": "2551.31",
                "full_code": "77106"
              },
              {
                "description": "Osteotomy, Condylar Neck",
                "descriptor": "",
                "fee": "2551.31",
                "full_code": "77107"
              },
              {
                "description": "Osteotomy, Sagittal Split",
                "descriptor": "",
                "fee": "5352.76",
                "full_code": "77108"
              }
            ],
            "descriptor": "",
            "description": "OSTEOTOMY/OSTECTOMY, RAMUS OF THE MANDIBLE",
            "fee": ""
          },
          {
            "full_code": "77200",
            "children": [
              {
                "description": "Osteotomy, Oblique with Bone Graft",
                "descriptor": "",
                "fee": "5002.57",
                "full_code": "77201"
              },
              {
                "description": "Osteotomy, Inverted \"L\"",
                "descriptor": "",
                "fee": "5002.57",
                "full_code": "77202"
              },
              {
                "description": "Osteotomy, \"C\"",
                "descriptor": "",
                "fee": "5002.57",
                "full_code": "77203"
              },
              {
                "description": "Osteotomy of the Ramus of the Mandible for Distraction Osteogenesis \u2013 Unilateral",
                "descriptor": "",
                "fee": "5002.57",
                "full_code": "77204"
              },
              {
                "description": "Osteotomy of the Ramus of the Mandible for Distraction Osteogenesis \u2013 Bilateral",
                "descriptor": "",
                "fee": "5002.57",
                "full_code": "77205"
              },
              {
                "description": "Activation of Distraction Device - Unilateral",
                "descriptor": "",
                "fee": "5002.57",
                "full_code": "77206"
              },
              {
                "description": "Activation of Distraction Device - Bilateral",
                "descriptor": "",
                "fee": "5002.57",
                "full_code": "77207"
              },
              {
                "description": "Removal of Distraction Device - Unilateral",
                "descriptor": "",
                "fee": "5002.57",
                "full_code": "77208"
              },
              {
                "description": "Removal of Distraction Device - Bilateral",
                "descriptor": "",
                "fee": "5002.57",
                "full_code": "77209"
              }
            ],
            "descriptor": "",
            "description": "OSTEOTOMY, MISCELLANEOUS",
            "fee": ""
          },
          {
            "full_code": "77300",
            "children": [
              {
                "description": "Osteotomy, Maxillary, Le Fort l",
                "descriptor": "",
                "fee": "5352.76",
                "full_code": "77301"
              },
              {
                "description": "Osteotomy, Maxillary, Le Fort ll",
                "descriptor": "",
                "fee": "5652.91",
                "full_code": "77302"
              },
              {
                "description": "Osteotomy, Maxillary, Le Fort lll",
                "descriptor": "",
                "fee": "6753.48",
                "full_code": "77303"
              },
              {
                "description": "Additional to the Above Osteotomy Requiring Two Segments",
                "descriptor": "",
                "fee": "700.35",
                "full_code": "77304"
              },
              {
                "description": "Additional to the Above Osteotomy Requiring Three Segments",
                "descriptor": "",
                "fee": "900.44",
                "full_code": "77305"
              },
              {
                "description": "Additional to the Above Osteotomy Requiring Four Segments",
                "descriptor": "",
                "fee": "1150.58",
                "full_code": "77306"
              },
              {
                "description": "Additional to the Above Osteotomy Requiring a Cranial Flap",
                "descriptor": "",
                "fee": "900.44",
                "full_code": "77307"
              },
              {
                "description": "Closure of Cleft Fistula (Alveolar)",
                "descriptor": "",
                "fee": "850.44",
                "full_code": "77308"
              },
              {
                "description": "Closure of Cleft Fistula (Palatal)",
                "descriptor": "",
                "fee": "850.44",
                "full_code": "77309"
              },
              {
                "description": "Pharyngoplasty",
                "descriptor": "",
                "fee": "1350.68",
                "full_code": "77311"
              },
              {
                "description": "Submuccous Resection",
                "descriptor": "",
                "fee": "850.44",
                "full_code": "77312"
              },
              {
                "description": "Osteotomy, Maxillary, Le Fort I \u2013 for Distraction Osteogenesis",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "77313"
              },
              {
                "description": "Osteotomy, Maxillary, Le Fort II \u2013 for Distraction Osteogenesis",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "77314"
              },
              {
                "description": "Osteogenesis, Maxillary, Le Fort III \u2013 for Distraction Osteogenesis",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "77315"
              },
              {
                "description": "Activation of Distraction Device \u2013 Le Fort I Level",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "77316"
              },
              {
                "description": "Activation of Distraction Device \u2013 Le Fort II Level",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "77317"
              },
              {
                "description": "Activation of Distraction Device \u2013 Le Fort III Level",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "77318"
              },
              {
                "description": "Removal of Maxillary Distraction Device",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "77319"
              }
            ],
            "descriptor": "",
            "description": "OSTEOTOMY, MAXILLARY",
            "fee": ""
          },
          {
            "full_code": "77400",
            "children": [
              {
                "description": "Osteotomy, Segmental, Maxillary",
                "descriptor": "",
                "fee": "",
                "full_code": "77410"
              },
              {
                "description": "Osteotomy, Segmental, Anterior",
                "descriptor": "",
                "fee": "2401.22",
                "full_code": "77411"
              },
              {
                "description": "Osteotomy, Segmental, Posterior",
                "descriptor": "",
                "fee": "2401.22",
                "full_code": "77412"
              },
              {
                "description": "Osteotomy, Mid-palatal Split, Anterior",
                "descriptor": "",
                "fee": "1600.82",
                "full_code": "77413"
              },
              {
                "description": "Osteotomy, Mid-palatal Split, Complete",
                "descriptor": "",
                "fee": "2401.22",
                "full_code": "77414"
              },
              {
                "description": "Osteotomy, Segmental, Anterior \u2013 for Distraction Osteogenesis",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "77415"
              },
              {
                "description": "Osteotomy, Segmental, Posterior \u2013 for Distraction Osteogenesis",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "77416"
              },
              {
                "description": "Activation of Distraction Device",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "77417"
              },
              {
                "description": "Removal of Segmentation Maxillary Distraction Device",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "77418"
              },
              {
                "description": "Osteotomy, Segmental, Mandible",
                "descriptor": "",
                "fee": "",
                "full_code": "77420"
              },
              {
                "description": "Osteotomy, Segmental, Anterior with Transfer of Mental Eminence",
                "descriptor": "",
                "fee": "2401.22",
                "full_code": "77421"
              },
              {
                "description": "Osteotomy, Segmental, Anterior, without the Transfer of Mental Eminence",
                "descriptor": "",
                "fee": "2401.22",
                "full_code": "77422"
              },
              {
                "description": "Osteotomy, Segmental, Posterior",
                "descriptor": "",
                "fee": "2176.46",
                "full_code": "77423"
              },
              {
                "description": "Osteotomy, Lower Border, Mandible",
                "descriptor": "",
                "fee": "2401.22",
                "full_code": "77424"
              },
              {
                "description": "Osteotomy, Total Dento-Alveolar, Mandible",
                "descriptor": "",
                "fee": "5002.57",
                "full_code": "77425"
              },
              {
                "description": "Osteotomy, Segmental, Anterior \u2013 for Distraction Osteogenesis",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "77426"
              },
              {
                "description": "Osteotomy, Segmental, Posterior \u2013 for Distraction Osteogenesis",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "77427"
              },
              {
                "description": "Activation of Distraction Device",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "77428"
              },
              {
                "description": "Removal of Segmental Mandibular Distraction Device",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "77429"
              },
              {
                "description": "Osteotomy When \"Interpositional Graft\" Is Required",
                "descriptor": "",
                "fee": "",
                "full_code": "77430"
              },
              {
                "description": "Using Bone",
                "descriptor": "",
                "fee": "600.3",
                "full_code": "77431"
              },
              {
                "description": "Using Alloplast",
                "descriptor": "+E",
                "fee": "562.83",
                "full_code": "77432"
              },
              {
                "description": "Using Cartilage",
                "descriptor": "",
                "fee": "600.3",
                "full_code": "77433"
              },
              {
                "description": "Osteotomy When \"Onlay Graft\" Is Required For Osteotomy, Trauma or Reconstructive Procedures",
                "descriptor": "",
                "fee": "",
                "full_code": "77440"
              },
              {
                "description": "Using Bone",
                "descriptor": "",
                "fee": "400.2",
                "full_code": "77441"
              },
              {
                "description": "Using Alloplast",
                "descriptor": "+E",
                "fee": "375.21",
                "full_code": "77442"
              },
              {
                "description": "Using Cartilage",
                "descriptor": "",
                "fee": "400.2",
                "full_code": "77443"
              }
            ],
            "descriptor": "",
            "description": "OSTEOTOMY, MAXILLARY/MANDIBULAR, SEGMENTAL"
          },
          {
            "full_code": "77500",
            "children": [
              {
                "description": "Genioplasty, Sliding, Reduction or Augmentation",
                "descriptor": "",
                "fee": "2401.22",
                "full_code": "77501"
              },
              {
                "description": "Genioplasty, Reduction (vertical)",
                "descriptor": "",
                "fee": "2401.22",
                "full_code": "77502"
              },
              {
                "description": "Genioplasty, Augmentation with Graft (see grafting codes)",
                "descriptor": "",
                "fee": "2401.22",
                "full_code": "77503"
              },
              {
                "description": "Myotomy, Suprahyoid",
                "descriptor": "",
                "fee": "600.59",
                "full_code": "77504"
              }
            ],
            "descriptor": "",
            "description": "GENIOPLASTY",
            "fee": ""
          },
          {
            "full_code": "77600",
            "children": [
              {
                "description": "Corticotomy",
                "descriptor": "",
                "fee": "700.51",
                "full_code": "77601"
              },
              {
                "description": "Interdental Septotomy",
                "descriptor": "",
                "fee": "700.51",
                "full_code": "77602"
              },
              {
                "description": "Surgical Expansion of the Palate",
                "descriptor": "",
                "fee": "1200.59",
                "full_code": "77603"
              },
              {
                "description": "Surgical Expansion of Alveolar Ridge \u2013 Ridge Splitting Technique, Maxilla \u2013 per Sextant",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "77604"
              },
              {
                "description": "Surgical Expansion of Alveolar Ridge \u2013 Ridge Splitting Technique, Mandible \u2013 per Sextant",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "77605"
              }
            ],
            "descriptor": "",
            "description": "MISCELLANEOUS TREATMENT OF MAXILLOFACIAL DEFORMITIES",
            "fee": ""
          },
          {
            "full_code": "77700",
            "children": [
              {
                "description": "Palatorrhaphy, Anterior (closure of palatine fissure)",
                "descriptor": "",
                "fee": "2401.22",
                "full_code": "77701"
              },
              {
                "description": "Palatorrhaphy, Posterior",
                "descriptor": "",
                "fee": "2401.22",
                "full_code": "77702"
              },
              {
                "description": "Palatorrhaphy, Total",
                "descriptor": "",
                "fee": "3001.55",
                "full_code": "77703"
              },
              {
                "description": "Palatorrhaphy, with Bone Graft",
                "descriptor": "",
                "fee": "4002.04",
                "full_code": "77704"
              },
              {
                "description": "Palatorrhaphy, Bone Graft to Anterior Alveolar Ridge",
                "descriptor": "",
                "fee": "2601.32",
                "full_code": "77705"
              }
            ],
            "descriptor": "",
            "description": "PALATORRHAPHY",
            "fee": ""
          },
          {
            "full_code": "77800",
            "children": [
              {
                "description": "Frenectomy, Upper Labial",
                "descriptor": "",
                "fee": "262.81",
                "full_code": "77801"
              },
              {
                "description": "Frenectomy, Lower Labial",
                "descriptor": "",
                "fee": "262.81",
                "full_code": "77802"
              },
              {
                "description": "Frenectomy, Lower Lingual or \"Z\" Plasty",
                "descriptor": "",
                "fee": "262.81",
                "full_code": "77803"
              },
              {
                "description": "Frenectomy, Lower Lingual or \"Z\" Plasty with Myotomy of Genioglossus",
                "descriptor": "",
                "fee": "450.43",
                "full_code": "77804"
              },
              {
                "description": "Frenoplasty, Upper \"Z\"",
                "descriptor": "",
                "fee": "394.25",
                "full_code": "77805"
              },
              {
                "description": "Frenoplasty, Lower \"Z\"",
                "descriptor": "",
                "fee": "394.25",
                "full_code": "77806"
              }
            ],
            "descriptor": "",
            "description": "FRENECTOMY/FRENOPLASTY",
            "fee": ""
          },
          {
            "full_code": "77900",
            "children": [
              {
                "description": "Glossectomy, Partial, Anterior Wedge",
                "descriptor": "",
                "fee": "700.51",
                "full_code": "77901"
              },
              {
                "description": "Glossectomy, Partial, for Orthodontic Purposes",
                "descriptor": "",
                "fee": "700.51",
                "full_code": "77902"
              },
              {
                "description": "Glossectomy, Full Postero-Anterior Wedge",
                "descriptor": "",
                "fee": "1300.64",
                "full_code": "77903"
              },
              {
                "description": "Cleft Surgery",
                "descriptor": "",
                "fee": "",
                "full_code": "77910"
              },
              {
                "description": "Primary Unilateral Cleft Lip Repair",
                "descriptor": "",
                "fee": "1350.68",
                "full_code": "77911"
              },
              {
                "description": "Secondary Unilateral Cleft Lip Repair",
                "descriptor": "",
                "fee": "1350.68",
                "full_code": "77912"
              },
              {
                "description": "Primary Bilateral Cleft Lip Repair",
                "descriptor": "",
                "fee": "1800.92",
                "full_code": "77913"
              },
              {
                "description": "Secondary Bilateral Cleft Lip Repair",
                "descriptor": "",
                "fee": "1800.92",
                "full_code": "77914"
              },
              {
                "description": "Reconstruction of Cleft Lip with Lip Switch Flap",
                "descriptor": "",
                "fee": "1800.92",
                "full_code": "77915"
              },
              {
                "description": "Complex Reconstruction or Revision of Cleft Lip",
                "descriptor": "",
                "fee": "2251.16",
                "full_code": "77916"
              },
              {
                "description": "Closure of Alveolar Cleft (see grafting Codes)",
                "descriptor": "",
                "fee": "2251.16",
                "full_code": "77917"
              },
              {
                "description": "Oral Nasal Fistula",
                "descriptor": "",
                "fee": "",
                "full_code": "77920"
              },
              {
                "description": "Primary Closure at Time of Initial Surgery",
                "descriptor": "",
                "fee": "800.4",
                "full_code": "77921"
              },
              {
                "description": "Secondary Closure with Palatal Flap",
                "descriptor": "",
                "fee": "1200.59",
                "full_code": "77922"
              },
              {
                "description": "Secondary Closure with Pharyngeal Flap",
                "descriptor": "",
                "fee": "1200.59",
                "full_code": "77923"
              },
              {
                "description": "Secondary Closure with Tongue Flap",
                "descriptor": "",
                "fee": "1350.68",
                "full_code": "77924"
              },
              {
                "description": "Secondary Closure with Buccal Flap",
                "descriptor": "",
                "fee": "1200.59",
                "full_code": "77925"
              },
              {
                "description": "Rigid Fixation",
                "descriptor": "",
                "fee": "",
                "full_code": "77930"
              },
              {
                "description": "Rigid Internal Fixation",
                "descriptor": "",
                "fee": "Add",
                "full_code": "77931"
              },
              {
                "description": "Rigid Internal Fixation Using Bone",
                "descriptor": "",
                "fee": "25% to",
                "full_code": "77932"
              },
              {
                "description": "Rigid Internal Fixation Using Alloplast",
                "descriptor": "+E",
                "fee": "Surgical",
                "full_code": "77933"
              },
              {
                "description": "Rigid Internal Fixation Using Cartilage",
                "descriptor": "",
                "fee": "fee",
                "full_code": "77934"
              }
            ],
            "descriptor": "",
            "description": "GLOSSECTOMY",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "MAXILLOFACIAL DEFORMITIES, TREATMENT OF"
      },
      {
        "full_code": "78000",
        "children": [
          {
            "full_code": "78100",
            "children": [
              {
                "description": "TMJ, Dislocation, Open Reduction",
                "descriptor": "",
                "fee": "1300.64",
                "full_code": "78101"
              },
              {
                "description": "TMJ, Dislocation, Closed Reduction, Uncomplicated\n",
                "descriptor": "",
                "fee": "118.92 to 237.86",
                "full_code": "78102"
              },
              {
                "description": "TMJ, Dislocation, Closed Reduction, Complicated (Requiring Sedation or General Anaesthesia)",
                "descriptor": "",
                "fee": "250.33",
                "full_code": "78103"
              },
              {
                "description": "TMJ, Subluxation, Closed Reduction, Uncomplicated",
                "descriptor": "",
                "fee": "237.86",
                "full_code": "78104"
              },
              {
                "description": "TMJ, Subluxation, Closed Reduction, Complicated (Requiring Sedation or General Anaesthesia)",
                "descriptor": "",
                "fee": "250.33",
                "full_code": "78105"
              },
              {
                "description": "TMJ, Manipulation, under Sedation or General Anaesthesia",
                "descriptor": "",
                "fee": "375.5",
                "full_code": "78106"
              },
              {
                "description": "TMJ, Fixation (Application of devices to prevent recurrent dislocation in the short term (arch bars,\nMMF screws, Ivy Loops)",
                "descriptor": "",
                "fee": "375.5",
                "full_code": "78107"
              }
            ],
            "descriptor": "",
            "description": "TEMPOROMANDIBULAR JOINT, DISLOCATION MANAGEMENT OF\n(Sedation and general anaesthesia services to be coded separately with appropriate 90000 series codes)",
            "fee": ""
          },
          {
            "full_code": "78200",
            "children": [
              {
                "description": "Condyloplasty",
                "descriptor": "",
                "fee": "2001.02",
                "full_code": "78201"
              },
              {
                "description": "Condylotomy",
                "descriptor": "",
                "fee": "1200.59",
                "full_code": "78202"
              },
              {
                "description": "Condylectomy",
                "descriptor": "",
                "fee": "2151.11",
                "full_code": "78203"
              },
              {
                "description": "Eminoplasty",
                "descriptor": "",
                "fee": "2151.11",
                "full_code": "78204"
              },
              {
                "description": "Re-contour of Glenoid Fossa",
                "descriptor": "",
                "fee": "2151.11",
                "full_code": "78205"
              },
              {
                "description": "Menisectomy",
                "descriptor": "",
                "fee": "2001.02",
                "full_code": "78206"
              },
              {
                "description": "Plication of Meniscus",
                "descriptor": "",
                "fee": "2151.11",
                "full_code": "78207"
              },
              {
                "description": "Repair of Meniscus",
                "descriptor": "",
                "fee": "2151.11",
                "full_code": "78208"
              },
              {
                "description": "Replacement of Meniscus (see grafting codes)",
                "descriptor": "",
                "fee": "2151.11",
                "full_code": "78209"
              }
            ],
            "descriptor": "",
            "description": "TEMPOROMANDIBULAR JOINT, OPEN PROCEDURES (ARTHROTOMY)",
            "fee": ""
          },
          {
            "full_code": "78300",
            "children": [
              {
                "description": "Fossa Replacement (see grafting codes)",
                "descriptor": "",
                "fee": "2151.11",
                "full_code": "78301"
              },
              {
                "description": "Condylar Replacement (see grafting codes)",
                "descriptor": "",
                "fee": "2151.11",
                "full_code": "78302"
              },
              {
                "description": "Gap, Arthroplasty for Ankylosis (see grafting codes)",
                "descriptor": "",
                "fee": "3401.75",
                "full_code": "78303"
              }
            ],
            "descriptor": "",
            "description": "TEMPOROMANDIBULAR JOINT, ARTHROTOMY FOR MAJOR RECONSTRUCTION",
            "fee": ""
          },
          {
            "full_code": "78400",
            "children": [
              {
                "description": "TMJ Arthroscopic Examination and Diagnosis",
                "descriptor": "",
                "fee": "600.3",
                "full_code": "78401"
              },
              {
                "description": "Biopsy",
                "descriptor": "",
                "fee": "850.44",
                "full_code": "78402"
              },
              {
                "description": "Removal of Loose Bodies",
                "descriptor": "",
                "fee": "850.44",
                "full_code": "78403"
              },
              {
                "description": "Lavage",
                "descriptor": "",
                "fee": "600.3",
                "full_code": "78404"
              },
              {
                "description": "Lysis of Adhesions",
                "descriptor": "",
                "fee": "850.44",
                "full_code": "78405"
              },
              {
                "description": "Synovectomy",
                "descriptor": "",
                "fee": "1300.64",
                "full_code": "78406"
              },
              {
                "description": "Condyloplasty",
                "descriptor": "",
                "fee": "1300.64",
                "full_code": "78407"
              },
              {
                "description": "Eminoplasty",
                "descriptor": "",
                "fee": "1300.64",
                "full_code": "78408"
              },
              {
                "description": "Re-contour of Glenoid Fossa",
                "descriptor": "",
                "fee": "1300.64",
                "full_code": "78409"
              },
              {
                "description": "Menisectomy",
                "descriptor": "",
                "fee": "1500.77",
                "full_code": "78411"
              },
              {
                "description": "Plication of Meniscus",
                "descriptor": "",
                "fee": "1500.77",
                "full_code": "78412"
              },
              {
                "description": "Repair of Meniscus",
                "descriptor": "",
                "fee": "1500.77",
                "full_code": "78413"
              }
            ],
            "descriptor": "",
            "description": "ARTHROSCOPY OF TEMPOROMANDIBULAR JOINT",
            "fee": ""
          },
          {
            "full_code": "78500",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "143.88",
                "full_code": "78501"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "287.76",
                "full_code": "78502"
              },
              {
                "description": "Each additional unit over two",
                "descriptor": "",
                "fee": "143.88",
                "full_code": "78509"
              }
            ],
            "descriptor": "",
            "description": "TEMPOROMANDIBULAR JOINT, ARTHROCENTESIS (puncture and aspiration)",
            "fee": ""
          },
          {
            "full_code": "78600",
            "children": [
              {
                "description": "Injection, therapeutic drug with or without local anaesthetic drug, \"per site\",",
                "descriptor": "+E",
                "fee": "150.12",
                "full_code": "78601"
              },
              {
                "description": "Injection, with Sclerosing Agent",
                "descriptor": "",
                "fee": "150.12",
                "full_code": "78602"
              }
            ],
            "descriptor": "",
            "description": "TEMPOROMANDIBULAR JOINT, MANAGEMENT BY INJECTIONS",
            "fee": ""
          },
          {
            "full_code": "78700",
            "children": [
              {
                "description": "Appliance Splint, Maxillary",
                "descriptor": "+L",
                "fee": "1013.3",
                "full_code": "78701"
              },
              {
                "description": "Appliance Splint, Mandibular",
                "descriptor": "+L",
                "fee": "1013.3",
                "full_code": "78702"
              }
            ],
            "descriptor": "",
            "description": "TEMPOROMANDIBULAR JOINT, APPLIANCE SPLINTS, ORTHOPEDIC REHABILITATION (post operative)",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "TEMPOROMANDIBULAR JOINT DYSFUNCTIONS, TREATMENT OF"
      },
      {
        "full_code": "79000",
        "children": [
          {
            "full_code": "79100",
            "children": [
              {
                "description": "Salivary Duct, Dilation of",
                "descriptor": "",
                "fee": "206.47",
                "full_code": "79101"
              },
              {
                "description": "Salivary Duct, Insertion of Polyethylene Tube",
                "descriptor": "",
                "fee": "275.32",
                "full_code": "79102"
              },
              {
                "description": "Salivary Duct, Sialodochoplasty",
                "descriptor": "",
                "fee": "600.59",
                "full_code": "79103"
              },
              {
                "description": "Salivary Duct, Reconstruction of",
                "descriptor": "",
                "fee": "900.9",
                "full_code": "79104"
              },
              {
                "description": "Salivary Duct, Sialolithotomy",
                "descriptor": "",
                "fee": "",
                "full_code": "79110"
              },
              {
                "description": "Sialolithotomy, Anterior 1/3 of Canal",
                "descriptor": "",
                "fee": "550.64",
                "full_code": "79111"
              },
              {
                "description": "Sialolithotomy, Posterior 2/3 of Canal",
                "descriptor": "",
                "fee": "1501.49",
                "full_code": "79112"
              },
              {
                "description": "Sialolithotomy, External Approach",
                "descriptor": "",
                "fee": "2326.29",
                "full_code": "79113"
              },
              {
                "description": "Salivary Gland, Excisions",
                "descriptor": "",
                "fee": "",
                "full_code": "79120"
              },
              {
                "description": "Excision of Submaxillary Gland",
                "descriptor": "",
                "fee": "1500.9",
                "full_code": "79121"
              },
              {
                "description": "Excision of Sublingual Gland",
                "descriptor": "",
                "fee": "1876.15",
                "full_code": "79122"
              },
              {
                "description": "Excision of Mucocele",
                "descriptor": "",
                "fee": "187.75",
                "full_code": "79123"
              },
              {
                "description": "Excision of Ranula",
                "descriptor": "",
                "fee": "600.59",
                "full_code": "79124"
              },
              {
                "description": "Marsupialization of Ranula",
                "descriptor": "",
                "fee": "550.64",
                "full_code": "79125"
              },
              {
                "description": "Salivary Gland, Removal",
                "descriptor": "",
                "fee": "",
                "full_code": "79130"
              },
              {
                "description": "Salivary Gland, Removal, Parotid (sub total)",
                "descriptor": "",
                "fee": "2001.02",
                "full_code": "79131"
              },
              {
                "description": "Salivary Gland, Removal, Parotid (radical, including facial nerve)",
                "descriptor": "",
                "fee": "3201.65",
                "full_code": "79132"
              }
            ],
            "descriptor": "",
            "description": "SALIVARY GLANDS, TREATMENT OF",
            "fee": ""
          },
          {
            "full_code": "79200",
            "children": [
              {
                "description": "Neurological Disturbances, Trigeminal Nerve",
                "descriptor": "",
                "fee": "",
                "full_code": "79210"
              },
              {
                "description": "Trigeminal Nerve, Injection for Destruction",
                "descriptor": "",
                "fee": "300.28",
                "full_code": "79211"
              },
              {
                "description": "Trigeminal Nerve, Avulsion at Periphery",
                "descriptor": "",
                "fee": "625.58",
                "full_code": "79212"
              },
              {
                "description": "Trigeminal Nerve, Total Avulsion of a Branch",
                "descriptor": "",
                "fee": "1138.46",
                "full_code": "79213"
              },
              {
                "description": "Trigeminal Nerve, Alcoholization of a Branch",
                "descriptor": "",
                "fee": "300.28",
                "full_code": "79214"
              },
              {
                "description": "Trigeminal Nerve, Infiltration of a Branch for Diagnosis",
                "descriptor": "",
                "fee": "143.88",
                "full_code": "79215"
              },
              {
                "description": "Trigeminal Nerve, Intraoperative, diagnostic or physiologic monitoring (stimulation with recording evoked potentials, ultrasound, or impedance)",
                "descriptor": "",
                "fee": "275.32",
                "full_code": "79216"
              },
              {
                "description": "Trigeminal Nerve, Neurolysis or tumor excision of trigeminal nerve branch in soft tissue",
                "descriptor": "",
                "fee": "900.9",
                "full_code": "79217"
              },
              {
                "description": "Trigeminal Nerve, Neurolysis or tumor excision of trigeminal nerve branch in bone (mandible, maxilla\nor orbit) (not to include osteotomy)",
                "descriptor": "",
                "fee": "1751.27",
                "full_code": "79218"
              },
              {
                "description": "Neurological Disturbances, Mental Nerve",
                "descriptor": "",
                "fee": "",
                "full_code": "79220"
              },
              {
                "description": "Mental Nerve, Transportation of",
                "descriptor": "",
                "fee": "1050.76",
                "full_code": "79221"
              },
              {
                "description": "Mental Nerve, Decompression in Canal",
                "descriptor": "",
                "fee": "1050.76",
                "full_code": "79222"
              },
              {
                "description": "Neurological Disturbances, Inferior Dental Nerve",
                "descriptor": "",
                "fee": "",
                "full_code": "79230"
              },
              {
                "description": "Inferior Dental Nerve, Complete Avulsion",
                "descriptor": "",
                "fee": "1050.76",
                "full_code": "79231"
              },
              {
                "description": "Inferior Dental Nerve, Decompression in the Canal",
                "descriptor": "",
                "fee": "1088.23",
                "full_code": "79232"
              },
              {
                "description": "Neurological Disturbances, Surgery",
                "descriptor": "",
                "fee": "",
                "full_code": "79240"
              },
              {
                "description": "Injured Nerve Repair, Primary",
                "descriptor": "",
                "fee": "1401.02",
                "full_code": "79241"
              },
              {
                "description": "Injured Nerve Repair, Secondary",
                "descriptor": "",
                "fee": "3551.84",
                "full_code": "79242"
              },
              {
                "description": "Injured Nerve Repair, Secondary, (when repair delayed more than four weeks)",
                "descriptor": "",
                "fee": "4002.04",
                "full_code": "79243"
              },
              {
                "description": "Neural Transposition and Decompression",
                "descriptor": "",
                "fee": "1050.76",
                "full_code": "79244"
              },
              {
                "description": "Implantation of Electrode for Peripheral Nerve Stimulation",
                "descriptor": "",
                "fee": "1401.02",
                "full_code": "79245"
              },
              {
                "description": "Excision of Tumor or Neuroma",
                "descriptor": "",
                "fee": "1500.9",
                "full_code": "79246"
              },
              {
                "description": "Nerve Repair with Graft",
                "descriptor": "+E",
                "fee": "5002.57",
                "full_code": "79247"
              },
              {
                "description": "Harvesting of Nerve Graft",
                "descriptor": "",
                "fee": "1751.27",
                "full_code": "79248"
              },
              {
                "description": "Epineurial Suture of Trigeminal Nerve Branch per Anastomosis",
                "descriptor": "",
                "fee": "1088.23",
                "full_code": "79251"
              },
              {
                "description": "Fascicular Suture of Trigeminal Nerve Branch per Anastomosis",
                "descriptor": "",
                "fee": "1088.23",
                "full_code": "79252"
              },
              {
                "description": "Conduit Implant for Repair of Nerve Gap up to 3 cm.",
                "descriptor": "",
                "fee": "2801.45",
                "full_code": "79253"
              },
              {
                "description": "Conduit Implant for Repair of Nerve Gap greater than 3 cm.",
                "descriptor": "",
                "fee": "4002.04",
                "full_code": "79254"
              },
              {
                "description": "Fibrin adhesive per nerve anastomosis",
                "descriptor": "",
                "fee": "700.51",
                "full_code": "79255"
              },
              {
                "description": "Laser coagulation per verve anastomosis",
                "descriptor": "",
                "fee": "750.45",
                "full_code": "79256"
              },
              {
                "description": "In addition to above procedures, when using operating microscopes",
                "descriptor": "",
                "fee": "150.12",
                "full_code": "79258"
              }
            ],
            "descriptor": "",
            "description": "NEUROLOGICAL DISTURBANCES, TREATMENT OF"
          },
          {
            "full_code": "79300",
            "children": [
              {
                "description": "Antral Surgery, Recovery, Foreign Bodies",
                "descriptor": "",
                "fee": "",
                "full_code": "79310"
              },
              {
                "description": "Antral Surgery, Immediate Recovery of a Dental Root or Foreign Body from the Antrum\n",
                "descriptor": "",
                "fee": "625.58 to 938.36",
                "full_code": "79311"
              },
              {
                "description": "Antral Surgery, Immediate Closure of Antrum by Another Dental Surgeon\n",
                "descriptor": "",
                "fee": "625.58 to 938.36",
                "full_code": "79312"
              },
              {
                "description": "Antral Surgery, Delayed Recovery of a Dental Root with Oral Antrostomy\n",
                "descriptor": "",
                "fee": "625.58 to 938.36",
                "full_code": "79313"
              },
              {
                "description": "Antral Surgery with Nasal Antrostomy\n",
                "descriptor": "",
                "fee": "625.58 to 938.36",
                "full_code": "79314"
              },
              {
                "description": "Antral Surgery, Lavage",
                "descriptor": "",
                "fee": "",
                "full_code": "79320"
              },
              {
                "description": "Lavage, Oral Approach",
                "descriptor": "",
                "fee": "131.41",
                "full_code": "79321"
              },
              {
                "description": "Lavage, Nasal Approach",
                "descriptor": "",
                "fee": "131.41",
                "full_code": "79322"
              },
              {
                "description": "Antral Surgery, Oro-Antral Fistula Closure, (same session)",
                "descriptor": "",
                "fee": "",
                "full_code": "79330"
              },
              {
                "description": "Oro-Antral Fistula Closure with Buccal Flap\n",
                "descriptor": "",
                "fee": "600.59 to 900.9",
                "full_code": "79331"
              },
              {
                "description": "Oro-Antral Fistula Closure with Gold Plate\n",
                "descriptor": "+L",
                "fee": "600.59 to 900.9",
                "full_code": "79332"
              },
              {
                "description": "Oro-Antral Fistula Closure with Palatal Flap\n",
                "descriptor": "",
                "fee": "600.59 to 900.9",
                "full_code": "79333"
              },
              {
                "description": "Antral Surgery, Oro-Antral Fistula Closure, (subsequent session)",
                "descriptor": "",
                "fee": "",
                "full_code": "79340"
              },
              {
                "description": "Oro-Antral Fistula Closure with Buccal Flap\n",
                "descriptor": "",
                "fee": "600.59 to 900.9",
                "full_code": "79341"
              },
              {
                "description": "Oro-Antral Fistula Closure with Gold Plate\n",
                "descriptor": "",
                "fee": "600.59 to 900.9",
                "full_code": "79342"
              },
              {
                "description": "Oro-Antral Fistula Closure with Palatal Flap\n",
                "descriptor": "",
                "fee": "600.59 to 900.9",
                "full_code": "79343"
              },
              {
                "description": "Sinus Osseous Augmentation",
                "descriptor": "",
                "fee": "",
                "full_code": "79350"
              },
              {
                "description": "Sinus Osseous Augmentation, Open Lateral Approach - Autograft",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "79351"
              },
              {
                "description": "Sinus Osseous Augmentation, Open Lateral Approach \u2013 Allograft",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "79352"
              },
              {
                "description": "Sinus Osseous Augmentation, Open Lateral Approach \u2013 Xenograft",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "79353"
              },
              {
                "description": "Sinus Osseous Augmentation, Indirect Inferior Approach \u2013 Autograft",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "79354"
              },
              {
                "description": "Sinus Osseous Augmentation, Indirect Inferior Approach \u2013 Allograft",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "79355"
              },
              {
                "description": "Sinus Osseous Augmentation, Indirect Inferior Approach \u2013 Xenograft",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "79356"
              }
            ],
            "descriptor": "",
            "description": "ANTRAL SURGERY"
          },
          {
            "full_code": "79400",
            "children": [
              {
                "description": "Primary Hemorrhage, Control\n",
                "descriptor": "",
                "fee": "150.12 to 600.59",
                "full_code": "79401"
              },
              {
                "description": "Secondary Hemorrhage, Control\n",
                "descriptor": "",
                "fee": "175.11 to 1751.27",
                "full_code": "79402"
              },
              {
                "description": "Hemorrhage Control, using Compression and Hemostatic Agent\n",
                "descriptor": "",
                "fee": "175.11 to 1751.27",
                "full_code": "79403"
              },
              {
                "description": "Hemorrhage Control, using Hemostatic Substance and Suture (including\nremoval of bony tissue, if necessary)",
                "descriptor": "",
                "fee": "175.11 to 1751.27",
                "full_code": "79404"
              }
            ],
            "descriptor": "",
            "description": "HEMORRHAGE, CONTROL OF",
            "fee": ""
          },
          {
            "full_code": "79500",
            "children": [
              {
                "description": "Harvesting of Intraoral Tissue For Grafting To Operative Site",
                "descriptor": "",
                "fee": "",
                "full_code": "79510"
              },
              {
                "description": "Bone",
                "descriptor": "",
                "fee": "506.65",
                "full_code": "79511"
              },
              {
                "description": "Cartilage",
                "descriptor": "",
                "fee": "506.65",
                "full_code": "79512"
              },
              {
                "description": "Skin",
                "descriptor": "",
                "fee": "506.65",
                "full_code": "79513"
              },
              {
                "description": "Mucosa",
                "descriptor": "",
                "fee": "506.65",
                "full_code": "79514"
              },
              {
                "description": "Fascia",
                "descriptor": "",
                "fee": "506.65",
                "full_code": "79515"
              },
              {
                "description": "Muscle",
                "descriptor": "",
                "fee": "506.65",
                "full_code": "79516"
              },
              {
                "description": "Dermis",
                "descriptor": "",
                "fee": "506.65",
                "full_code": "79517"
              },
              {
                "description": "Harvesting of Extraoral Tissue For Grafting To Operative Site (To Include Ilium, Rib, Etc.)",
                "descriptor": "",
                "fee": "",
                "full_code": "79520"
              },
              {
                "description": "Bone",
                "descriptor": "",
                "fee": "700.51",
                "full_code": "79521"
              },
              {
                "description": "Cartilage",
                "descriptor": "",
                "fee": "700.51",
                "full_code": "79522"
              },
              {
                "description": "Costochondral",
                "descriptor": "",
                "fee": "700.51",
                "full_code": "79523"
              },
              {
                "description": "Skin",
                "descriptor": "",
                "fee": "700.51",
                "full_code": "79524"
              },
              {
                "description": "Fat",
                "descriptor": "",
                "fee": "700.51",
                "full_code": "79525"
              },
              {
                "description": "Fascia",
                "descriptor": "",
                "fee": "700.51",
                "full_code": "79526"
              },
              {
                "description": "Muscle",
                "descriptor": "",
                "fee": "700.51",
                "full_code": "79527"
              },
              {
                "description": "Dermis",
                "descriptor": "",
                "fee": "700.51",
                "full_code": "79528"
              },
              {
                "description": "Nerve",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "79529"
              },
              {
                "description": "Vascularized Tissue Flaps, Extraoral",
                "descriptor": "",
                "fee": "",
                "full_code": "79530"
              },
              {
                "description": "Elevation Free Soft Tissue Flap",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "79531"
              },
              {
                "description": "Elevation Free Hard Tissue Flap",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "79532"
              },
              {
                "description": "Artery/Vein/Nerve Graft/Patch, Autogenous/Allograft/Alloplastic",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "79539"
              },
              {
                "description": "Harvesting and Preparation of Platelet Rich Plasma",
                "descriptor": "",
                "fee": "",
                "full_code": "79540"
              },
              {
                "description": "Harvesting and Preparation of Platelet Rich Plasma",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "79541"
              },
              {
                "description": "Delivery of Growth Factors",
                "descriptor": "",
                "fee": "",
                "full_code": "79550"
              },
              {
                "description": "Delivery of Growth Factors \u2013 Autologous \u2013 per site",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "79551"
              },
              {
                "description": "Delivery of Growth Factors \u2013 Allogenic \u2013 per site",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "79552"
              },
              {
                "description": "Delivery of Growth Factors \u2013 Human Recombinant \u2013 per site",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "79553"
              }
            ],
            "descriptor": "",
            "description": "GRAFTS AND RECONSTRUCTION, SURGICAL"
          },
          {
            "full_code": "79600",
            "children": [
              {
                "description": "Post Surgical Care, Subsequent to Initial Post Surgical Treatment, Minor, by Treating Dentist",
                "descriptor": "",
                "fee": "125.17",
                "full_code": "79601"
              },
              {
                "description": "Post Surgical Care, Minor, by Other Than Treating Dentist",
                "descriptor": "",
                "fee": "131.41",
                "full_code": "79602"
              },
              {
                "description": "Post Surgical Care, Major, by Treating Dentist\n",
                "descriptor": "",
                "fee": "131.41 to 1314.19",
                "full_code": "79603"
              },
              {
                "description": "Post Surgical Care, Major, by Other Than Treating Dentist\n",
                "descriptor": "",
                "fee": "131.41 to 1314.19",
                "full_code": "79604"
              },
              {
                "description": "Post Surgical Care, Alveolitis, Treatment of (without Anaesthesia)",
                "descriptor": "",
                "fee": "131.41",
                "full_code": "79605"
              },
              {
                "description": "Post Surgical Care, Alveolitis, Treatment of (with Anaesthesia)",
                "descriptor": "",
                "fee": "131.41",
                "full_code": "79606"
              }
            ],
            "descriptor": "",
            "description": "POST SURGICAL CARE (Required by complications and unusual circumstances, refer to comment\nunder section heading 70000)",
            "fee": ""
          },
          {
            "full_code": "79700",
            "children": [
              {
                "description": "Tracheotomy",
                "descriptor": "",
                "fee": "800.4",
                "full_code": "79701"
              },
              {
                "description": "Crico-Thyroidotomy",
                "descriptor": "",
                "fee": "800.4",
                "full_code": "79702"
              }
            ],
            "descriptor": "",
            "description": "AIRWAY PROCEDURES",
            "fee": ""
          },
          {
            "full_code": "79800",
            "children": [
              {
                "description": "Treatment of Muscular Dysfunctions",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "79801"
              },
              {
                "description": "Myotomy",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "79802"
              }
            ],
            "descriptor": "",
            "description": "MUSCULAR DISORDERS, TREATMENT OF",
            "fee": ""
          },
          {
            "full_code": "79900",
            "children": [
              {
                "description": "Implants, Blade",
                "descriptor": "",
                "fee": "",
                "full_code": "79910"
              },
              {
                "description": "Maxillary per implant",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "79911"
              },
              {
                "description": "Mandibular per implant",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "79912"
              },
              {
                "description": "Implants, Subperiosteal",
                "descriptor": "",
                "fee": "",
                "full_code": "79920"
              },
              {
                "description": "Maxillary",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "79921"
              },
              {
                "description": "Mandibular",
                "descriptor": "+L",
                "fee": "I.C.",
                "full_code": "79922"
              },
              {
                "description": "Implants, Ossenointegrated, Root Form, More than one component",
                "descriptor": "",
                "fee": "",
                "full_code": "79930"
              },
              {
                "description": "Surgical Installation of Implant with Cover Screw \u2013 per Implant",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "79931"
              },
              {
                "description": "Surgical Installation of Implant with Healing Transmucosal Element \u2013 per Implant",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "79932"
              },
              {
                "description": "Surgical Installation of Implant with Final Transmucusal Element \u2013 per Implant",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "79933"
              },
              {
                "description": "Surgical Re-entry, Removal of Healing Screw and Placement of Healing Transmucosal Element \u2013 per\nImplant",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "79934"
              },
              {
                "description": "Surgical Re-entry, Removal of Healing Screw and Placement of Final Standard Transmucosal Element \u2013\nper Implant",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "79935"
              },
              {
                "description": "Surgical Re-entry, Removal of Healing Screw and Placement of Final Custom Transmucosal Element \u2013\nper Implant",
                "descriptor": "+L +E",
                "fee": "I.C.",
                "full_code": "79936"
              },
              {
                "description": "Implants Osseointegrated, Root Form, Single Component",
                "descriptor": "",
                "fee": "",
                "full_code": "79940"
              },
              {
                "description": "Surgical Installation of Implant \u2013 per Implant",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "79941"
              },
              {
                "description": "Implants, Osseointegrated, Provisional",
                "descriptor": "",
                "fee": "",
                "full_code": "79950"
              },
              {
                "description": "Installation of Provisional Implant \u2013 per Implant",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "79951"
              },
              {
                "description": "Removal of Provisional Implant \u2013 per Implant",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "79952"
              },
              {
                "description": "Implants, Removal of",
                "descriptor": "",
                "fee": "",
                "full_code": "79960"
              },
              {
                "description": "Per implant, Uncomplicated",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "79961"
              },
              {
                "description": "Per implant, Complicated",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "79962"
              }
            ],
            "descriptor": "",
            "description": "IMPLANTOLOGY (Includes placement of implant, post-surgical care, uncovering and placement of\nattachment but not prosthesis)"
          }
        ],
        "descriptor": "",
        "description": "MAXILLOFACIAL SURGERY PROCEDURES, OTHER"
      }
    ],
    "descriptor": "",
    "description": "ORAL MAXILLOFACIAL SURGERY The following surgical services include necessary local anaesthetic, removal of excess gingival tissue, suturing and one post-operative treatment, when required. A surgical site is an area that lends itself to one or more procedures. It is considered to include a full quadrant, sextant or a group of teeth or in some cases a single tooth."
  },
  {
    "full_code": "80000",
    "children": [
      {
        "full_code": "80600",
        "children": [
          {
            "full_code": "80601",
            "children": [
              {
                "description": "Orthodontic Observation and adjustment - to Orthodontic Appliances and/or the Reduction of\nProximal Surfaces of Teeth per appointment",
                "descriptor": "",
                "fee": "88.84",
                "full_code": "80602"
              },
              {
                "description": "Repairs to Removable or Fixed Appliances (not including removal and recementation)",
                "descriptor": "",
                "fee": "",
                "full_code": "80630"
              },
              {
                "description": "One unit of time",
                "descriptor": "+L",
                "fee": "96.24",
                "full_code": "80631"
              },
              {
                "description": "Two units",
                "descriptor": "+L",
                "fee": "192.48",
                "full_code": "80632"
              },
              {
                "description": "Each additional unit over two",
                "descriptor": "",
                "fee": "96.24",
                "full_code": "80639"
              },
              {
                "description": "Alterations to Removable or Fixed Appliances",
                "descriptor": "",
                "fee": "",
                "full_code": "80640"
              },
              {
                "description": "One unit of time",
                "descriptor": "+L",
                "fee": "96.24",
                "full_code": "80641"
              },
              {
                "description": "Two units",
                "descriptor": "+L",
                "fee": "192.48",
                "full_code": "80642"
              },
              {
                "description": "Each additional unit over two",
                "descriptor": "",
                "fee": "96.24",
                "full_code": "80649"
              },
              {
                "description": "Recementation of Fixed Appliances",
                "descriptor": "",
                "fee": "",
                "full_code": "80650"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "96.24",
                "full_code": "80651"
              },
              {
                "description": "Each additional unit of time",
                "descriptor": "",
                "fee": "96.24",
                "full_code": "80659"
              },
              {
                "description": "Separation (except where included in the fabrication of an appliance)",
                "descriptor": "",
                "fee": "",
                "full_code": "80660"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "96.24",
                "full_code": "80661"
              },
              {
                "description": "Each addition unit of time",
                "descriptor": "",
                "fee": "96.24",
                "full_code": "80669"
              },
              {
                "description": "Removal of Fixed Orthodontic Appliances (By a Practitioner Other Than The Original Treatment\nPractice Or Practitioner)",
                "descriptor": "",
                "fee": "",
                "full_code": "80670"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "96.23",
                "full_code": "80671"
              },
              {
                "description": "Each additional unit of time",
                "descriptor": "",
                "fee": "96.23",
                "full_code": "80679"
              }
            ],
            "descriptor": "",
            "description": "Orthodontic Observation - for Tooth Guidance (i.e. tooth position, eruption sequence, serial extraction\nsupervision, etc.) per appointment",
            "fee": "88.84"
          }
        ],
        "descriptor": "",
        "description": "ORTHODONTIC, OBSERVATIONS AND ADJUSTMENTS",
        "fee": ""
      },
      {
        "full_code": "81000",
        "children": [
          {
            "full_code": "81100",
            "children": [
              {
                "description": "Appliances, Removable, Space Regaining",
                "descriptor": "",
                "fee": "",
                "full_code": "81110"
              },
              {
                "description": "Appliance, Maxillary, Unilateral",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "81111"
              },
              {
                "description": "Appliance, Mandibular, Unilateral",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "81112"
              },
              {
                "description": "Appliance, Maxillary, Bilateral",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "81113"
              },
              {
                "description": "Appliance, Mandibular, Bilateral",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "81114"
              },
              {
                "description": "Appliances, Removable, Cross-Bite Correction",
                "descriptor": "",
                "fee": "",
                "full_code": "81120"
              },
              {
                "description": "Appliance, Maxillary, Simple",
                "descriptor": "+L",
                "fee": "364.88",
                "full_code": "81121"
              },
              {
                "description": "Appliance, Mandibular, Simple",
                "descriptor": "+L",
                "fee": "364.88",
                "full_code": "81122"
              },
              {
                "description": "Appliances, Removable, Dental Arch Expansion",
                "descriptor": "",
                "fee": "",
                "full_code": "81130"
              },
              {
                "description": "Appliance, Maxillary, Simple",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "81131"
              },
              {
                "description": "Appliances, Mandibular, Simple",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "81132"
              },
              {
                "description": "Appliances, Removable, Closure of Diastemas",
                "descriptor": "",
                "fee": "",
                "full_code": "81140"
              },
              {
                "description": "Appliance, Maxillary, Simple",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "81141"
              },
              {
                "description": "Appliance, Mandibular, Simple",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "81142"
              },
              {
                "description": "Appliances, Removable, Alignment of Anterior Teeth",
                "descriptor": "",
                "fee": "",
                "full_code": "81150"
              },
              {
                "description": "Appliance, Maxillary, Simple",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "81151"
              },
              {
                "description": "Appliance, Mandibular, Simple",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "81152"
              }
            ],
            "descriptor": "",
            "description": "APPLIANCES, REMOVABLE A maximum of eight observations or adjustment appointments may be charged for these appliances."
          },
          {
            "full_code": "81200",
            "children": [
              {
                "description": "Appliance, Fixed, Space Regaining (e.g. lingual or labial arch with molar bands, tubes, locks)",
                "descriptor": "",
                "fee": "",
                "full_code": "81210"
              },
              {
                "description": "Appliance, Maxillary",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "81211"
              },
              {
                "description": "Appliance, Mandibular",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "81212"
              },
              {
                "description": "Appliance, Fixed, Spaces Regaining, Unilateral",
                "descriptor": "",
                "fee": "",
                "full_code": "81220"
              },
              {
                "description": "Appliance, Maxillary",
                "descriptor": "+L",
                "fee": "288.73",
                "full_code": "81221"
              },
              {
                "description": "Appliance, Mandibular",
                "descriptor": "+L",
                "fee": "288.73",
                "full_code": "81222"
              },
              {
                "description": "Appliance, Fixed, Cross-Bite Correction - Anterior",
                "descriptor": "",
                "fee": "",
                "full_code": "81230"
              },
              {
                "description": "Appliance, Maxillary",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "81231"
              },
              {
                "description": "Appliance, Mandibular",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "81232"
              },
              {
                "description": "Appliance, Fixed, Cross-Bite Correction - Posterior",
                "descriptor": "",
                "fee": "",
                "full_code": "81240"
              },
              {
                "description": "Appliance, Maxillary",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "81241"
              },
              {
                "description": "Appliance, Mandibular",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "81242"
              },
              {
                "description": "Appliance, Two-Molar Band, Hooked and Elastics",
                "descriptor": "+L",
                "fee": "308.27",
                "full_code": "81243"
              },
              {
                "description": "Appliance, Fixed, Dental Arch Expansion",
                "descriptor": "",
                "fee": "",
                "full_code": "81250"
              },
              {
                "description": "Appliance, Maxillary",
                "descriptor": "+L",
                "fee": "481.23",
                "full_code": "81251"
              },
              {
                "description": "Appliance, Mandibular",
                "descriptor": "+L",
                "fee": "481.23",
                "full_code": "81252"
              },
              {
                "description": "Appliance, Maxillary, Rapid Expansion",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "81253"
              },
              {
                "description": "Appliance, Fixed, Closure of Diastemas",
                "descriptor": "",
                "fee": "",
                "full_code": "81260"
              },
              {
                "description": "Appliance, Maxillary, Simple",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "81261"
              },
              {
                "description": "Appliance, Mandibular, Simple",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "81262"
              },
              {
                "description": "Appliance, Fixed, Alignment of Incisor Teeth",
                "descriptor": "",
                "fee": "",
                "full_code": "81270"
              },
              {
                "description": "Appliance, Maxillary, Simple",
                "descriptor": "+L",
                "fee": "481.23",
                "full_code": "81271"
              },
              {
                "description": "Appliance, Mandibular, Simple",
                "descriptor": "+L",
                "fee": "481.22",
                "full_code": "81272"
              },
              {
                "description": "Appliances, Fixed, Ligatures",
                "descriptor": "",
                "fee": "",
                "full_code": "81280"
              },
              {
                "description": "Grassline or Elastic Ligatures per visit",
                "descriptor": "+L",
                "fee": "96.24",
                "full_code": "81281"
              },
              {
                "description": "Appliances, Fixed, Mechanical Eruption of Tooth/Teeth",
                "descriptor": "",
                "fee": "",
                "full_code": "81290"
              },
              {
                "description": "Appliance, Maxillary, Impaction",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "81291"
              },
              {
                "description": "Appliance, Mandibular, Impaction",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "81292"
              },
              {
                "description": "Appliance, Maxillary, Erupted",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "81293"
              },
              {
                "description": "Appliance, Mandibular, Erupted",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "81294"
              }
            ],
            "descriptor": "",
            "description": "APPLIANCES, FIXED OR CEMENTED A maximum of eight observations or adjustment appointments may be charged for these appliances."
          }
        ],
        "descriptor": "",
        "description": "APPLIANCES, ACTIVE, FOR TOOTH GUIDANCE OR MINOR TOOTH MOVEMENT"
      },
      {
        "full_code": "83000",
        "children": [
          {
            "full_code": "83100",
            "children": [
              {
                "description": "Appliance, Maxillary",
                "descriptor": "+L",
                "fee": "288.73",
                "full_code": "83101"
              },
              {
                "description": "Appliance, Mandibular",
                "descriptor": "+L",
                "fee": "288.73",
                "full_code": "83102"
              },
              {
                "description": "Appliance, Tooth Positioner",
                "descriptor": "+L",
                "fee": "288.73",
                "full_code": "83103"
              }
            ],
            "descriptor": "",
            "description": "APPLIANCES, REMOVABLE, RETENTION",
            "fee": ""
          },
          {
            "full_code": "83200",
            "children": [
              {
                "description": "Appliance, Maxillary",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "83201"
              },
              {
                "description": "Appliance, Mandibular COMPREHENSIVE ORTHODONTIC TREATMENT CASE TYPE - Fixed Appliance (includes formal full banded treatment and retention) The range of fees with these procedure codes reflects such variables as length of time required to complete the treatment, degree of difficulty, co-operation of the patient, etc. and the fee charged should be determined accordingly.",
                "descriptor": "+L",
                "fee": "384.71",
                "full_code": "83202"
              }
            ],
            "descriptor": "",
            "description": "APPLIANCES, FIXED/CEMENTED, RETENTION",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "APPLIANCES,  RETENTION, ORTHODONTIC RETAINING APPLIANCES"
      },
      {
        "full_code": "84000",
        "children": [
          {
            "full_code": "84101",
            "children": [],
            "descriptor": "+L",
            "description": "Class l Malocclusion\n"
          },
          {
            "full_code": "84201",
            "children": [],
            "descriptor": "+L",
            "description": "Class ll Malocclusion\n"
          },
          {
            "full_code": "84301",
            "children": [],
            "descriptor": "+L",
            "description": "Class lll Malocclusions\n"
          },
          {
            "full_code": "84401",
            "children": [],
            "descriptor": "+L",
            "description": "Malocclusions Not Requiring Complete Banding\n"
          }
        ],
        "descriptor": "",
        "description": "PERMANENT DENTITION"
      },
      {
        "full_code": "85000",
        "children": [
          {
            "full_code": "85101",
            "children": [],
            "descriptor": "+L",
            "description": "Class l Malocclusion\n"
          },
          {
            "full_code": "85201",
            "children": [],
            "descriptor": "+L",
            "description": "Class ll Malocclusion\n"
          },
          {
            "full_code": "85301",
            "children": [],
            "descriptor": "+L",
            "description": "Class lll Malocclusion\n"
          }
        ],
        "descriptor": "",
        "description": "MIXED DENTITION"
      },
      {
        "full_code": "87000",
        "children": [
          {
            "full_code": "87101",
            "children": [],
            "descriptor": "+L",
            "description": "Class l Malocclusion"
          },
          {
            "full_code": "87201",
            "children": [],
            "descriptor": "+L",
            "description": "Class ll Malocclusion"
          },
          {
            "full_code": "87301",
            "children": [],
            "descriptor": "+L",
            "description": "Class lll Malocclusion"
          }
        ],
        "descriptor": "",
        "description": "PERMANENT DENTITION CASE TYPE - Removable Appliances (includes removable appliance therapy and retention; e.g. functional appliances)"
      },
      {
        "full_code": "88000",
        "children": [
          {
            "full_code": "88101",
            "children": [],
            "descriptor": "+L",
            "description": "Class l Malocclusion\n"
          },
          {
            "full_code": "88201",
            "children": [],
            "descriptor": "+L",
            "description": "Class ll Malocclusion\n"
          },
          {
            "full_code": "88301",
            "children": [],
            "descriptor": "+L",
            "description": "Class lll Malocclusion\n"
          }
        ],
        "descriptor": "",
        "description": "MIXED DENTITION"
      },
      {
        "full_code": "89500",
        "children": [
          {
            "full_code": "89501",
            "children": [
              {
                "description": "Extraoral Retraction Appliance for Infants with Cleft Palate\n",
                "descriptor": "+L",
                "fee": "384.99 to 3464.91",
                "full_code": "89502"
              },
              {
                "description": "Stage l - Initial Expansion\n",
                "descriptor": "+L",
                "fee": "1443.71 to 2887.44",
                "full_code": "89503"
              },
              {
                "description": "Stage ll - Anterior Alignment\n",
                "descriptor": "+L",
                "fee": "1443.71 to 2887.44",
                "full_code": "89504"
              },
              {
                "description": "Stage lll - Final Alignment (complete banding)\n",
                "descriptor": "+L",
                "fee": "2887.44 to 7699.85",
                "full_code": "89505"
              },
              {
                "description": "Stage lll - Where Stage l and ll were not provided for\n",
                "descriptor": "+L",
                "fee": "5774.87 to 15399.71",
                "full_code": "89506"
              }
            ],
            "descriptor": "+L",
            "description": "Expansion Appliance for Infants with Cleft Palate\n",
            "fee": "384.99 to 3464.91"
          }
        ],
        "descriptor": "",
        "description": "NEONATAL DENTO-FACIAL ORTHOPEDICS (comprehensive treatment for first six months of life) (1) Diagnostic procedures (includes radiographs and/or photographs); (2) Parent consultation; (3) Impression and appliance construction; (4) Insertion and parent instruction; (5) Post treatment evaluation; (6) Adjustment of appliances (includes soft relines); (7) Reconstruction and/or reevaluation (may include up to two remakes).",
        "fee": ""
      }
    ],
    "descriptor": "",
    "description": "ORTHODONTICS",
    "fee": ""
  },
  {
    "full_code": "90000",
    "children": [
      {
        "full_code": "91000",
        "children": [
          {
            "full_code": "91100",
            "children": [
              {
                "description": "Palliative (emergency) Treatment of Dental Pain, Minor Procedure",
                "descriptor": "",
                "fee": "",
                "full_code": "91110"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "118.93",
                "full_code": "91111"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "237.86",
                "full_code": "91112"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "356.79",
                "full_code": "91113"
              },
              {
                "description": "Each additional unit over three",
                "descriptor": "",
                "fee": "118.93",
                "full_code": "91119"
              },
              {
                "description": "Emergency Services Not Otherwise Specified In Guide",
                "descriptor": "",
                "fee": "",
                "full_code": "91120"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "125.16",
                "full_code": "91121"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "250.32",
                "full_code": "91122"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "375.48",
                "full_code": "91123"
              },
              {
                "description": "Each additional unit over three",
                "descriptor": "",
                "fee": "125.16",
                "full_code": "91129"
              }
            ],
            "descriptor": "",
            "description": "UNCLASSIFIED TREATMENT, DENTAL PAIN"
          },
          {
            "full_code": "91200",
            "children": [
              {
                "description": "Unusual Time and Responsibility Requirement, in Addition to Usual Procedures in Guide",
                "descriptor": "",
                "fee": "",
                "full_code": "91210"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "137.64",
                "full_code": "91211"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "275.28",
                "full_code": "91212"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "412.92",
                "full_code": "91213"
              },
              {
                "description": "Each additional unit over three",
                "descriptor": "",
                "fee": "137.64",
                "full_code": "91219"
              },
              {
                "description": "Second Surgeon (team approach)",
                "descriptor": "",
                "fee": "",
                "full_code": "91220"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "118.93",
                "full_code": "91221"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "237.86",
                "full_code": "91222"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "356.79",
                "full_code": "91223"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "475.72",
                "full_code": "91224"
              },
              {
                "description": "Five units",
                "descriptor": "",
                "fee": "594.65",
                "full_code": "91225"
              },
              {
                "description": "Six units",
                "descriptor": "",
                "fee": "713.58",
                "full_code": "91226"
              },
              {
                "description": "Seven units",
                "descriptor": "",
                "fee": "832.51",
                "full_code": "91227"
              },
              {
                "description": "Eight units",
                "descriptor": "",
                "fee": "951.44",
                "full_code": "91228"
              },
              {
                "description": "Each additional unit over eight",
                "descriptor": "",
                "fee": "118.93",
                "full_code": "91229"
              },
              {
                "description": "Management of Exceptional Patient",
                "descriptor": "",
                "fee": "",
                "full_code": "91230"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "137.64",
                "full_code": "91231"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "275.28",
                "full_code": "91232"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "412.92",
                "full_code": "91233"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "550.56",
                "full_code": "91234"
              },
              {
                "description": "Each additional unit over four",
                "descriptor": "",
                "fee": "137.64",
                "full_code": "91239"
              }
            ],
            "descriptor": "",
            "description": "UNCLASSIFIED TREATMENTS, UNUSUAL TIME AND RESPONSIBILITIES (Note:  If the service affected is\nanaesthesia, Service Class 92000, and the unusual time and responsibility is the result of a patient BMI of 35 or above, refer to code series 92900)"
          }
        ],
        "descriptor": "",
        "description": "UNCLASSIFIED TREATMENTS"
      },
      {
        "full_code": "92000",
        "children": [
          {
            "full_code": "92100",
            "children": [
              {
                "description": "Regional Block Anaesthesia (not in conjunction with operative or surgical procedures)",
                "descriptor": "",
                "fee": "125.17",
                "full_code": "92101"
              },
              {
                "description": "Trigeminal Division Block (not in conjunction with operative or surgical procedures)",
                "descriptor": "",
                "fee": "125.17",
                "full_code": "92102"
              }
            ],
            "descriptor": "",
            "description": "ANAESTHESIA, LOCAL (not in conjunction with operative or surgical procedures, includes pre-anaesthetic evaluation and post anaesthetic evaluation and post-anaesthetic follow-up)",
            "fee": ""
          },
          {
            "full_code": "92200",
            "children": [
              {
                "description": "General Anaesthesia",
                "descriptor": "+PS",
                "fee": "",
                "full_code": "92210"
              },
              {
                "description": "Two units of time",
                "descriptor": "",
                "fee": "262.82",
                "full_code": "92212"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "394.23",
                "full_code": "92213"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "525.64",
                "full_code": "92214"
              },
              {
                "description": "Five units",
                "descriptor": "",
                "fee": "657.05",
                "full_code": "92215"
              },
              {
                "description": "Six units",
                "descriptor": "",
                "fee": "788.46",
                "full_code": "92216"
              },
              {
                "description": "Seven units",
                "descriptor": "",
                "fee": "919.87",
                "full_code": "92217"
              },
              {
                "description": "Eight units",
                "descriptor": "",
                "fee": "1051.28",
                "full_code": "92218"
              },
              {
                "description": "Each additional unit over eight",
                "descriptor": "",
                "fee": "131.41",
                "full_code": "92219"
              },
              {
                "description": "Provision of facilities, equipment and support services for general anaesthesia when provided by a\nseparate practitioner",
                "descriptor": "",
                "fee": "",
                "full_code": "92220"
              },
              {
                "description": "Two units of time",
                "descriptor": "",
                "fee": "262.82",
                "full_code": "92222"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "394.23",
                "full_code": "92223"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "525.64",
                "full_code": "92224"
              },
              {
                "description": "Five units",
                "descriptor": "",
                "fee": "657.05",
                "full_code": "92225"
              },
              {
                "description": "Six units",
                "descriptor": "",
                "fee": "788.46",
                "full_code": "92226"
              },
              {
                "description": "Seven units",
                "descriptor": "",
                "fee": "919.87",
                "full_code": "92227"
              },
              {
                "description": "Eight units",
                "descriptor": "",
                "fee": "1051.28",
                "full_code": "92228"
              },
              {
                "description": "Each additional unit over eight",
                "descriptor": "",
                "fee": "131.41",
                "full_code": "92229"
              }
            ],
            "descriptor": "",
            "description": "ANAESTHESIA, GENERAL (includes pre-anaesthetic evaluation and post-anaesthetic evaluation and post-anaesthetic follow-up)"
          },
          {
            "full_code": "92300",
            "children": [
              {
                "description": "Two units of time",
                "descriptor": "",
                "fee": "237.86",
                "full_code": "92302"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "356.79",
                "full_code": "92303"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "475.72",
                "full_code": "92304"
              },
              {
                "description": "Five units",
                "descriptor": "",
                "fee": "594.65",
                "full_code": "92305"
              },
              {
                "description": "Six units",
                "descriptor": "",
                "fee": "713.58",
                "full_code": "92306"
              },
              {
                "description": "Seven units",
                "descriptor": "",
                "fee": "832.51",
                "full_code": "92307"
              },
              {
                "description": "Eight units",
                "descriptor": "",
                "fee": "951.44",
                "full_code": "92308"
              },
              {
                "description": "Each additional unit over eight",
                "descriptor": "",
                "fee": "118.93",
                "full_code": "92309"
              },
              {
                "description": "Provision of facilities, equipment and support services for deep sedation when provided by a\nseparate practitioner",
                "descriptor": "",
                "fee": "",
                "full_code": "92320"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "237.86",
                "full_code": "92322"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "356.79",
                "full_code": "92323"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "475.72",
                "full_code": "92324"
              },
              {
                "description": "Five units",
                "descriptor": "",
                "fee": "594.65",
                "full_code": "92325"
              },
              {
                "description": "Six units",
                "descriptor": "",
                "fee": "713.58",
                "full_code": "92326"
              },
              {
                "description": "Seven units",
                "descriptor": "",
                "fee": "832.51",
                "full_code": "92327"
              },
              {
                "description": "Eight units",
                "descriptor": "",
                "fee": "951.44",
                "full_code": "92328"
              },
              {
                "description": "Each additional unit over eight",
                "descriptor": "",
                "fee": "118.93",
                "full_code": "92329"
              }
            ],
            "descriptor": "+PS",
            "description": "ANAESTHESIA, DEEP SEDATION Anaesthesia, Deep Sedation - a controlled state of depressed consciousness accompanied by partial loss of protective reflexes, including inability to respond purposefully to verbal command. These states apply to any technique that has depressed the patient beyond conscious sedation except general anaesthesia. Any intravenous technique leading to these conditions in a patient including neuroleptanalgesia or anaesthesia, regardless of route of administration, would fall within this category of service. (includes pre-anaesthetic evaluation and post anaesthetic follow-up)",
            "fee": ""
          },
          {
            "full_code": "92400",
            "children": [
              {
                "description": "Nitrous Oxide Time is measured from the placement of the inhalation device and terminates with the\nremoval of the inhalation device",
                "descriptor": "",
                "fee": "",
                "full_code": "92410"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "62.87",
                "full_code": "92411"
              },
              {
                "description": "Two units of time",
                "descriptor": "",
                "fee": "94.32",
                "full_code": "92412"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "125.78",
                "full_code": "92413"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "157.24",
                "full_code": "92414"
              },
              {
                "description": "Five units",
                "descriptor": "",
                "fee": "188.69",
                "full_code": "92415"
              },
              {
                "description": "Six units",
                "descriptor": "",
                "fee": "220.14",
                "full_code": "92416"
              },
              {
                "description": "Seven units",
                "descriptor": "",
                "fee": "251.6",
                "full_code": "92417"
              },
              {
                "description": "Eight units",
                "descriptor": "",
                "fee": "283.05",
                "full_code": "92418"
              },
              {
                "description": "Each additional unit over eight",
                "descriptor": "",
                "fee": "31.45",
                "full_code": "92419"
              },
              {
                "description": "Oral Sedation, Sedation sufficient to require monitored care. Time is measured from the start of\npatient monitoring to release from the treatment/recovery room",
                "descriptor": "",
                "fee": "",
                "full_code": "92420"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "56.79",
                "full_code": "92421"
              },
              {
                "description": "Two units of time",
                "descriptor": "",
                "fee": "63.88",
                "full_code": "92422"
              },
              {
                "description": "Three units of time",
                "descriptor": "",
                "fee": "82.5",
                "full_code": "92423"
              },
              {
                "description": "Four units of time",
                "descriptor": "",
                "fee": "101.08",
                "full_code": "92424"
              },
              {
                "description": "Five units of time",
                "descriptor": "",
                "fee": "119.7",
                "full_code": "92425"
              },
              {
                "description": "Six units of time",
                "descriptor": "",
                "fee": "138.29",
                "full_code": "92426"
              },
              {
                "description": "Seven units of time",
                "descriptor": "",
                "fee": "156.91",
                "full_code": "92427"
              },
              {
                "description": "Eight units of time",
                "descriptor": "",
                "fee": "175.5",
                "full_code": "92428"
              },
              {
                "description": "Each addition unit over eight",
                "descriptor": "",
                "fee": "21.85",
                "full_code": "92429"
              },
              {
                "description": "Parenteral Conscious Sedation (regardless of method -IM or IV)",
                "descriptor": "",
                "fee": "",
                "full_code": "92440"
              },
              {
                "description": "One unit",
                "descriptor": "",
                "fee": "77.81",
                "full_code": "92441"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "155.62",
                "full_code": "92442"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "233.43",
                "full_code": "92443"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "311.24",
                "full_code": "92444"
              },
              {
                "description": "Five units",
                "descriptor": "",
                "fee": "389.05",
                "full_code": "92445"
              },
              {
                "description": "Six units",
                "descriptor": "",
                "fee": "466.86",
                "full_code": "92446"
              },
              {
                "description": "Seven units",
                "descriptor": "",
                "fee": "544.67",
                "full_code": "92447"
              },
              {
                "description": "Eight units",
                "descriptor": "",
                "fee": "622.48",
                "full_code": "92448"
              },
              {
                "description": "Each additional unit over eight",
                "descriptor": "",
                "fee": "77.81",
                "full_code": "92449"
              }
            ],
            "descriptor": "+PS",
            "description": "ANAESTHESIA, CONSCIOUS SEDATION Anaesthesia, Conscious Sedation - a medically controlled state of depressed consciousness that allows protective reflexes to be maintained, retains the patient's ability to maintain a patent airway independently and continuously and permits appropriate response by the patient to physical stimulation or verbal command, e.g., \"open your eyes\". (includes pre-anaesthetic evaluation and post anaesthetic follow-up) Any technique leading to these conditions in a patient would fall within this category of service. Conscious sedation is a varied technique which can require different levels of monitoring, in accordance with the Regulatory Authority Guidelines for the Use of Sedation and General Anaesthesia in Dental Practice. The Guidelines should be consulted and observed."
          },
          {
            "full_code": "92500",
            "children": [
              {
                "description": "Hypnosis",
                "descriptor": "",
                "fee": "",
                "full_code": "92510"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "62.87",
                "full_code": "92511"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "94.32",
                "full_code": "92512"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "125.78",
                "full_code": "92513"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "157.24",
                "full_code": "92514"
              },
              {
                "description": "Each additional unit over four",
                "descriptor": "",
                "fee": "31.45",
                "full_code": "92519"
              },
              {
                "description": "Acupuncture",
                "descriptor": "",
                "fee": "",
                "full_code": "92520"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "62.87",
                "full_code": "92521"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "94.32",
                "full_code": "92522"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "125.78",
                "full_code": "92523"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "157.24",
                "full_code": "92524"
              },
              {
                "description": "Each additional unit over four",
                "descriptor": "",
                "fee": "31.45",
                "full_code": "92529"
              },
              {
                "description": "Electronic Dental Anaesthesia",
                "descriptor": "",
                "fee": "",
                "full_code": "92530"
              },
              {
                "description": "One Unit of Time",
                "descriptor": "",
                "fee": "62.87",
                "full_code": "92531"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "94.32",
                "full_code": "92532"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "125.78",
                "full_code": "92533"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "157.24",
                "full_code": "92534"
              },
              {
                "description": "Each additional unit over four",
                "descriptor": "",
                "fee": "31.45",
                "full_code": "92539"
              }
            ],
            "descriptor": "",
            "description": "NON PHARAMACOLOGICAL PAIN CONTROL AND PATIENT MANAGEMENT"
          },
          {
            "full_code": "92900",
            "children": [
              {
                "description": "Management of patient with BMI 35 or above, in addition to code series 92200 or 92300",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "92901"
              }
            ],
            "descriptor": "",
            "description": "Anaesthesia \u2013 General Anaesthesia Or Deep Sedation, Unusual Time and Responsibility",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "ANAESTHESIA"
      },
      {
        "full_code": "93000",
        "children": [
          {
            "full_code": "93100",
            "children": [
              {
                "description": "Consultation with Member of the Profession or other Healthcare Providers, in or out of the office",
                "descriptor": "",
                "fee": "",
                "full_code": "93110"
              },
              {
                "description": "One unit of time",
                "descriptor": "+E",
                "fee": "101.7",
                "full_code": "93111"
              },
              {
                "description": "Two units",
                "descriptor": "+E",
                "fee": "203.4",
                "full_code": "93112"
              },
              {
                "description": "Each additional unit over two",
                "descriptor": "+E",
                "fee": "101.7",
                "full_code": "93119"
              },
              {
                "description": "Dental Legal Letters, Reports and Opinions",
                "descriptor": "",
                "fee": "",
                "full_code": "93120"
              },
              {
                "description": "A dental-legal report - a short factually written or verbal communication given to any lay person (e.g. lawyer, insurance representative, local, municipal or government agency, etc.) in relation the patient with prior patient approval.\n",
                "descriptor": "",
                "fee": "83.15 to 166.3",
                "full_code": "93121"
              },
              {
                "description": "A dental-legal report - a comprehensive written report with patient approval, on systems, history and records giving diagnosis, treatment, results and present condition. The report is a factual summary of all information available on the case and could contain prognostic information regarding patient response.\n",
                "descriptor": "",
                "fee": "166.3 to 332.6",
                "full_code": "93122"
              },
              {
                "description": "A dental-legal opinion - a comprehensive written report primarily in the field of expert opinion. The report may be an opinion regarding the possible course of events (when these cannot be determined factually), with possible long term consequences and complications in the development of the conditions.  The report will require expert knowledge and judgment with respect to the facts leading to a detailed prognosis.",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "93123"
              },
              {
                "description": "Consultation and/or Participation During Autopsy (other than forensic)",
                "descriptor": "",
                "fee": "",
                "full_code": "93130"
              },
              {
                "description": "One unit of time",
                "descriptor": "+E",
                "fee": "109.35",
                "full_code": "93131"
              },
              {
                "description": "Two units",
                "descriptor": "+E",
                "fee": "218.7",
                "full_code": "93132"
              },
              {
                "description": "Each additional unit over two",
                "descriptor": "",
                "fee": "109.35",
                "full_code": "93139"
              }
            ],
            "descriptor": "",
            "description": "PROFESSIONAL COMMUNICATIONS"
          },
          {
            "full_code": "93300",
            "children": [
              {
                "description": "Completing CDA \"Blank\" Approved Standard Claim Forms.",
                "descriptor": "",
                "fee": "NO FEE",
                "full_code": "93301"
              },
              {
                "description": "Upon request, Providing a Written Treatment Plan/Outline for a Patient, Similar to the Example in the\nCDA Policy Manual on Claim Form Completion.",
                "descriptor": "",
                "fee": "NO FEE",
                "full_code": "93302"
              },
              {
                "description": "Completing Prepaid Claim Forms which do not conform with Code 93301",
                "descriptor": "",
                "fee": "29.45",
                "full_code": "93303"
              },
              {
                "description": "For Extraordinary Time Spent in Relation to Claim Forms/Treatment Plan Forms, the Claim Problem\nof the Patient or Processing of Payments",
                "descriptor": "",
                "fee": "",
                "full_code": "93310"
              },
              {
                "description": "One unit of time",
                "descriptor": "+E",
                "fee": "96.71",
                "full_code": "93311"
              },
              {
                "description": "Two units",
                "descriptor": "+E",
                "fee": "193.42",
                "full_code": "93312"
              },
              {
                "description": "Each additional unit over two",
                "descriptor": "",
                "fee": "96.71",
                "full_code": "93319"
              },
              {
                "description": "For Extraordinary office Time Spent, In Forwarding Predetermination Records, In Predeterminations\nSituations, To Third Parties Plus Expenses (i.e. registration, postage, etc.)",
                "descriptor": "",
                "fee": "",
                "full_code": "93320"
              },
              {
                "description": "One unit of time",
                "descriptor": "+E",
                "fee": "25.68",
                "full_code": "93321"
              },
              {
                "description": "Two units",
                "descriptor": "+E",
                "fee": "51.36",
                "full_code": "93322"
              },
              {
                "description": "Each additional unit over two",
                "descriptor": "",
                "fee": "25.68",
                "full_code": "93329"
              },
              {
                "description": "Payment for Orthodontic Treatment In Progress",
                "descriptor": "",
                "fee": "",
                "full_code": "93330"
              },
              {
                "description": "Payment/Installment for treatment in progress",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "93331"
              },
              {
                "description": "Monthly payment/Instalments for treatment in progress",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "93332"
              },
              {
                "description": "Quarterly payment/installment for treatment in progress",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "93333"
              },
              {
                "description": "One time appliance",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "93334"
              },
              {
                "description": "Predetermination of available benefit. NO FEE",
                "descriptor": "",
                "fee": "",
                "full_code": "93340"
              },
              {
                "description": "Orthodontic Treatment (fee entered is the value of the treatment plan being predetermined)",
                "descriptor": "",
                "fee": "NO FEE",
                "full_code": "93341"
              }
            ],
            "descriptor": "",
            "description": "CLAIM FORMS AND TREATMENT FORMS",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "PROFESSIONAL CONSULTATIONS (diagnostic services provided by dentist other than practitioner providing treatment)"
      },
      {
        "full_code": "94000",
        "children": [
          {
            "full_code": "94100",
            "children": [
              {
                "description": "House Call, Non Emergency Visit (in addition to procedures performed)",
                "descriptor": "",
                "fee": "105.39",
                "full_code": "94101"
              },
              {
                "description": "House Call, Emergency Visit, when one must immediately leave home, office or hospital (in addition to\nprocedures performed)",
                "descriptor": "",
                "fee": "210.82",
                "full_code": "94102"
              }
            ],
            "descriptor": "",
            "description": "HOUSE CALLS",
            "fee": ""
          },
          {
            "full_code": "94300",
            "children": [
              {
                "description": "Office (of another professional) or Institutional Visit, During Regular Scheduled Office Hours (in\naddition to services performed)",
                "descriptor": "",
                "fee": "87.31",
                "full_code": "94301"
              },
              {
                "description": "Office (of another professional) or Institutional Visit Unscheduled, After Regular Scheduled Office\nHours (in addition to services performed)",
                "descriptor": "",
                "fee": "108.07",
                "full_code": "94302"
              },
              {
                "description": "Missed or Canceled Appointment, with Insufficient Notice, During Regular Scheduled Office Hours",
                "descriptor": "",
                "fee": "55.34",
                "full_code": "94303"
              },
              {
                "description": "Missed or Canceled Appointment with insufficient Notice, being a Special Appointment Outside Regular Scheduled Office Hours\n",
                "descriptor": "",
                "fee": "91.87 to 385.9",
                "full_code": "94304"
              },
              {
                "description": "Traveling Expenses",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "94305"
              },
              {
                "description": "Professional Visits Out of Office, plus actual services performed + E, (out of pocket expenses, etc.)",
                "descriptor": "+E",
                "fee": "163.5",
                "full_code": "94306"
              }
            ],
            "descriptor": "",
            "description": "OFFICE OR INSTITUTIONAL VISITS",
            "fee": ""
          },
          {
            "full_code": "94400",
            "children": [
              {
                "description": "Preparation as an Expert Witness",
                "descriptor": "",
                "fee": "",
                "full_code": "94410"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "94411"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "94412"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "94413"
              },
              {
                "description": "Four units",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "94414"
              },
              {
                "description": "Each additional unit over four",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "94419"
              },
              {
                "description": "Court Appearance as an Expert Witness",
                "descriptor": "",
                "fee": "",
                "full_code": "94420"
              },
              {
                "description": "One half day",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "94421"
              },
              {
                "description": "Full day",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "94422"
              }
            ],
            "descriptor": "",
            "description": "COURT APPEARANCE AND/OR PREPARATION"
          }
        ],
        "descriptor": "",
        "description": "PROFESSIONAL VISITS"
      },
      {
        "full_code": "95000",
        "children": [
          {
            "full_code": "95100",
            "children": [
              {
                "description": "Identification - opinion as an expert assisting in civil or criminal cases\n",
                "descriptor": "+E",
                "fee": "483.4 per hour",
                "full_code": "95101"
              },
              {
                "description": "Full or Part Time Participation in Civil Disaster\n",
                "descriptor": "+E",
                "fee": "2657.66 per diem",
                "full_code": "95102"
              },
              {
                "description": "Written Odontology Report\n",
                "descriptor": "+E",
                "fee": "51.78 to 557.74",
                "full_code": "95104"
              },
              {
                "description": "Post Mortem Examination of Tissues in Forensic Cases (non-identification)",
                "descriptor": "",
                "fee": "I.C.",
                "full_code": "95105"
              },
              {
                "description": "Management of Oral Disease or Abnormality\n",
                "descriptor": "",
                "fee": "91.87 to 192.93",
                "full_code": "95106"
              }
            ],
            "descriptor": "",
            "description": "FORENSIC SERVICES, MISCELLANEOUS",
            "fee": ""
          },
          {
            "full_code": "95200",
            "children": [
              {
                "description": "Identification Disk System, Acid Etch/Bonded",
                "descriptor": "+L",
                "fee": "87.31",
                "full_code": "95201"
              }
            ],
            "descriptor": "",
            "description": "IDENTIFICATION SYSTEMS",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "FORENSIC DENTAL SERVICES"
      },
      {
        "full_code": "96000",
        "children": [
          {
            "full_code": "96100",
            "children": [
              {
                "description": "Prescription, Emergency",
                "descriptor": "",
                "fee": "39.78",
                "full_code": "96101"
              },
              {
                "description": "Emergency Dispensing of One or Two Doses of a Therapeutic Drug, plus Giving a Written Prescription",
                "descriptor": "+E",
                "fee": "54.15",
                "full_code": "96102"
              },
              {
                "description": "Dispensing, Non Emergency (e.g. fluorides, vitamins, other drugs/medications)",
                "descriptor": "+E",
                "fee": "43.59",
                "full_code": "96103"
              },
              {
                "description": "Prescription, vaccine",
                "descriptor": "",
                "fee": "39.78",
                "full_code": "96104"
              }
            ],
            "descriptor": "",
            "description": "PRESCRIPTIONS",
            "fee": ""
          },
          {
            "full_code": "96200",
            "children": [
              {
                "description": "Intramuscular Drug Injection",
                "descriptor": "+E",
                "fee": "58.47",
                "full_code": "96201"
              },
              {
                "description": "Intravenous Drug Injection",
                "descriptor": "+E",
                "fee": "58.47",
                "full_code": "96202"
              },
              {
                "description": "Intralesional Delivery (Intra-articular Injections - see 78600)",
                "descriptor": "+E",
                "fee": "58.47",
                "full_code": "96203"
              }
            ],
            "descriptor": "",
            "description": "INJECTIONS, THERAPEUTIC",
            "fee": ""
          },
          {
            "full_code": "96300",
            "children": [
              {
                "description": "Injections of neuromodulator, aesthetic 1 to 5 units",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "96301"
              },
              {
                "description": "Injections of neuromodulator, aesthetic 6 to 10 units",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "96302"
              },
              {
                "description": "Injections of neuromodulator, aesthetic 11 to 20 units",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "96303"
              },
              {
                "description": "Injections of neuromodulator, aesthetic 21 to 30 units",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "96304"
              },
              {
                "description": "Injections of neuromodulator, aesthetic 31 to 40 units",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "96305"
              },
              {
                "description": "Injections of neuromodulator, aesthetic 41 to 50 units",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "96306"
              },
              {
                "description": "Injections of neuromodulator, aesthetic 51 to 60 units",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "96307"
              },
              {
                "description": "Injections of neuromodulator, aesthetic 61 to 70 units",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "96308"
              },
              {
                "description": "Injections of neuromodulator, aesthetic more than 70 units",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "96309"
              }
            ],
            "descriptor": "",
            "description": "INJECTIONS AESTHETIC \u2013 ADMINISTRATION OF AESTHETIC NEUROMODULATORS (EG. BOTULINUM\nTOXIN TYPE A) (Note \u201cunits\u201d refers to a drug dosage)",
            "fee": ""
          },
          {
            "full_code": "96400",
            "children": [
              {
                "description": "Aesthetic dermal filler first syringe",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "96401"
              },
              {
                "description": "Aesthetic dermal filler subsequent syringe (use once for each syringe)",
                "descriptor": "+E",
                "fee": "I.C.",
                "full_code": "96409"
              }
            ],
            "descriptor": "",
            "description": "INJECTIONS AESTHETIC \u2013 ADMINISTRATION OF AESTHETIC DERMAL FILLERS",
            "fee": ""
          },
          {
            "full_code": "96500",
            "children": [
              {
                "description": "Vaccine injection",
                "descriptor": "+E",
                "fee": "58.47",
                "full_code": "96501"
              },
              {
                "description": "Vaccine, administered by other routes (e.g. nasal/oral)",
                "descriptor": "+E",
                "fee": "58.47",
                "full_code": "96502"
              }
            ],
            "descriptor": "",
            "description": "VACCINE ADMINISTRATION",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "DRUGS/MEDICATION, DISPENSING"
      },
      {
        "full_code": "97000",
        "children": [
          {
            "full_code": "97110",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "96.47",
                "full_code": "97111"
              },
              {
                "description": "Two units",
                "descriptor": "",
                "fee": "192.94",
                "full_code": "97112"
              },
              {
                "description": "Three units",
                "descriptor": "",
                "fee": "289.41",
                "full_code": "97113"
              },
              {
                "description": "Each additional unit over three",
                "descriptor": "",
                "fee": "96.47",
                "full_code": "97119"
              },
              {
                "description": "Bleaching, Vital Home (Includes the Fabrication of Bleaching Trays, Dispensing the System and Follow-\nup Care)",
                "descriptor": "",
                "fee": "",
                "full_code": "97120"
              },
              {
                "description": "Maxillary Arch",
                "descriptor": "+L and/or\n+E",
                "fee": "275.66",
                "full_code": "97121"
              },
              {
                "description": "Mandibular Arch",
                "descriptor": "+L and/or\n+E",
                "fee": "275.66",
                "full_code": "97122"
              },
              {
                "description": "Micro-Abrasion",
                "descriptor": "",
                "fee": "",
                "full_code": "97130"
              },
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "87.3",
                "full_code": "97131"
              },
              {
                "description": "Two units of time",
                "descriptor": "",
                "fee": "174.6",
                "full_code": "97132"
              },
              {
                "description": "Three units of time",
                "descriptor": "",
                "fee": "261.9",
                "full_code": "97133"
              },
              {
                "description": "Four units of time",
                "descriptor": "",
                "fee": "349.2",
                "full_code": "97134"
              },
              {
                "description": "Each additional unit over four",
                "descriptor": "",
                "fee": "87.3",
                "full_code": "97139"
              }
            ],
            "descriptor": "",
            "description": "Bleaching, Vital, In Office",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "BLEACHING, VITAL"
      },
      {
        "full_code": "98000",
        "children": [
          {
            "full_code": "98100",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "+E",
                "fee": "87.3",
                "full_code": "98101"
              },
              {
                "description": "Two units of time",
                "descriptor": "+E",
                "fee": "174.6",
                "full_code": "98102"
              },
              {
                "description": "Each additional unit of time",
                "descriptor": "+E",
                "fee": "87.3",
                "full_code": "98109"
              }
            ],
            "descriptor": "",
            "description": "TOBACCO OR CANNABIS-USE CESSATION SERVICES To include: identifying patients who use tobacco or cannabis, informing patients of oral health consequences associated with tobacco or cannabis; advising tobacco or cannabis users to quit; provide appropriate self-help material; and discuss\ntreatment options.",
            "fee": ""
          },
          {
            "full_code": "98300",
            "children": [
              {
                "description": "One unit of time",
                "descriptor": "",
                "fee": "87.3",
                "full_code": "98301"
              },
              {
                "description": "Two units of time",
                "descriptor": "",
                "fee": "174.6",
                "full_code": "98302"
              },
              {
                "description": "Each additional unit of time",
                "descriptor": "",
                "fee": "87.3",
                "full_code": "98309"
              }
            ],
            "descriptor": "",
            "description": "Vaccine/Vaccination Consultation, with patient (includes analysis of medical status, indications and\ncontraindications, and the risks and benefits)",
            "fee": ""
          }
        ],
        "descriptor": "",
        "description": "COUNSELLING"
      },
      {
        "full_code": "99000",
        "children": [
          {
            "full_code": "99111",
            "children": [],
            "descriptor": "+L",
            "description": "\"+L\" Commercial Laboratory Procedures (A commercial laboratory is defined as an  independent\nbusiness which performs laboratory services and bills the dental practices for these services on a case by case basis)"
          },
          {
            "full_code": "99222",
            "children": [],
            "descriptor": "+L",
            "description": "\"+L\" For oral pathology biopsy services when provided in relation to surgical services from the 30000,\n40000, or 70000 code services."
          },
          {
            "full_code": "99333",
            "children": [],
            "descriptor": "+L",
            "description": "\"+L\" In-Office Laboratory  Procedures  (An in-office laboratory is defined as a laboratory service(s)\nperformed within the same business entity)."
          },
          {
            "full_code": "99555",
            "children": [],
            "descriptor": "+E",
            "description": "\"+E\" Additional Expense of Materials"
          },
          {
            "full_code": "99777",
            "children": [],
            "descriptor": "+PS",
            "description": "\"+PS\" Charges for professional services billed to the dentist and passed through to the patient."
          }
        ],
        "descriptor": "",
        "description": "LABORATORY, EXPENSE AND PROFESSIONAL SERVICE PROCEDURES (This code is used in conjunction with the \"+L\" and \"+E\" and \"+P.S.\" designation following specific codes in the guide. The addition of these codes are to facilitate computer or manual input for third party claims processing, personal records and statistics, providing one description for a specific procedure code.) When filling out the third party claim forms, these codes must follow immediately after the corresponding dental procedure code carried out by the dentist, so as to correlate the lab expenses with the correct procedures."
      }
    ],
    "descriptor": "",
    "description": "GENERAL SERVICES"
  }
]

export default data;